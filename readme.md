# Wheels and Deals Package

## Use this in production

* Add the repository to composer.json:


~~~~
"repositories": [
    {
      "type": "vcs",
      "url": "git@bitbucket.org:tsawler/wheelspackage.git"
    }
    ],
    "require": {
~~~~

* Add the package in composer.json

~~~~
    "torann/geoip": "^1.0",
    "tsawler/wheelspackage": "dev-master",
    "unisharp/laravel-filemanager": "^1.8",
~~~~

* Add this to the post-install-cmd in composer.json

~~~~
    "@php -r \"exec('cp ./vendor/tsawler/wheelspackage/src/fonts/* ./vendor/setasign/fpdf/font/');\"",
~~~~

* Add this to the post-update-cmd in composer.json

~~~~
    "@php -r \"exec('cp ./vendor/tsawler/wheelspackage/src/fonts/* ./vendor/setasign/fpdf/font/');\"",
~~~~

* run composer update
* run php artisan:vendor publish
* copy src/views/composer.php to <root>/resources/views/custom

## Setup Instructions for development

1. Install a new [ Blender ](https://bitbucket.org/tsawler/blender) app
2. Clone this package
3. Put contents of this package into laravel app at this location: <root>/packages/tsawler/csacpackage
4. Add this the the app's composer.json file:

~~~~
"autoload": {
    "classmap": [
        "database/seeds",
        "database/factories"
    ],
    "psr-4": {
        "App\\": "app/",
        "Tsawler\\WheelsPackage\\": "packages/tsawler/wheelspackage/src"
    }
},
~~~~

_For development only_, you need to add this to config/app.php:

~~~~
   Tsawler\WheelsPackage\WheelsPackageServiceProvider::class,
~~~~


Run composer update

Also need this mysql function to strip html tags:

~~~~
delimiter ||
DROP FUNCTION IF EXISTS strip_tags||
CREATE FUNCTION strip_tags( x longtext) RETURNS longtext
LANGUAGE SQL NOT DETERMINISTIC READS SQL DATA
BEGIN
DECLARE sstart INT UNSIGNED;
DECLARE ends INT UNSIGNED;
SET sstart = LOCATE('<', x, 1);
REPEAT
SET ends = LOCATE('>', x, sstart);
SET x = CONCAT(SUBSTRING( x, 1 ,sstart -1) ,SUBSTRING(x, ends +1 )) ;
SET sstart = LOCATE('<', x, 1);
UNTIL sstart < 1 END REPEAT;
return x;
END;
||
delimiter ;
~~~~


