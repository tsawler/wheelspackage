<?php

namespace Tsawler\WheelsPackage;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QuickQuoteMailable extends Mailable
{

    use Queueable, SerializesModels;

    public $users_name;
    public $users_phone;
    public $users_email;
    public $vehicle_id;
    public $talk_about;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->users_name = $data['users_name'];
        $this->users_phone = $data['users_phone'];
        $this->users_email = $data['users_email'];
        $this->vehicle_id = $data['vehicle_id'];
        $this->talk_about = $data['talk_about'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('wheelspackage::mail.request-quick-quote')
            ->subject($this->users_name . ' has requested a quick quote');
    }
}
