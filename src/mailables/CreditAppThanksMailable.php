<?php

namespace Tsawler\WheelsPackage;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreditAppThanksMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $first_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('wheelspackage::mail.credit-app-thanks')
            ->subject('Credit Application at Jim Gilbert\'s Wheels & Deals');;
    }
}
