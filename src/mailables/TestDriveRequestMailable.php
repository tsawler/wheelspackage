<?php

namespace Tsawler\WheelsPackage;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestDriveRequestMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $users_name;
    public $vehicle_id;
    public $users_phone;
    public $users_email;
    public $preferred_date;
    public $preferred_time;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->users_name = $data['users_name'];
        $this->users_email = $data['users_email'];
        $this->vehicle_id = $data['vehicle_id'];
        $this->preferred_date = $data['preferred_date'];
        $this->preferred_time = $data['preferred_time'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('wheelspackage::mail.request-test-drive')
            ->subject($this->users_name . ' has requested a test drive');
    }
}
