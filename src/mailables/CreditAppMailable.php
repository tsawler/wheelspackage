<?php

namespace Tsawler\WheelsPackage;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreditAppMailable extends Mailable
{

    use Queueable, SerializesModels;

    public $id;
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $address;
    public $city;
    public $province;
    public $zip;
    public $vehicle;
    public $income;
    public $rent;
    public $employer;
    public $dob;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->first_name = $data['first_name'];
        $this->last_name = $data['last_name'];
        $this->phone = $data['phone'];
        $this->email = $data['email'];
        $this->address = $data['address'];
        $this->city = $data['city'];
        $this->province = $data['province'];
        $this->zip = $data['zip'];
        $this->vehicle = $data['vehicle'];
        $this->income = $data['income'];
        $this->rent = $data['rent'];
        $this->employer = $data['employer'];
        $this->dob = $data['dob'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('wheelspackage::mail.credit-app-notify')
            ->subject('Credit Application from ' . $this->first_name . " " . $this->last_name);
    }
}
