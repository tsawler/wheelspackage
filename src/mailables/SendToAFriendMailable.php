<?php

namespace Tsawler\WheelsPackage;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendToAFriendMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $users_name;
    public $vehicle_id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->users_name = $data['users_name'];
        $this->vehicle_id = $data['vehicle_id'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('wheelspackage::mail.send-to-a-friend')
            ->subject($this->users_name . ' thought you might like to see this vehicle at Jim Gilbert\'s Wheels & Deals');;
    }
}
