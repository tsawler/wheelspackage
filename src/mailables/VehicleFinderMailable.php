<?php

namespace Tsawler\WheelsPackage;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VehicleFinderMailable extends Mailable
{

    use Queueable, SerializesModels;

    public $id;
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $contact_method;
    public $year;
    public $make;
    public $model;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->first_name = $data['first_name'];
        $this->last_name = $data['last_name'];
        $this->phone = $data['phone'];
        $this->email = $data['email'];
        $this->contact_method = $data['contact_method'];
        $this->year = $data['year'];
        $this->make = $data['make'];
        $this->model = $data['model'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('wheelspackage::mail.vehicle-finder')
            ->subject('Vehicle Finder Request from ' . $this->first_name . " " . $this->last_name)
            ->to(['chelsea.gilbert@wheelsanddeals.ca', 'wheelsanddeals@pbssystems.com']);
//            ->to(['john.eliakis@wheelsanddeals.ca', 'chelsea.gilbert@wheelsanddeals.ca', 'wheelsanddeals@pbssystems.com']);
    }
}
