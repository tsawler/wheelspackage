<?php namespace Tsawler\WheelsPackage;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Sale
 * @package Tsawler\WheelsPackage
 */
class Sale extends Model
{

    use Sluggable;

    /**
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'salesperson_name',
            ],
        ];
    }
}
