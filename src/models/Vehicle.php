<?php namespace Tsawler\WheelsPackage;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Vehicle
 * @package Tsawler\WheelsPackage
 */
class Vehicle extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'stock_no',
        'cost',
        'vin',
        'odometer',
        'year',
        'make',
        'model',
        'stock_no',
        'trim',
        'vehicle_type',
        'body',
        'seating_capacity',
        'drive_train',
        'engine',
        'exterior_color',
        'interior_color',
        'transmission',
        'options',
        'model_number',
        'total_msr',
        'status',
        'description',
        'vehicle_makes_id',
        'vehicle_models_id',
        'hand_picked',
        'used',
        'price_for_display',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function make()
    {
        return $this->hasOne('Tsawler\WheelsPackage\VehicleMake', 'id', 'vehicle_makes_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function vehicleModel()
    {
        return $this->hasOne('Tsawler\WheelsPackage\VehicleModel', 'id', 'vehicle_models_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('Tsawler\WheelsPackage\VehicleImage')->orderBy('sort_order', 'asc')->orderBy('created_at', 'asc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function firstImage()
    {
        return $this->hasOne('Tsawler\WheelsPackage\VehicleImage')
            ->where('sort_order', '=', '1');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany('Tsawler\WheelsPackage\VehicleOption');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function video()
    {
        return $this->hasOne('Tsawler\WheelsPackage\VehicleVideo', 'vehicle_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function panorama()
    {
        return $this->hasOne('Tsawler\WheelsPackage\VehiclePanorama', 'vehicle_id', 'id');
    }


}
