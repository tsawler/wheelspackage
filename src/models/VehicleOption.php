<?php namespace Tsawler\WheelsPackage;

use Illuminate\Database\Eloquent\Model;

/**
 * Class VehicleOption
 * @package Tsawler\WheelsPackage
 */
class VehicleOption extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function option()
    {
        return $this->hasOne('Tsawler\WheelsPackage\Option', 'id', 'option_id');
    }
}
