<?php namespace Tsawler\WheelsPackage;

/**
 * Class WindowSticker
 * @package Tsawler\WheelsPackage
 */
class WindowSticker extends \setasign\Fpdi\Fpdi
{

    var $B;
    var $I;
    var $U;
    var $HREF;
    var $NewPageGroup;
    var $PageGroups;   // variable indicating whether a new group was requested
    var $CurrPageGroup;     // variable containing the number of pages of the groups
    var $wLine;  // variable containing the alias of the current page group

    // create a new page group; call this before calling AddPage()
    var $hLine;

    // current page in the group
    var $Text;

    // alias of the current page group -- will be replaced by the total number of pages in this group
    var $border;
    var $align;
    var $fill;
    var $Padding;
    var $lPadding;
    var $tPadding;
    var $bPadding;
    var $rPadding;
    var $TagStyle;


    // Page footer
    var $Indent;
    var $Space;
    var $PileStyle;
    var $Line2Print;
    var $NextLineBegin;
    var $TagName;
    var $Delta;
    var $StringLength; // Largeur maximale de la ligne
    var $LineLength; // Hauteur de la ligne
    var $wTextLine; // Texte à afficher
    var $nbSpace;
    var $Xini; // Justification du texte
    var $href;
    var $TagHref;
    private $name;
    private $student_name;
    private $footer_trailing;

    function StartPageGroup()
    {
        $this->NewPageGroup = true;
    }

    function GroupPageNo()
    {
        return $this->PageGroups[$this->CurrPageGroup];
    } // Style associé à chaque balise

    function PageGroupAlias()
    {
        return $this->CurrPageGroup;
    }

    function _beginpage($orientation, $size = "Letter", $rotation = "")
    {
        parent::_beginpage($orientation, $size, $rotation);
        if ($this->NewPageGroup) {
            // start a new group
            $n = sizeof($this->PageGroups) + 1;
            $alias = "{nb}";
            $this->PageGroups[$alias] = 1;
            $this->CurrPageGroup = $alias;
            $this->NewPageGroup = false;
        } elseif ($this->CurrPageGroup) {
            $this->PageGroups[$this->CurrPageGroup] ++;
        }
    } // Espace minimum entre les mots

    function _putpages()
    {
        $nb = $this->page;
        if (!empty($this->PageGroups)) {
            // do page number replacement
            foreach ($this->PageGroups as $k => $v) {
                for ($n = 1; $n <= $nb; $n ++) {
                    $this->pages[$n] = str_replace($k, $v, $this->pages[$n]);
                }
            }
        }
        parent::_putpages();
    }

    function setName($x)
    {
        $this->name = $this->Unaccent($x);
    } // Ligne à afficher

    function Unaccent($string)
    {
        if (strpos($string = htmlentities($string, ENT_QUOTES, 'UTF-8'), '&') !== false) {
            $codes = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i';
            $string = html_entity_decode(preg_replace($codes, '$1', $string), ENT_QUOTES, 'UTF-8');
        }

        return $string;
    } // Tampon entre lignes

    function Header()
    {
        $this->SetY(10);
        $this->SetFont('Arial', 'I', 8);
        // Move to the right
        //$this->Cell(80);
        // Title
        $this->Cell(0, 0, iconv('UTF-8', 'ASCII//TRANSLIT', $this->name), 0, 0, 'C');
        //$this->Cell(0,0,  $this->name,0,0,'C');
        $this->Ln(10);
    } // Tableau

    function setFooterText($name)
    {
        $this->student_name = $name;
    } // Largeur maximale moins largeur

    function setFooterTextTrailing($name)
    {
        $this->footer_trailing = $name;
    }

    function Footer()
    {

    }

    function WriteHTML($html)
    {
        // HTML parser
        $html = str_replace("\n", ' ', $html);
        $html = str_replace("\t", ' ', $html);
        $a = preg_split('/<(.*)>/U', $html, - 1, PREG_SPLIT_DELIM_CAPTURE);
        foreach ($a as $i => $e) {
            if ($i % 2 == 0) {
                // Text
                if ($this->HREF) {
                    $this->PutLink($this->HREF, $e);
                } else {
                    $this->Write(5, $e);
                }
            } else {
                // Tag
                if ($e[0] == '/') {
                    $this->CloseTag(strtoupper(substr($e, 1)));
                } else {
                    // Extract attributes
                    $a2 = explode(' ', $e);
                    $tag = strtoupper(array_shift($a2));
                    $attr = [];
                    foreach ($a2 as $v) {
                        if (preg_match('/([^=]*)=["\']?([^"\']*)/', $v, $a3)) {
                            $attr[strtoupper($a3[1])] = $a3[2];
                        }
                    }
                    $this->OpenTag($tag, $attr);
                }
            }
        }
    } //Largeur moins les "padding"

    function PutLink($URL, $txt)
    {
        // Put a hyperlink
        $this->SetTextColor(0, 0, 255);
        $this->SetStyle('U', true);
        $this->Write(5, $txt, $URL);
        $this->SetStyle('U', false);
        $this->SetTextColor(0);
    } // Nombre d'espaces dans la ligne

    function SetStyle($tag, $enable)
    {
        // Modify style and select corresponding font
        $this->$tag += ($enable ? 1 : - 1);
        $style = '';
        foreach (['B', 'I', 'U'] as $s) {
            if ($this->$s > 0) {
                $style .= $s;
            }
        }
        $this->SetFont('', $style);
    } // Position initiale

    function CloseTag($tag)
    {
        // Closing tag
        if ($tag == 'B' || $tag == 'I' || $tag == 'U') {
            $this->SetStyle($tag, false);
        }
        if ($tag == 'A') {
            $this->HREF = '';
        }
    } // URL courante

    function OpenTag($tag, $attr)
    {
        // Opening tag
        if ($tag == 'B' || $tag == 'I' || $tag == 'U') {
            $this->SetStyle($tag, true);
        }
        if ($tag == 'A') {
            $this->HREF = $attr['HREF'];
        }
        if ($tag == 'BR') {
            $this->Ln(5);
        }
    } // URL associée à une cellule

    // Fonctions Publiques

    function WriteTag($w, $h, $txt, $border = 0, $align = "J", $fill = false, $padding = 0)
    {
        $this->wLine = $w;
        $this->hLine = $h;
        $this->Text = trim($txt);
        $this->Text = preg_replace("/\n|\r|\t/", "", $this->Text);
        $this->border = $border;
        $this->align = $align;
        $this->fill = $fill;
        $this->Padding = $padding;

        $this->Xini = $this->GetX();
        $this->href = "";
        $this->PileStyle = [];
        $this->TagHref = [];
        $this->LastLine = false;

        $this->SetSpace();
        $this->Padding();
        $this->LineLength();
        $this->BorderTop();

        while ($this->Text != "") {
            $this->MakeLine();
            $this->PrintLine();
        }

        $this->BorderBottom();
    }

    function SetSpace() // Espace minimal entre les mots
    {
        $tag = $this->Parser($this->Text);
        $this->FindStyle($tag[2], 0);
        $this->DoStyle(0);
        $this->Space = $this->GetStringWidth(" ");
    }


    // Fonctions privées

    function Parser($text)
    {
        $tab = [];
        // Balise fermante
        if (preg_match("|^(</([^>]+)>)|", $text, $regs)) {
            $tab[1] = "c";
            $tab[2] = trim($regs[2]);
        } // Balise ouvrante
        else if (preg_match("|^(<([^>]+)>)|", $text, $regs)) {
            $regs[2] = preg_replace("/^a/", "a ", $regs[2]); // Rustine : l'espace disparaît
            $tab[1] = "o";
            $tab[2] = trim($regs[2]);

            // Présence d'attributs
            if (preg_match("/(.+) (.+)='(.+)'/", $regs[2])) {
                $tab1 = preg_split("/ +/", $regs[2]);
                $tab[2] = trim($tab1[0]);
                while (list($i, $couple) = each($tab1)) {
                    if ($i > 0) {
                        $tab2 = explode("=", $couple);
                        $tab2[0] = trim($tab2[0]);
                        $tab2[1] = trim($tab2[1]);
                        $end = strlen($tab2[1]) - 2;
                        $tab[$tab2[0]] = substr($tab2[1], 1, $end);
                    }
                }
            }
        } // Espace
        else if (preg_match("/^( )/", $text, $regs)) {
            $tab[1] = "s";
            $tab[2] = ' ';
        } // Texte
        else if (preg_match("/^([^< ]+)/", $text, $regs)) {
            $tab[1] = "t";
            $tab[2] = trim($regs[1]);
        }
        // Elagage
        $begin = strlen($regs[1]);
        $end = strlen($text);
        $text = substr($text, $begin, $end);
        $tab[0] = $text;

        return $tab; // 0 : $text; 1 : type de balise (tag); 2 : élément
    }

    function FindStyle($tag, $ind) // Héritage des éléments parents
    {
        $tag = trim($tag);

        // Famille
        if ($this->TagStyle[$tag]['family'] != "") {
            $family = $this->TagStyle[$tag]['family'];
        } else {
            reset($this->PileStyle);
            while (list($k, $val) = each($this->PileStyle)) {
                $val = trim($val);
                if ($this->TagStyle[$val]['family'] != "") {
                    $family = $this->TagStyle[$val]['family'];
                    break;
                }
            }
        }

        // Style
        $style = "";
        $style1 = strtoupper($this->TagStyle[$tag]['style']);
        if ($style1 != "N") {
            $bold = false;
            $italic = false;
            $underline = false;
            reset($this->PileStyle);
            while (list($k, $val) = each($this->PileStyle)) {
                $val = trim($val);
                $style1 = strtoupper($this->TagStyle[$val]['style']);
                if ($style1 == "N") {
                    break;
                } else {
                    if (strpos($style1, "B") !== false) {
                        $bold = true;
                    }
                    if (strpos($style1, "I") !== false) {
                        $italic = true;
                    }
                    if (strpos($style1, "U") !== false) {
                        $underline = true;
                    }
                }
            }
            if ($bold) {
                $style .= "B";
            }
            if ($italic) {
                $style .= "I";
            }
            if ($underline) {
                $style .= "U";
            }
        }

        // Taille
        if ($this->TagStyle[$tag]['size'] != 0) {
            $size = $this->TagStyle[$tag]['size'];
        } else {
            reset($this->PileStyle);
            while (list($k, $val) = each($this->PileStyle)) {
                $val = trim($val);
                if ($this->TagStyle[$val]['size'] != 0) {
                    $size = $this->TagStyle[$val]['size'];
                    break;
                }
            }
        }

        // Couleur
        if ($this->TagStyle[$tag]['color'] != "") {
            $color = $this->TagStyle[$tag]['color'];
        } else {
            reset($this->PileStyle);
            while (list($k, $val) = each($this->PileStyle)) {
                $val = trim($val);
                if ($this->TagStyle[$val]['color'] != "") {
                    $color = $this->TagStyle[$val]['color'];
                    break;
                }
            }
        }

        // Résultat
        $this->TagStyle[$ind]['family'] = $family;
        $this->TagStyle[$ind]['style'] = $style;
        $this->TagStyle[$ind]['size'] = $size;
        $this->TagStyle[$ind]['color'] = $color;
        $this->TagStyle[$ind]['indent'] = $this->TagStyle[$tag]['indent'];
    }

    function DoStyle($tag) // Applique un style
    {
        $tag = trim($tag);
        $this->SetFont(
            $this->TagStyle[$tag]['family'],
            $this->TagStyle[$tag]['style'],
            $this->TagStyle[$tag]['size']
        );

        $tab = explode(",", $this->TagStyle[$tag]['color']);
        if (count($tab) == 1) {
            $this->SetTextColor($tab[0]);
        } else {
            $this->SetTextColor($tab[0], $tab[1], $tab[2]);
        }
    }

    function Padding()
    {
        if (preg_match("/^.+,/", $this->Padding)) {
            $tab = explode(",", $this->Padding);
            $this->lPadding = $tab[0];
            $this->tPadding = $tab[1];
            if (isset($tab[2])) {
                $this->bPadding = $tab[2];
            } else {
                $this->bPadding = $this->tPadding;
            }
            if (isset($tab[3])) {
                $this->rPadding = $tab[3];
            } else {
                $this->rPadding = $this->lPadding;
            }
        } else {
            $this->lPadding = $this->Padding;
            $this->tPadding = $this->Padding;
            $this->bPadding = $this->Padding;
            $this->rPadding = $this->Padding;
        }
        if ($this->tPadding < $this->LineWidth) {
            $this->tPadding = $this->LineWidth;
        }
    }

    function LineLength()
    {
        if ($this->wLine == 0) {
            $this->wLine = $this->w - $this->Xini - $this->rMargin;
        }

        $this->wTextLine = $this->wLine - $this->lPadding - $this->rPadding;
    }

    function BorderTop()
    {
        $border = 0;
        if ($this->border == 1) {
            $border = "TLR";
        }
        $this->Cell($this->wLine, $this->tPadding, "", $border, 0, 'C', $this->fill);
        $y = $this->GetY() + $this->tPadding;
        $this->SetXY($this->Xini, $y);
    }

    function MakeLine() // Fabrique une ligne
    {
        $this->Text .= " ";
        $this->LineLength = [];
        $this->TagHref = [];
        $Length = 0;
        $this->nbSpace = 0;

        $i = $this->BeginLine();
        $this->TagName = [];

        if ($i == 0) {
            $Length = $this->StringLength[0];
            $this->TagName[0] = 1;
            $this->TagHref[0] = $this->href;
        }

        while ($Length < $this->wTextLine) {
            $tab = $this->Parser($this->Text);
            $this->Text = $tab[0];
            if ($this->Text == "") {
                $this->LastLine = true;
                break;
            }

            if ($tab[1] == "o") {
                array_unshift($this->PileStyle, $tab[2]);
                $this->FindStyle($this->PileStyle[0], $i + 1);

                $this->DoStyle($i + 1);
                $this->TagName[$i + 1] = 1;
                if ($this->TagStyle[$tab[2]]['indent'] != - 1) {
                    $Length += $this->TagStyle[$tab[2]]['indent'];
                    $this->Indent = $this->TagStyle[$tab[2]]['indent'];
                }
                if ($tab[2] == "a") {
                    $this->href = $tab['href'];
                }
            }

            if ($tab[1] == "c") {
                array_shift($this->PileStyle);
                if (isset($this->PileStyle[0])) {
                    $this->FindStyle($this->PileStyle[0], $i + 1);
                    $this->DoStyle($i + 1);
                }
                $this->TagName[$i + 1] = 1;
                if ($this->TagStyle[$tab[2]]['indent'] != - 1) {
                    $this->LastLine = true;
                    $this->Text = trim($this->Text);
                    break;
                }
                if ($tab[2] == "a") {
                    $this->href = "";
                }
            }

            if ($tab[1] == "s") {
                $i ++;
                $Length += $this->Space;
                $this->Line2Print[$i] = "";
                if ($this->href != "") {
                    $this->TagHref[$i] = $this->href;
                }
            }

            if ($tab[1] == "t") {
                $i ++;
                $this->StringLength[$i] = $this->GetStringWidth($tab[2]);
                $Length += $this->StringLength[$i];
                $this->LineLength[$i] = $Length;
                $this->Line2Print[$i] = $tab[2];
                if ($this->href != "") {
                    $this->TagHref[$i] = $this->href;
                }
            }
        }

        trim($this->Text);
        if ($Length > $this->wTextLine || $this->LastLine == true) {
            $this->EndLine();
        }
    }

    function BeginLine()
    {
        $this->Line2Print = [];
        $this->StringLength = [];

        if (isset($this->PileStyle[0])) {
            $this->FindStyle($this->PileStyle[0], 0);
            $this->DoStyle(0);
        }

        if (count($this->NextLineBegin) > 0) {
            $this->Line2Print[0] = $this->NextLineBegin['text'];
            $this->StringLength[0] = $this->NextLineBegin['length'];
            $this->NextLineBegin = [];
            $i = 0;
        } else {
            preg_match("/^(( *(<([^>]+)>)* *)*)(.*)/", $this->Text, $regs);
            $regs[1] = str_replace(" ", "", $regs[1]);
            $this->Text = $regs[1] . $regs[5];
            $i = - 1;
        }

        return $i;
    }

    function EndLine()
    {
        if (end($this->Line2Print) != "" && $this->LastLine == false) {
            $this->NextLineBegin['text'] = array_pop($this->Line2Print);
            $this->NextLineBegin['length'] = end($this->StringLength);
            array_pop($this->LineLength);
        }

        while (end($this->Line2Print) === "") {
            array_pop($this->Line2Print);
        }

        $this->Delta = $this->wTextLine - end($this->LineLength);

        $this->nbSpace = 0;
        for ($i = 0; $i < count($this->Line2Print); $i ++) {
            if ($this->Line2Print[$i] == "") {
                $this->nbSpace ++;
            }
        }
    }

    function PrintLine()
    {
        $border = 0;
        if ($this->border == 1) {
            $border = "LR";
        }
        $this->Cell($this->wLine, $this->hLine, "", $border, 0, 'C', $this->fill);
        $y = $this->GetY();
        $this->SetXY($this->Xini + $this->lPadding, $y);

        if ($this->Indent != - 1) {
            if ($this->Indent != 0) {
                $this->Cell($this->Indent, $this->hLine);
            }
            $this->Indent = - 1;
        }

        $space = $this->LineAlign();
        $this->DoStyle(0);
        for ($i = 0; $i < count($this->Line2Print); $i ++) {
            if (isset($this->TagName[$i])) {
                $this->DoStyle($i);
            }
            if (isset($this->TagHref[$i])) {
                $href = $this->TagHref[$i];
            } else {
                $href = '';
            }
            if ($this->Line2Print[$i] == "") {
                $this->Cell($space, $this->hLine, "         ", 0, 0, 'C', false, $href);
            } else {
                $this->Cell($this->StringLength[$i], $this->hLine, $this->Line2Print[$i], 0, 0, 'C', false, $href);
            }
        }

        $this->LineBreak();
        if ($this->LastLine && $this->Text != "") {
            $this->EndParagraph();
        }
        $this->LastLine = false;
    }

    function LineAlign()
    {
        $space = $this->Space;
        if ($this->align == "J") {
            if ($this->nbSpace != 0) {
                $space = $this->Space + ($this->Delta / $this->nbSpace);
            }
            if ($this->LastLine) {
                $space = $this->Space;
            }
        }

        if ($this->align == "R") {
            $this->Cell($this->Delta, $this->hLine);
        }

        if ($this->align == "C") {
            $this->Cell($this->Delta / 2, $this->hLine);
        }

        return $space;
    }

    function LineBreak() // Retour à la ligne
    {
        $x = $this->Xini;
        $y = $this->GetY() + $this->hLine;
        $this->SetXY($x, $y);
    }

    function EndParagraph() // Interligne entre paragraphes
    {
        $border = 0;
        if ($this->border == 1) {
            $border = "LR";
        }
        $this->Cell($this->wLine, $this->hLine / 2, "", $border, 0, 'C', $this->fill);
        $x = $this->Xini;
        $y = $this->GetY() + $this->hLine / 2;
        $this->SetXY($x, $y);
    }

    function BorderBottom()
    {
        $border = 0;
        if ($this->border == 1) {
            $border = "BLR";
        }
        $this->Cell($this->wLine, $this->bPadding, "", $border, 0, 'C', $this->fill);
    }

    function SetStyleWriteTag($tag, $family, $style, $size, $color, $indent = - 1)
    {
        $tag = trim($tag);
        $this->TagStyle[$tag]['family'] = trim($family);
        $this->TagStyle[$tag]['style'] = trim($style);
        $this->TagStyle[$tag]['size'] = trim($size);
        $this->TagStyle[$tag]['color'] = trim($color);
        $this->TagStyle[$tag]['indent'] = $indent;
    }

    function ShadowCell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '', $color = 'G', $distance = 0.5)
    {
        if ($color == 'G')
            $ShadowColor = 100;
        elseif ($color == 'B')
            $ShadowColor = 0;
        else
            $ShadowColor = $color;
        $TextColor = $this->TextColor;
        $x = $this->x - 3;
        $this->SetTextColor($ShadowColor);
        $this->Cell($w, $h, $txt, $border, 0, $align, $fill, $link);
        $this->TextColor = $TextColor;
        $this->x = $x;
        $this->y += $distance;
        $this->Cell($w, $h, $txt, 0, $ln, $align);
    }
}
