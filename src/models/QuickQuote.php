<?php namespace Tsawler\WheelsPackage;

use Illuminate\Database\Eloquent\Model;

/**
 * Class QuickQuote
 * @package Tsawler\WheelsPackage
 */
class QuickQuote extends Model
{

    public function vehicle()
    {
        return $this->hasOne('Tsawler\WheelsPackage\Vehicle', 'id', 'vehicle_id');
    }
}
