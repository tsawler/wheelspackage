<?php namespace Tsawler\WheelsPackage;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;

/**
 * Class RedirectIfNotTestDriveMiddleware
 * @package Tsawler\WheelsPackage
 */
class RedirectIfNotTestDriveMiddleware
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->hasRole('test_drive')) {
            return Redirect::to('/admin/unauthorized', 301);
        }

        return $next($request);
    }

}
