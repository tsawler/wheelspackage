<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinderTable extends Migration
{
    public function up()
    {
        Schema::create('finders', function ($table)
        {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('contact_method');
            $table->string('year')->nullable();
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->integer('processed')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('finders');
    }
}
