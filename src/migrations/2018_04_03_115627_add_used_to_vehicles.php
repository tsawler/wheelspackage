<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsedToVehicles extends Migration
{
    public function up()
    {
        Schema::table('vehicles', function($table) {
            $table->integer('used')->default(1);
        });
    }

    public function down()
    {

    }

}
