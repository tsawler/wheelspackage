<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVechicleImages extends Migration
{

    public function up()
    {
        Schema::create('vehicle_images', function ($table)
        {
            $table->increments('id');
            $table->integer('vehicle_id')->unsigned()->nullable();
            $table->text('image');
            $table->timestamps();

            $table->foreign('vehicle_id')
                ->references('id')
                ->on('vehicles')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    public function down()
    {
        Schema::drop('vehicle_images');
    }
}
