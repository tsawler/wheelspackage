<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVehiclesDropColumns extends Migration
{
    public function up()
    {
        Schema::table('vehicles', function($table) {
            $table->dropColumn(['make', 'model']);
        });
    }

    public function down()
    {

    }
}
