<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVechicleMakeTable extends Migration
{
    public function up()
    {
        Schema::create('vehicle_makes', function ($table)
        {
            $table->increments('id');
            $table->string('make');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('vehicle_makes');
    }
}
