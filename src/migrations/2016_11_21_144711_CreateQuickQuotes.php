<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuickQuotes extends Migration
{
    public function up()
    {
        Schema::create('quick_quotes', function ($table)
        {
            $table->increments('id');
            $table->string('users_name');
            $table->string('phone');
            $table->string('email');
            $table->integer('vehicle_id');
            $table->integer('processed')->default(0);
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('quick_quotes');
    }
}
