<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalToVehicles extends Migration
{
    public function up()
    {
        Schema::table('vehicles', function($table) {
            $table->integer('hand_picked')->default(0)->nullable();
        });
    }

    public function down()
    {

    }
}
