<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployees extends Migration
{
    public function up()
    {
        Schema::create('employees', function ($table)
        {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('image');
            $table->string('email')->nullable();
            $table->string('position')->nullable();
            $table->text('description')->nullable();
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('employees');
    }
}
