<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSortOrderToVehicleImages extends Migration
{
    public function up()
    {
        Schema::table('vehicle_images', function($table) {
            $table->integer('sort_order')->default(1)->nullable();
        });
    }

    public function down()
    {

    }
}
