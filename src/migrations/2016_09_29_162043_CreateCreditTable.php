<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditTable extends Migration
{
    public function up()
    {
        Schema::create('credit_applications', function ($table)
        {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('zip')->nullable();
            $table->string('vehicle')->nullable();
            $table->integer('processed')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('credit_applications');
    }
}
