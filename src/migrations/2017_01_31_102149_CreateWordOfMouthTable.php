<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordOfMouthTable extends Migration
{
    public function up()
    {
        Schema::create('words', function ($table)
        {
            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->integer('active')->default(1);
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('words');
    }
}
