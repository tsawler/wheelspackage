<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    public function up()
    {
        Schema::create('vehicles', function ($table)
        {
            $table->increments('id');
            $table->string('dealer_id')->nullable();
            $table->string('stock_no')->nullable();
            $table->string('stock_type')->nullable();
            $table->timestamp('inventory_date')->nullable();
            $table->integer('days_in_inventory')->nullable();
            $table->double('invoice_amount', 15,4)->nullable();
            $table->double('pack_amount', 15, 4)->nullable();
            $table->double('cost', 15, 4)->nullable();
            $table->double('retail', 15, 4)->nullable();
            $table->double('msrp', 15, 4)->nullable();
            $table->string('lot_location')->nullable();
            $table->integer('certified')->default(0);
            $table->string('vin');
            $table->bigInteger('odometer')->nullable();
            $table->integer('year')->nullable();
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->string('trim')->nullable();
            $table->string('vehicle_type')->nullable();
            $table->string('body')->nullable();
            $table->string('classification')->nullable();
            $table->string('payload_capacity')->nullable();
            $table->string('seating_capacity')->nullable();
            $table->string('wheel_base')->nullable();
            $table->string('drive_train')->nullable();
            $table->string('engine')->nullable();
            $table->string('exterior_color')->nullable();
            $table->string('interior_color')->nullable();
            $table->string('transmission')->nullable();
            $table->text('options')->nullable();
            $table->string('condition')->nullable();
            $table->string('tagline')->nullable();
            $table->string('certification_number')->nullable();
            $table->string('fld_holdback')->nullable();
            $table->string('license_plate')->nullable();
            $table->string('registration_state')->nullable();
            $table->string('registration_expiry')->nullable();
            $table->string('model_number')->nullable();
            $table->double('reconditioning_cost', 15, 4)->nullable();
            $table->double('internet_price', 15, 4)->nullable();
            $table->string('blank')->nullable();
            $table->double('total_option_cost', 15, 4)->nullable();
            $table->double('total_option_price', 15, 4)->nullable();
            $table->double('total_msr', 15, 4)->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('vehicles');
    }
}
