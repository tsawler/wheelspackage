<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    public function up()
    {
        Schema::create('options', function ($table)
        {
            $table->increments('id');
            $table->string('option_name');
            $table->integer('active')->default(1);
            $table->timestamps();


        });
    }

    public function down()
    {
        Schema::drop('options');
    }
}
