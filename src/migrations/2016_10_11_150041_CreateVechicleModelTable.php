<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVechicleModelTable extends Migration
{
    public function up()
    {
        Schema::create('vehicle_models', function ($table)
        {
            $table->increments('id');
            $table->string('model');
            $table->integer('vehicle_makes_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('vehicle_makes_id')
                ->references('id')
                ->on('vehicle_makes')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::drop('vehicle_models');
    }
}
