<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToPowerSPorts extends Migration
{
    public function up()
    {
        Schema::table('power_sports', function($table) {
            $table->string('category')->default('');
        });
    }

    public function down()
    {

    }
}
