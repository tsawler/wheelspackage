<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePowerSports extends Migration
{
    public function up()
    {
        Schema::create('power_sports', function ($table)
        {
            $table->increments('id');
            $table->string('product_name');
            $table->integer('year');
            $table->text('description');
            $table->string('url');
            $table->string('images');
            $table->string('msrp');

            $table->timestamps();


        });
    }

    public function down()
    {
        Schema::drop('power_sports');
    }
}
