<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevisedVehicleTable extends Migration
{
    public function up()
    {
        Schema::create('vehicles', function ($table)
        {
            $table->increments('id');
            $table->string('stock_no')->nullable();
            $table->string('stock_type')->nullable();
            $table->timestamp('inventory_date')->nullable();
            $table->double('cost', 15, 4)->nullable();
            $table->string('vin');
            $table->bigInteger('odometer')->nullable();
            $table->integer('year')->nullable();
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->string('trim')->nullable();
            $table->string('vehicle_type')->nullable();
            $table->string('body')->nullable();
            $table->string('seating_capacity')->nullable();
            $table->string('drive_train')->nullable();
            $table->string('engine')->nullable();
            $table->string('exterior_color')->nullable();
            $table->string('interior_color')->nullable();
            $table->string('transmission')->nullable();
            $table->text('options')->nullable();
            $table->string('model_number')->nullable();
            $table->double('internet_price', 15, 4)->nullable();
            $table->double('total_msr', 15, 4)->nullable();
            $table->integer('status')->default(1);
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('vehicles');
    }
}
