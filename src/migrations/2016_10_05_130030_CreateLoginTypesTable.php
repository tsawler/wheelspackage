<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginTypesTable extends Migration
{
    public function up()
    {
        Schema::create('login_types', function ($table)
        {
            $table->increments('id');
            $table->string('login_type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('login_types');
    }
}
