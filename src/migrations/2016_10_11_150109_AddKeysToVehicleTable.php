<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeysToVehicleTable extends Migration
{
    public function up()
    {
        Schema::table('vehicles', function($table) {
            $table->integer('vehicle_makes_id')->unsigned()->nullable();
            $table->integer('vehicle_models_id')->unsigned()->nullable();

            $table->foreign('vehicle_makes_id')
                ->references('id')
                ->on('vehicle_makes')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->foreign('vehicle_models_id')
                ->references('id')
                ->on('vehicle_models')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    public function down()
    {

    }
}
