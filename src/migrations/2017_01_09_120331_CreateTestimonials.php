<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonials extends Migration
{
    public function up()
    {
        Schema::create('testimonials', function ($table)
        {
            $table->increments('id');
            $table->string('label');
            $table->text('url');
            $table->integer('active')->default(1);
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('testimonials');
    }
}
