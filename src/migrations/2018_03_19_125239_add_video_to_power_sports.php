<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVideoToPowerSports extends Migration
{
    public function up()
    {
        Schema::table('power_sports', function($table) {
            $table->integer('video_id')->unsigned()->nullable();

            $table->foreign('video_id')
                ->references('id')
                ->on('videos')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {

    }
}
