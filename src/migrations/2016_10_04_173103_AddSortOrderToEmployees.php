<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSortOrderToEmployees extends Migration
{
    public function up()
    {
        Schema::table('employees', function($table) {
            $table->integer('sort_order')->default(0);
        });
    }

    public function down()
    {

    }
}
