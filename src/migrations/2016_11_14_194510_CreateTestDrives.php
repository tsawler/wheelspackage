<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestDrives extends Migration
{
    public function up()
    {
        Schema::create('test_drives', function ($table)
        {
            $table->increments('id');
            $table->string('users_name');
            $table->string('phone');
            $table->string('email');
            $table->string('preferred_date');
            $table->string('preferred_time');
            $table->integer('vehicle_id');
            $table->integer('processed')->default(0);
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('test_drives');
    }
}
