<?php namespace Tsawler\WheelsPackage;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

/**
 * Class WheelsPackageServiceProvider
 * @package Tsawler\WheelsPackage;
 */
class WheelsPackageServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *c
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->loadViewsFrom(__DIR__ . '/views', 'wheelspackage');

        $this->publishes([
            __DIR__ . '/public' => public_path('vendor/wheelspackage'),
        ], 'public');

        // add custom middleware
        $this->app['router']->aliasMiddleware('auth.inventory', RedirectIfNotInventoryMiddleware::class);
        $this->app['router']->aliasMiddleware('auth.members', RedirectIfNotMembersMiddleware::class);
        $this->app['router']->aliasMiddleware('auth.credit', RedirectIfNotCreditMiddleware::class);
        $this->app['router']->aliasMiddleware('auth.email', RedirectIfNotEmailMiddleware::class);
        $this->app['router']->aliasMiddleware('auth.finder', RedirectIfNotFinderMiddleware::class);
        $this->app['router']->aliasMiddleware('auth.staff', RedirectIfNotStaffMiddleware::class);
        $this->app['router']->aliasMiddleware('auth.test_drive', RedirectIfNotStaffMiddleware::class);
        $this->app['router']->aliasMiddleware('auth.word', RedirectIfNotWordofMouthMiddleware::class);

        // register the load-inventory command
        $this->commands([
            \Tsawler\WheelsPackage\CleanPanoramas::class,
            \Tsawler\WheelsPackage\CleanVideos::class,
            \Tsawler\WheelsPackage\CleanImages::class,
            \Tsawler\WheelsPackage\LoadInventory::class,
            \Tsawler\WheelsPackage\VehiclesKijiji::class,
            \Tsawler\WheelsPackage\PowersportsKijiji::class,
            \Tsawler\WheelsPackage\CarGuru::class,
//            \Tsawler\WheelsPackage\PowerSportsAutoTrader::class,
        ]);

        // wait until application is booted before adding scheduled tasks
        if (env('DEBUG') == false) {
            $this->app->booted(function () {
                $schedule = $this->app->make(Schedule::class);
                $schedule->command('load-inventory')->everyFiveMinutes();
                $schedule->command('kijiji-vehicles')->dailyAt('22:00');
                $schedule->command('clean-images')->dailyAt('2:00');
                $schedule->command('clean-videos')->dailyAt('2:30');
                $schedule->command('clean-panoramas')->dailyAt('3:00');
                $schedule->command('kijiji-powersports')->dailyAt('23:00');
                $schedule->command('carguru-vehicles')->dailyAt('23:30');
//                $schedule->command('autotrader-powersports')->dailyAt('23:30');
            });
        }

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

}
