<?php namespace Tsawler\WheelsPackage;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

/**
 * Class FileUploader
 * @package Tsawler\Wheelspackage
 */
class FileUploader
{

    /**
     * @param $field_name
     * @param $req_height
     * @param $req_width
     * @param $dir
     * @param int $thumb_height
     * @param int $thumb_width
     * @return array
     */
    static function uploadFile($field_name, $req_height, $req_width,
                               $dir, $thumb_height = 100, $thumb_width = 100)
    {
        $success = [];
        $okay = true;

        $destinationPath = public_path() . '/storage/' . $dir . "/";

        $file = Input::file($field_name);
        $filename = $file->getClientOriginalName();
        $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
        $filename_save = Str::slug($withoutExt) . ".jpg";

        if (!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath);
        }

        $upload_success = Input::file($field_name)->move($destinationPath, $filename_save);

        if (!File::exists($destinationPath . "thumbs")) {
            File::makeDirectory($destinationPath . "thumbs");
        }
        $thumb_img = Image::make($destinationPath . $filename_save);

        $thumb_img->fit($thumb_width, $thumb_height)
            ->save($destinationPath . "thumbs/" . $filename_save);

        $img = Image::make($destinationPath . $filename_save);
        $height = $img->height();
        $width = $img->width();


        if (($height < $req_height) || ($width < $req_width)) {
            File::delete($destinationPath . $filename_save);
            $success = [
                'response'  => false,
                'message'   => 'Your image is too small. It must be at least '
                    . $req_width
                    . ' pixels wide and '
                    . $req_height
                    . ' pixels tall!',
                'file_name' => '',
            ];
            $okay = false;
        }


        if ($okay) {
            if (($width > $req_width) || ($height > $req_height)) {
                // this image is very large; we'll need to resize it.
                $img = $img->fit($req_width, $req_height);
                $img->save();
            }
        }

        if (($upload_success) && ($okay)) {
            $success = [
                'response'  => true,
                'message'   => 'Changes saved',
                'file_name' => $filename_save,
            ];
        } else if (($upload_success) && ($okay == false)) {
            // do nothing -- we have our appropriate error message
        } else {
            $success = [
                'response'  => false,
                'message'   => 'There was a problem with your image. Changes not saved!',
                'file_name' => '',
            ];
        }

        return $success;
    }


    /**
     * @param $field_name
     * @param $dir
     * @return array
     */
    static function uploadRegularFile($field_name, $dir, $name = null)
    {
        $success = [];
        $okay = true;

        $destinationPath = base_path() . '/' . $dir . "/";
        if (! File::exists($destinationPath))
            $result = File::makeDirectory($destinationPath, 0775, true);

        $file = Input::file($field_name);
        $filename = $file->getClientOriginalName();
        $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
        $extension = $file->getClientOriginalExtension();

        $filename_save = Str::slug($withoutExt) . "." . $extension;

        if ($name == null)
            $upload_success = Input::file($field_name)->move($destinationPath, $filename_save);
        else
            $upload_success = Input::file($field_name)->move($destinationPath, $name);

        if (($upload_success) && ($okay)) {
            $success = [
                'response'  => true,
                'message'   => 'Changes saved',
                'file_name' => $filename_save,
            ];
        } else if (($upload_success) && ($okay == false)) {
            // do nothing -- we have our appropriate error message
        } else {
            $success = [
                'response'  => false,
                'message'   => 'There was a problem with your upload. Changes not saved!',
                'file_name' => '',
            ];
        }

        return $success;
    }


    static function uploadFileOnCanvas($field_name, $req_height, $req_width,
                                       $dir, $thumb_height = 100, $thumb_width = 100, $canvas_height = 480, $canvas_width = 1200)
    {
        $success = [];
        $okay = true;
        $uploadCount = 0;
        $uploaded_files = Input::file($field_name);
        $files = array_reverse($uploaded_files);
        $file_count = count($files);
        $filename_save = [];

        foreach ($files as $file) {
            $destinationPath = public_path() . '/storage/' . $dir . "/";
            $filename = $file->getClientOriginalName();
            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
            $filename_to_save = Str::slug($withoutExt) . ".jpg";

            $upload_success = $file->move($destinationPath, $filename_to_save);

            if (!File::exists($destinationPath . "thumbs")) {
                File::makeDirectory($destinationPath . "thumbs");
            }
            $thumb_img = Image::make($destinationPath . $filename_to_save);

            $thumb_img->fit($thumb_width, $thumb_height)
                ->save($destinationPath . "thumbs/" . $filename_to_save);

            $img = Image::make($destinationPath . $filename_to_save);
            $height = $img->height();
            $width = $img->width();

            if (($height < $req_height) || ($width < $req_width)) {
                File::delete($destinationPath . $filename_to_save);
                $success = [
                    'response'  => false,
                    'message'   => 'Your image is too small. It must be at least '
                        . $req_width
                        . ' pixels wide and '
                        . $req_height
                        . ' pixels tall!',
                    'file_name' => '',
                ];
                $okay = false;
            }

            if ($okay) {
                if (($width > $req_width) || ($height > $req_height)) {
                    // this image is very large; we'll need to resize it.
                    $img = $img->resize($req_width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $final_image = $img;
                }

                $final_image->save($destinationPath . $filename_to_save);
                $filename_save[] = $filename_to_save;
            }
        }


        if (($upload_success) && ($okay)) {
            $success = [
                'response'  => true,
                'message'   => 'Changes saved',
                'file_name' => $filename_save,
            ];
        } else if (($upload_success) && ($okay == false)) {
            // do nothing -- we have our appropriate error message
        } else {
            $success = [
                'response'  => false,
                'message'   => 'There was a problem with your image. Changes not saved!',
                'file_name' => '',
            ];
        }

        return $success;
    }
}
