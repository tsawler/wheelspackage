<?php
namespace Tsawler\WheelsPackage;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Contracts\Factory as Socialite;

/**
 * Class AuthenticateUserSocial
 * @package App
 */
class AuthenticateUserSocial
{

    private $login_type = [
        'facebook' => '2',
        'twitter'  => '3',
        'google'   => '4',
        'github'   => '5',
    ];

    /**
     * @var Socialite
     */
    private $socialite;

    /**
     * AuthenticateUserSocial constructor.
     * @param Socialite $socialite
     */
    public function __construct(Socialite $socialite)
    {
        $this->socialite = $socialite;
    }


    /**
     * @param $hasCode
     * @param $api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function execute($hasCode, $api)
    {

        if (!$hasCode)
            return $this->getAuthorizationFirst($api);

        $response = $this->getOrCreateUser($this->socialite->driver($api)->user(), $api);

        if ($response['success'] === false) {
            return Redirect::to('/user/login')
                ->with('error', $response['error']);
        } else {
            $user = $response['user'];
            Auth::login($user, true);

            return $this->redirectToHome();
        }
    }


    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getAuthorizationFirst($api)
    {
        return $this->socialite->driver($api)->redirect();
    }


    /**
     * @param $userData
     * @return mixed
     */
    public function getOrCreateUser($userData, $api)
    {
        $name = $userData->name;
        $exploded = explode(" ", $name);
        $first_name = $exploded[0];

        if (isset($exploded[1]))
            $last_name = $exploded[1];
        else
            $last_name = " ";

        if (User::where('email', '=', $userData->email)->where('login_types_id', '<>', $this->login_type[$api])->exists()) {
            $existing_acccount = User::where('email', '=', $userData->email)->where('login_types_id', '<>', $this->login_type[$api])->first();
            $error_string = "There is already a "
                . $existing_acccount->loginType->login_type
                . " account registered on this system! Please login using "
                . $existing_acccount->loginType->login_type
                . ".";

            return [
                'success' => false,
                'error'   => $error_string,
            ];
        }

        $user = User::firstOrCreate([
            'email'          => $userData->email,
            'first_name'     => $first_name,
            'last_name'      => $last_name,
            'access_level'   => 1,
            'user_active'    => 1,
            'login_types_id' => $this->login_type[$api],
        ]);

        // send welcome email
        $mail = Email::find(1);
        $content = $mail->message;
        $subject = $mail->subject;

        $data = [
            'name'  => $first_name,
            'content' => $content,
            'subject' => $subject,
        ];

        Mail::to($userData->email)
            ->queue(new GenericMailable($data));

        return [
            'success' => true,
            'user'    => $user,
        ];
    }


    /**
     * @return mixed
     */
    public function redirectToHome()
    {
        return Redirect::to('/')
            ->with('message', 'Welcome back ' . Auth::user()->first_name);
    }
}
