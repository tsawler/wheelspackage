<?php

namespace Tsawler\WheelsPackage;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;


class LoadInventory extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load-inventory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load inventory from PBS';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // get ids of all options
        $results = \Tsawler\WheelsPackage\Option::all();
        $current_options = [];

        foreach ($results as $option) {
            $current_options[] = $option->id;
        }

        // add default options
        $default = [
            33,
            34,
            35,
            36,
            37,
            45,
            62,
            63,
            65,
            75,
            76,
            80,
            81,
        ];
        // remove any options that are not currently active
        foreach ($default as $index => $value) {
            if (!in_array($value, $current_options)) {
                unset($default[$index]);
            }
        }

        $counter = 0;

        $time = date('Y-m-d', strtotime('-74 hours')) . "T" . date('H:i:s', strtotime('-74 hours')) . '.0000000Z';

        $arr = [
            "SerialNumber"         => '2675',
            "Year"                 => "",
            "Status"               => "Used",
            "IncludeInactive"      => false,
            "IncludeBuildVehicles" => false,
            "ModifiedSince"        => $time,
            'ModifiedUntil'        => date('Y-m-d' . 'T' . date('H:i:s') . '.0000000Z'),
        ];

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
        ]);

        $response = $client->request('POST', 'https://partnerhub.pbsdealers.com/api/json/reply/VehicleGet',
            [
                'auth' => ['PBSVERL', 'D8H3GRT56'],
                'json' => $arr,
            ]);

        $json = $response->getBody()->getContents();

        $cars = json_decode($json);

        Log::info("Forced");

        foreach ($cars as $car) {
            foreach ($car as $current) {
                $vehicle_makes_id = 0;
                $vin = $current->VIN;
                $test = \Tsawler\WheelsPackage\Vehicle::where('VIN', '=', strtoupper($vin))->get();
                if (count($test) == 0) {
                    Log::info("Doing new vehicle");

                    // this is a new vehicle
                    $counter = $counter + 1;
                    $v = new Vehicle();
                    $v->stock_no = $current->StockNumber;
                    $v->cost = $current->Retail;
                    $v->vin = $current->VIN;
                    $v->odometer = $current->Odometer;
                    $v->year = $current->Year;
                    $v->used = 1;
                    $make = $current->Make;
                    $model = $current->Model;

                    // check to see if we have this make
                    $test_make = VehicleMake::where('make', '=', $make)->get();
                    if (count($test_make) === 0) {
                        // this is a new make; create it and get its id
                        $new_make = new VehicleMake();
                        $new_make->make = $make;
                        $new_make->save();
                        $v->vehicle_makes_id = $new_make->id;
                        $vehicle_makes_id = $new_make->id;
                    } else {
                        // have this make already
                        $current_make = VehicleMake::where('make', '=', $make)->first();
                        $v->vehicle_makes_id = $current_make->id;
                        $vehicle_makes_id = $current_make->id;
                    }

                    Log::info("Make ID is " . $vehicle_makes_id);

                    // check to see if we have this model
                    $test_model = VehicleModel::where('model', '=', $model)->get();
                    if (count($test_model) === 0) {
                        // this is a new model; create and save it.
                        $new_model = new VehicleModel();
                        $new_model->model = $model;
                        $new_model->vehicle_makes_id = $vehicle_makes_id;
                        $new_model->save();
                        $v->vehicle_models_id = $new_model->id;
                    } else {
                        // have this model already
                        $current_model = VehicleModel::where('model', '=', $model)->first();
                        $v->vehicle_models_id = $current_model->id;
                    }
                    $v->trim = $current->Trim;
                    $type = $current->VehicleType;

                    if (strtoupper($type) == 'CAR') {
                        $v->vehicle_type = 1;
                    } else if (strtoupper($type) == 'P') {
                        $v->vehicle_type = 1;
                    } else if (strtoupper($type) == 'PASSENGER') {
                        $v->vehicle_type = 1;
                    } else if (strtoupper($type) == 'T') {
                        $v->vehicle_type = 2;
                    } else if (strtoupper($type) == 'TRUCK') {
                        $v->vehicle_type = 2;
                    } else {
                        $v->vehicle_type = 3;
                    }

                    $v->body = '';
                    $v->seating_capacity = '';
                    $v->drive_train = $current->DriveWheel;
                    $v->engine = $current->Engine;
                    if (isset($current->ExteriorColor->Description)) {
                        $v->exterior_color = $current->ExteriorColor->Description;
                    }
                    if (isset($current->InteriorColor->Description)) {
                        $v->interior_color = $current->InteriorColor->Description;
                    }
                    $v->transmission = $current->Transmission;
                    $v->status = 2;
                    $v->description = "Factory Warranty Plus Our 12 Month Huggable Guarantee!! COMPARE AT NEW MSRP \"Pay Less-Owe Less\"";
                    $v->save();
                    $vid = $v->id;

                    foreach ($default as $current) {
                        $o = new VehicleOption();
                        $o->vehicle_id = $vid;
                        $o->option_id = $current;
                        $o->save();
                    }

                }
            }
        }


        $arr = [
            "SerialNumber"         => '2675',
            "Year"                 => "",
            "Status"               => "New",
            "IncludeInactive"      => false,
            "IncludeBuildVehicles" => false,
            "ModifiedSince"        => $time,
            'ModifiedUntil'        => date('Y-m-d' . 'T' . date('H:i:s') . '.0000000Z'),
        ];

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
        ]);

        $response = $client->request('POST', 'https://partnerhub.pbsdealers.com/api/json/reply/VehicleGet',
            [
                'auth' => ['PBSVERL', 'D8H3GRT56'],
                'json' => $arr,
            ]);

        $json = $response->getBody()->getContents();

        $cars = json_decode($json);

        foreach ($cars as $car) {
            foreach ($car as $current) {
                $vehicle_makes_id = 0;
                $vin = $current->VIN;
                $test = \Tsawler\WheelsPackage\Vehicle::where('VIN', '=', strtoupper($vin))->get();
                if (count($test) == 0) {
                    // this is a new vehicle
                    $counter = $counter + 1;
                    $v = new Vehicle();
                    $v->stock_no = $current->StockNumber;
                    $v->cost = $current->Retail;
                    $v->vin = $current->VIN;
                    $v->odometer = $current->Odometer;
                    $v->year = $current->Year;
                    $v->used = 0;
                    $make = $current->Make;
                    $model = $current->Model;

                    // check to see if we have this make
                    $test_make = VehicleMake::where('make', '=', $make)->get();
                    if (count($test_make) === 0) {
                        // this is a new make; create it and get its id
                        $new_make = new VehicleMake();
                        $new_make->make = $make;
                        $new_make->save();
                        $v->vehicle_makes_id = $new_make->id;
                        $vehicle_makes_id = $new_make->id;
                    } else {
                        // have this make already
                        $current_make = VehicleMake::where('make', '=', $make)->first();
                        $v->vehicle_makes_id = $current_make->id;
                        $vehicle_makes_id = $current_make->id;
                    }

                    // check to see if we have this model
                    $test_model = VehicleModel::where('model', '=', $model)->get();
                    if (count($test_model) === 0) {
                        // this is a new model; create and save it.
                        $new_model = new VehicleModel();
                        $new_model->model = $model;
                        $new_model->vehicle_makes_id = $vehicle_makes_id;
                        $new_model->save();
                        $v->vehicle_models_id = $new_model->id;
                    } else {
                        // have this model already
                        $current_model = VehicleModel::where('model', '=', $model)->first();
                        $v->vehicle_models_id = $current_model->id;
                    }
                    $v->trim = $current->Trim;
                    $type = $current->VehicleType;

                    if (strtoupper($type) == 'CAR') {
                        $v->vehicle_type = 1;
                    } else if (strtoupper($type) == 'P') {
                        $v->vehicle_type = 1;
                    } else if (strtoupper($type) == 'PASSENGER') {
                        $v->vehicle_type = 1;
                    } else if (strtoupper($type) == 'T') {
                        $v->vehicle_type = 2;
                    } else if (strtoupper($type) == 'TRUCK') {
                        $v->vehicle_type = 2;
                    } else {
                        $v->vehicle_type = 3;
                    }

                    $v->body = '';
                    $v->seating_capacity = '';
                    $v->drive_train = $current->DriveWheel;
                    $v->engine = $current->Engine;
                    if (isset($current->ExteriorColor->Description)) {
                        $v->exterior_color = $current->ExteriorColor->Description;
                    }
                    if (isset($current->InteriorColor->Description)) {
                        $v->interior_color = $current->InteriorColor->Description;
                    }
                    $v->transmission = $current->Transmission;
                    $v->status = 2;
                    $v->description = "Factory Warranty Plus Our 12 Month Huggable Guarantee!! COMPARE AT NEW MSRP \"Pay Less-Owe Less\"";
                    $v->save();
                    $vid = $v->id;

                    foreach ($default as $current) {
                        $o = new VehicleOption();
                        $o->vehicle_id = $vid;
                        $o->option_id = $current;
                        $o->save();
                    }

                }
            }
        }

        print "Done loading inventory. $counter vehicles added.\n";
    }
}
