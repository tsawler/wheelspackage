<?php

namespace Tsawler\WheelsPackage;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class PowerSportsAutoTrader extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autotrader-powersports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Powersports Feed For Autotrader';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

//        $query = "
//        select
//			date_format(v.updated_at, '%m/%d/%Y %H:%i:%s') as AdLastModifiedDate,
//            20180622174205686 as 3rdPartyADID,
//            case
//            when vehicle_type = 7 and v.vehicle_models_id in (225, 380, 341) then 48
//            when vehicle_type = 7 and v.vehicle_models_id = 223 then 57
//            when vehicle_type = 7 and v.vehicle_models_id in (342,228,227,232) then 43
//            when vehicle_type = 7 and v.vehicle_models_id not in(225, 380, 341, 223,342,228,227,232) then 41
//            when vehicle_type = 8 then 9300
//            when vehicle_type = 11 then 9300
//            when vehicle_type = 12 then 9300
//            when vehicle_type = 13 then 23
//            when vehicle_type = 15 then 4732
//            when vehicle_type = 10 then 71
//            when vehicle_type = 9 then 9261
//            end as CategoryID,
//            stock_no as StockNumber,
//            vin as Vin,
//            case when used = 1 then 'used' else 'new' end as Status,
//            year as Year,
//            vm.make as Make,
//            vmod.model as Model,
//            v.trim as Trim,
//            v.odometer as KMS,
//            v.exterior_color as 'Exterior Color',
//            v.interior_color as 'Interior Color',
//
//            v.engine as EngineSize,
//            v.transmission as Transmission,
//            v.cost as Price,
//            REPLACE(strip_tags(v.description), '&nbsp;', ' ') as Description,
//
//            case
//            when (select count(id) from vehicle_images vi where vi.vehicle_id = v.id) = 0 then ''
//            else
//           	(select CONCAT('https://www.wheelsanddeals.ca/storage/inventory/', v.id, '/',vimages.image) from vehicle_images vimages where vimages.vehicle_id = v.id limit 1)
//            end as MainPhoto,
//            date_format(v.updated_at, '%m/%d/%Y %H:%i:%s') as MainPhotoLastModifiedDate,
//
//            case
//            when (select count(id) from vehicle_images vi where vi.vehicle_id = v.id) = 0 then ''
//            else
//           	(select GROUP_CONCAT(CONCAT('https://www.wheelsanddeals.ca/storage/inventory/', v.id, '/',vimages.image) SEPARATOR ';') from vehicle_images vimages where vimages.vehicle_id = v.id)
//            end as ExtraPhotos,
//            date_format(v.updated_at, '%m/%d/%Y %H:%i:%s') as ExtraphotoLastModifiedDate,
//            v.total_msr as MSRP,
//            case
//            when vehicle_type = 7 then concat('https://www.wheelsanddeals.ca/vehicles/motorcycle/', v.id)
//            when vehicle_type = 8 then concat('https://www.wheelsanddeals.ca/vehicles/motorcycle/', v.id)
//            when vehicle_type = 11 then concat('https://www.wheelsanddeals.ca/vehicles/motorcycle/', v.id)
//            when vehicle_type = 12 then concat('https://www.wheelsanddeals.ca/vehicles/motorcycle/', v.id)
//            when vehicle_type = 15 then concat('https://www.wheelsanddeals.ca/vehicles/boats/', v.id)
//            when vehicle_type = 9 then concat('https://www.wheelsanddeals.ca/vehicles/boats/', v.id)
//            when vehicle_type = 10 then concat('https://www.wheelsanddeals.ca/vehicles/boats/', v.id)
//            when vehicle_type = 13 then concat('https://www.wheelsanddeals.ca/vehicles/boats/', v.id)
//            end as InventoryURL
//
//
//        from vehicles v
//        left join vehicle_makes vm on (v.vehicle_makes_id = vm.id)
//        left join vehicle_models vmod on (v.vehicle_models_id = vmod.id)
//
//        where v.status = 1 and v.vehicle_type > 6 and v.vehicle_type <> 14
//        ";

        $query = "
        select
			date_format(v.updated_at, '%m/%d/%Y %H:%i:%s') as AdLastModifiedDate,
            20180622174205686 as 3rdPartyADID, 
            case 
            when vehicle_type = 7 and v.vehicle_models_id in (225, 380, 341) then 48
            when vehicle_type = 7 and v.vehicle_models_id = 223 then 57
            when vehicle_type = 7 and v.vehicle_models_id in (342,228,227,232) then 43
            when vehicle_type = 7 and v.vehicle_models_id not in(225, 380, 341, 223,342,228,227,232) then 41
            when vehicle_type = 8 then 9300
            when vehicle_type = 11 then 9300
            when vehicle_type = 12 then 9300
            when vehicle_type = 13 then 23
            when vehicle_type = 15 then 4732
            when vehicle_type = 10 then 71
            when vehicle_type = 9 then 9261
            end as CategoryID,
            stock_no as StockNumber,
            vin as Vin,
            case when used = 1 then 'used' else 'new' end as Status,
            year as Year,
            vm.make as Make,
            vmod.model as Model,
            v.trim as Trim,
            v.odometer as KMS,
            v.exterior_color as 'Exterior Color',
            v.interior_color as 'Interior Color',
            
            v.engine as EngineSize,
            v.transmission as Transmission,
            v.cost as Price,
            v.description as Description, 
            
            case 
            when (select count(id) from vehicle_images vi where vi.vehicle_id = v.id) = 0 then ''
            else
           	(select CONCAT('https://www.wheelsanddeals.ca/storage/inventory/', v.id, '/',vimages.image) from vehicle_images vimages where vimages.vehicle_id = v.id limit 1) 
            end as MainPhoto,
            date_format(v.updated_at, '%m/%d/%Y %H:%i:%s') as MainPhotoLastModifiedDate,
            
            case 
            when (select count(id) from vehicle_images vi where vi.vehicle_id = v.id) = 0 then ''
            else
           	(select GROUP_CONCAT(CONCAT('https://www.wheelsanddeals.ca/storage/inventory/', v.id, '/',vimages.image) SEPARATOR ';') from vehicle_images vimages where vimages.vehicle_id = v.id) 
            end as ExtraPhotos,
            date_format(v.updated_at, '%m/%d/%Y %H:%i:%s') as ExtraphotoLastModifiedDate,
            v.total_msr as MSRP,
            case
            when vehicle_type = 7 then concat('https://www.wheelsanddeals.ca/vehicles/motorcycle/', v.id) 
            when vehicle_type = 8 then concat('https://www.wheelsanddeals.ca/vehicles/motorcycle/', v.id) 
            when vehicle_type = 11 then concat('https://www.wheelsanddeals.ca/vehicles/motorcycle/', v.id) 
            when vehicle_type = 12 then concat('https://www.wheelsanddeals.ca/vehicles/motorcycle/', v.id) 
            when vehicle_type = 15 then concat('https://www.wheelsanddeals.ca/vehicles/boats/', v.id) 
            when vehicle_type = 9 then concat('https://www.wheelsanddeals.ca/vehicles/boats/', v.id) 
            when vehicle_type = 10 then concat('https://www.wheelsanddeals.ca/vehicles/boats/', v.id) 
            when vehicle_type = 13 then concat('https://www.wheelsanddeals.ca/vehicles/boats/', v.id) 
            end as InventoryURL
            
        
        from vehicles v
        left join vehicle_makes vm on (v.vehicle_makes_id = vm.id)
        left join vehicle_models vmod on (v.vehicle_models_id = vmod.id)
        
        where v.status = 1 and v.vehicle_type > 6 and v.vehicle_type <> 14
        ";

        $r = DB::select($query);
        $results = json_decode(json_encode($r), true);
        $formatter = \SoapBox\Formatter\Formatter::make($results, \SoapBox\Formatter\Formatter::ARR);
        $csv = $formatter->toCsv();

        \File::put(storage_path("app/" . date("Y-m-d")."_powersports_wheelsanddeals_autotrader.csv"), $csv);

        $localFile = \File::get(storage_path("app/" . date("Y-m-d")."_powersports_wheelsanddeals_autotrader.csv"));
        \Storage::disk('ftp_at')->put('CACI_Verillon.csv', $localFile);
        print "\nPut file via ftp\n";
        print "\nDone\n";
    }
}
