<?php

namespace Tsawler\WheelsPackage;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class PowersportsKijiji extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kijiji-powersports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Powersports Feed For Kijiji';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $query = "
        select
            86450547 as dealer_id, 
            'Jim Gilbert\'s Wheels and Deals' as dealer_name,
            '402 St. Mary\'s Street, Fredericton NB' as address,
            '5064596832' as phone,
            'E3A 8H5' as postalcode,
            'salesmanager@wheelsanddeals.ca' as email,
            v.id, vin, stock_no as stockid, engine, used as is_used,  year,
            vm.make, vmod.model,  v.body, v.trim, v.transmission, v.odometer as kilometers,
            v.exterior_color, v.cost as price, '' as model_code,
            REPLACE(strip_tags(v.description), '&nbsp;', ' ') as comments, 
            v.drive_train as drivetrain, '' as video_url,
            case 
            when (select count(id) from vehicle_images vi where vi.vehicle_id = v.id) = 0 then ''
            else
           	(select GROUP_CONCAT(CONCAT('https://www.wheelsanddeals.ca/storage/inventory/', v.id, '/',vimages.image) SEPARATOR '|') from vehicle_images vimages where vimages.vehicle_id = v.id order by sort_order) 
            end as images,
            case 
            when vehicle_type = 7 and v.vehicle_models_id in (225, 380, 341)  then 303
            when vehicle_type = 7 and v.vehicle_models_id = 223 then 304
            when vehicle_type = 7 and v.vehicle_models_id in (342,228,227,232) then 307
            when vehicle_type = 7 and v.vehicle_models_id not in(225, 380, 341, 223,342,228,227,232) then 306
            
            when vehicle_type = 8 then 311
            when vehicle_type = 11 then 311
            when vehicle_type = 12 then 311
            when vehicle_type = 13 then 330
            when vehicle_type = 15 then 327
            when vehicle_type = 10 then 327
            when vehicle_type = 15 then 327
            when vehicle_type = 9 then 327
            when vehicle_type = 16 then 308
            when vehicle_type = 17 then 308
            
            end as category
        
        from vehicles v
        left join vehicle_makes vm on (v.vehicle_makes_id = vm.id)
        left join vehicle_models vmod on (v.vehicle_models_id = vmod.id)
        
        where v.status = 1 and v.vehicle_type > 6 and v.vehicle_type <> 14
        ";
        $r = DB::select($query);
        $results = json_decode(json_encode($r), true);
        $formatter = \SoapBox\Formatter\Formatter::make($results, \SoapBox\Formatter\Formatter::ARR);
        $csv = $formatter->toCsv();

        \File::put(storage_path("app/" . date("Y-m-d")."_powersports_wheelsanddeals.csv"), $csv);

        $localFile = \File::get(storage_path("app/" . date("Y-m-d")."_powersports_wheelsanddeals.csv"));
        \Storage::disk('ftp_rec')->put('Kijiji.csv', $localFile);
        print "\nPut file via ftp\n";
        print "\nDone\n";

        print "\nDone\n";
    }
}
