<?php

namespace Tsawler\WheelsPackage;

use GuzzleHttp\Client;
use Illuminate\Console\Command;


class CleanImages extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove images for sold vehicles';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(7200);
        $results = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
            select id from vehicles where status = 0;
        "));
        print "Starting database clean...\n";
        foreach($results as $vehicle) {
            $images = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
               select id, image from vehicle_images where vehicle_id = " . $vehicle->id));
            // delete it's images
            foreach($images as $image) {
                // delete the record
                \Illuminate\Support\Facades\DB::table('vehicle_images')->where('id','=', $image->id)->delete();
            }
        }
        print "...database clean finished.\n";

        // now delete directories/files
        $r = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
            select id from vehicles where status = 0;
        "));

        print "Deleting directory and images...\n";
        foreach($r as $v) {

            \Storage::deleteDirectory(base_path() . "/storage/app/public/inventory/" . $v->id);
        }
        print "...done.\n";
        print "Complete.\n";

    }
}
