<?php

namespace Tsawler\WheelsPackage;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class CarGuru extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'carguru-vehicles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Feed For CarGurus';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $query = "
       select
            437564 as dealer_id, 
            'Jim Gilbert\'s Wheels and Deals' as dealer_name,
            '402 St. Mary\'s Street, Fredericton NB' as address,
            'New Brunswick' as dealer_state,
            'E3A 8H5' as dealer_zip,
            '5064596832' as dealer_phone_number,
            'salesmanager@wheelsanddeals.ca' as dealer_crm_email,
            v.vin,
            vm.make,
            vmod.model,
            v.year,
            v.trim,
            (select group_concat(o.option_name SEPARATOR ', ') from options o where o.id in (select option_id from vehicle_options where vehicle_id = v.id)) as options,
            v.cost as price,
            v.total_msr as msrp,
            1 as certified,
            v.odometer as mileage,
            REPLACE(strip_tags(v.description), '&nbsp;', ' ') as dealer_comments,
            stock_no as stock_number,
            v.transmission,
            case 
            when (select count(id) from vehicle_images vi where vi.vehicle_id = v.id) = 0 then ''
            else
           	(select GROUP_CONCAT(CONCAT('https://www.wheelsanddeals.ca/storage/inventory/', v.id, '/',vimages.image) SEPARATOR ',') from vehicle_images vimages where vimages.vehicle_id = v.id order by sort_order) 
            end as image_urls,
            case 
            when (select count(id) from vehicle_images vi where vi.vehicle_id = v.id) = 0 then 'https://www.wheelsanddeals.ca/vendor/wheelspackage/hug-in-progress.jpg'
            else
           	(select concat('https://www.wheelsanddeals.ca/storage/inventory/',v.id,'/',vimages.image) as main_image from vehicle_images vimages where vimages.vehicle_id = v.id and sort_order = 1 limit 1) 
            end as main_image,
            v.exterior_color,
            'https://www.wheelsanddeals.ca' as dealer_website_url
        
        from vehicles v
        left join vehicle_makes vm on (v.vehicle_makes_id = vm.id)
        left join vehicle_models vmod on (v.vehicle_models_id = vmod.id)
        
        where v.status = 1 and v.vehicle_type < 7;
        ";
        $r = DB::select($query);
        $results = json_decode(json_encode($r), true);
        $formatter = \SoapBox\Formatter\Formatter::make($results, \SoapBox\Formatter\Formatter::ARR);
        $csv = $formatter->toCsv();

        \File::put(storage_path("app/" . date("Y-m-d")."_car_gurus_wheelsanddeals.csv"), $csv);

        $localFile = \File::get(storage_path("app/" . date("Y-m-d")."_car_gurus_wheelsanddeals.csv"));
        \Storage::disk('ftp_gurus')->put('feed.csv', $localFile);
        print "\nPut file via ftp\n";
        print "\nDone\n";
    }
}
