<?php

namespace Tsawler\WheelsPackage;

use GuzzleHttp\Client;
use Illuminate\Console\Command;


class CleanVideos extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean-videos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove videos for sold vehicles';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(7200);
        $count = 0;
        print "Starting video clean...\n";
        $results = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
            select id from vehicles where status = 0 and vehicle_type < 7 and id in (select vehicle_id from vehicle_videos);
        "));
        foreach($results as $vehicle) {
            $count = $count + 1;
            // delete video files
            $videos = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
               select vv.id, v.id as video_id, v.file_name, v.thumb
               from vehicle_videos vv 
               left join videos v on (vv.video_id = v.id)
               where vv.vehicle_id = $vehicle->id
            "));

            foreach($videos as $video) {
                $filename = base_path() . "/storage/app/public/videos/" . $video->id . "-" .$video->file_name;
                $thumb = base_path() . "/storage/app/public/videos/" . $video->id . "-" .$video->thumb;
                // delete the video/thumb
                \File::delete($filename);
                \File::delete($thumb);
                //delete the record
                \Illuminate\Support\Facades\DB::table('vehicle_videos')->where('vehicle_id','=', $vehicle->id)->delete();
                \Illuminate\Support\Facades\DB::table('videos')->where('id','=', $video->video_id)->delete();
            }
        }

        print "...done. $count videos deleted.\n";
    }
}
