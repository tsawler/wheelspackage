<?php

namespace Tsawler\WheelsPackage;

use GuzzleHttp\Client;
use Illuminate\Console\Command;


class CleanPanoramas extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean-panoramas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove panoramic videos for sold vehicles';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(7200);
        $count = 0;
        print "Starting panorama clean...\n";
        $results = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
            select id from vehicles where status = 0 and vehicle_type < 7 and id in (select vehicle_id from vehicle_panoramas);
        "));
        foreach ($results as $vehicle) {
            $count = $count + 1;
            // delete video files
            $videos = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
               select id, panorama
               from vehicle_panoramas 
               where vehicle_id = $vehicle->id
            "));

            foreach ($videos as $video) {
                if (strpos($video->panorama, '.JPG') !== false) {
                    $filename = base_path() . "/storage/app/public/panoramas/" . $vehicle->id . "-" . str_replace(".JPG", ".mp4", $video->panorama);
                } else {
                    $filename = base_path() . "/storage/app/public/panoramas/" . $vehicle->id . "-" . str_replace(".jpeg", ".mp4", $video->panorama);
                }
                $thumb = base_path() . "/storage/app/public/panoramas/" . $vehicle->id . "-" . $video->panorama;
                // delete the video/thumb
                \File::delete($filename);
                \File::delete($thumb);
                print "Deleting movie " . $filename . "\n";
                print "Deleting image " . $thumb . "\n";
                //delete the record
                \Illuminate\Support\Facades\DB::table('vehicle_panoramas')->where('vehicle_id', '=', $vehicle->id)->delete();
            }
        }

        print "...done. $count videos deleted.\n";
    }
}
