<?php

Route::group(['middleware' => ['web']], function () {

    // test for deleting images
//    Route::get('/wheels/cleanup-images', function(){
//        set_time_limit(7200);
//        $results = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
//            select id from vehicles where status = 0;
//        "));
//       Log::info("starting");
//        foreach($results as $vehicle) {
//            Log::info("Doing: " . $vehicle->id);
//            $images = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
//               select id, image from vehicle_images where vehicle_id = " . $vehicle->id));
//            // delete it's images
//            foreach($images as $image) {
//                // delete the record
//                \Illuminate\Support\Facades\DB::table('vehicle_images')->where('id','=', $image->id)->delete();
//            }
//        }
//
//        // now delete directories/files
//        $r = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
//            select id from vehicles where status = 0 limit 10;
//        "));
//
//        foreach($r as $v) {
//            Log::info("Deleting directory " . base_path() . "/storage/app/public/inventory/" . $v->id);
//            \Storage::deleteDirectory(base_path() . "/storage/app/public/inventory/" . $v->id);
//        }
//        Log::info('Done');
//    });

//    Route::get('/wheels/cleanup-video', function(){
//        set_time_limit(7200);
//        Log::info('Starting videos');
//        $results = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
//            select id from vehicles where status = 0 limit 10;
//        "));
//        foreach($results as $vehicle) {
//            // delete video files
//            $videos = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("
//               select vv.id, v.file_name, v.thumb
//               from vehicle_videos vv
//               left join videos v on (vv.video_id = v.id)
//               where vv.vehicle_id = $vehicle->id
//            "));
//
//            foreach($videos as $video) {
//                Log::info('Doing video ' . $video->id);
//                $filename = base_path() . "/storage/app/public/videos/" . $video->id . "-" .$video->file_name;
//                $thumb = base_path() . "/storage/app/public/videos/" . $video->id . "-" .$video->thumb;
//                // delete the video/thumb
//                Log::info('deleting files');
//                \File::delete($filename);
//                \File::delete($thumb);
//                //delete the record
//                Log::info('Deleting records');
//                \Illuminate\Support\Facades\DB::table('vehicle_videos')->where('vehicle_id','=', $vehicle->id)->delete();
//                \Illuminate\Support\Facades\DB::table('videos')->where('id','=', $video->id)->delete();
//            }
//        }
//
//        echo "Done";
//    });

    Route::get('/feeds/insight', '\Tsawler\WheelsPackage\InsightFeed@getFeed');
    Route::get('/sales/{slug}', '\Tsawler\WheelsPackage\SalesController@getSalesPerson');

//    Route::get('migrate-gallery', function(){
//        $query = "select * from wheelsanddeals.gallery_items order by id";
//        $results = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw($query));
//        foreach($results as $result) {
//            echo $result->id . "<br>";
//        }
//    });

    Route::get('/vehicles/hand-picked-local-trade-ins', function(){
        return Redirect::to('/hand-picked-local-trade-ins', 301);
    });

    Route::get('/vehicles/suv-inventory', function(){
        return Redirect::to('/used-suvs-fredericton', 301);
    });

    Route::get('/vehicles/truck-inventory', function(){
        return Redirect::to('/used-trucks-fredericton', 301);
    });

    Route::get('/vehicles/vehicle-inventory', function(){
        return Redirect::to('/used-vehicle-inventory', 301);
    });

    Route::get('/vehicles/used-car-inventory', function(){
        return Redirect::to('/used-cars-fredericton', 301);
    });

    Route::get('/vehicles/used-van-inventory', function(){
        return Redirect::to('/used-minivans-fredericton', 301);
    });

    // kawasaki
    Route::get('/wheels/motorcycles', '\Tsawler\WheelsPackage\MotorcycleController@getCatalog');
    Route::get('/wheels/motorcycle/{id}', '\Tsawler\WheelsPackage\MotorcycleController@getItem');

    Route::get('/wheels/atv', '\Tsawler\WheelsPackage\ATVController@getCatalog');
    Route::get('/wheels/atv/{id}', '\Tsawler\WheelsPackage\MotorcycleController@getItem');

    Route::get('/wheels/glastron', '\Tsawler\WheelsPackage\GlastronController@getCatalog');
    Route::get('/wheels/glastron/{id}', '\Tsawler\WheelsPackage\MotorcycleController@getItem');



    // create account (standard)
    Route::get('/wheels/user/create', '\Tsawler\WheelsPackage\WheelsLoginController@getCreateAccount');
    Route::post('/wheels/user/create', '\Tsawler\WheelsPackage\WheelsLoginController@postCreateAccount');

    // social login
    Route::get('/wheels/{api}/api-login', '\Tsawler\WheelsPackage\WheelsLoginController@getSocialLogin');

    // inventory - everything
    Route::get('/inventory/modelsJson', '\Tsawler\WheelsPackage\VehicleController@getModelsPublicJson');

    // trucks
    Route::get('/inventory/modelsJsonTrucks', '\Tsawler\WheelsPackage\TruckController@getModelsPublicJson');

    //cars
    Route::get('/inventory/modelsJsonCars', '\Tsawler\WheelsPackage\CarController@getModelsPublicJson');

    // suvs
    Route::get('/inventory/modelsJsonSUVS', '\Tsawler\WheelsPackage\SUVController@getModelsPublicJson');

    //vans
    Route::get('/inventory/modelsJsonVans', '\Tsawler\WheelsPackage\VanController@getModelsPublicJson');

    // ATVs
    Route::get('/inventory/modelsJsonATVs', '\Tsawler\WheelsPackage\ATVController@getModelsPublicJson');

    // Glastron
    Route::get('/inventory/modelsJsonBoats', '\Tsawler\WheelsPackage\GlastronController@getModelsPublicJson');

    // Motorcycles
    Route::get('/inventory/modelsJsonMotorcycles', '\Tsawler\WheelsPackage\MotorcycleController@getModelsPublicJson');

    // Electric bikes
    Route::get('/inventory/modelsJsonElectricBikes', '\Tsawler\WheelsPackage\ElectricBikeController@getModelsPublicJson');

    // local trades
    Route::get('/inventory/modelsJsonHandPicked', '\Tsawler\WheelsPackage\VehicleController@getModelsHandPickedPublicJson');

    // motorcycles
    Route::get('/vehicles/motorcycle/{id}/{vehicle?}', '\Tsawler\WheelsPackage\MotorcycleController@getVehicleForPublic');
    Route::get('/vehicles/boats/{id}/{vehicle?}', '\Tsawler\WheelsPackage\MotorcycleController@getVehicleForPublic');

    // ebikes
    Route::get('/vehicles/ebike/{id}', '\Tsawler\WheelsPackage\ElectricBikeController@getVehicleForPublic');

    // common among vehicles
    Route::get('/vehicles/vehicle/{id}/{vehicle?}', '\Tsawler\WheelsPackage\VehicleController@getVehicleForPublic');
    Route::post('/ajax/send-to-a-friend', '\Tsawler\WheelsPackage\SendToAFriendController@sendMessage');
    Route::post('/ajax/request-test-drive', '\Tsawler\WheelsPackage\TestDriveController@sendMessage');
    Route::get('/vehicles/compare-vehicles', '\Tsawler\WheelsPackage\VehicleController@getCompareVehicles');
    Route::get('/vehicles/quick-quote', '\Tsawler\WheelsPackage\QuickQuoteController@quickQuote');

    // vehicle finder
    Route::post('/wheels/vehicle-finder', '\Tsawler\WheelsPackage\VehicleFinderController@postVehicleFinder');

    // credit applications
    Route::get('/credit/apply', '\Tsawler\WheelsPackage\CreditController@getCreditApp');
    Route::post('/credit/apply', '\Tsawler\WheelsPackage\CreditController@postCreditApp');
    Route::get('/credit/thanks', '\Tsawler\WheelsPackage\CreditController@getThanks');

    // window sticker
    Route::get('/wheels/vehicle-window-sticker', '\Tsawler\WheelsPackage\WindowStickerController@getSticker');


    // Admin routes
    Route::group(['middleware' => 'auth'], function () {

        // "huggable" word of mouth
        Route::group(['middleware' => 'auth.word'], function () {
            Route::get('/admin/word/all', '\Tsawler\WheelsPackage\WordController@getAllAdmin');
            Route::get('/admin/word/edit', '\Tsawler\WheelsPackage\WordController@getEdit');
            Route::post('/admin/word/edit', '\Tsawler\WheelsPackage\WordController@postEdit');
            Route::post('/admin/word/delete', '\Tsawler\WheelsPackage\WordController@delete');
            Route::any('/admin/all-word-json-admin', [
                'as'   => 'datatables-admin-word.allWordJson',
                'uses' => '\Tsawler\WheelsPackage\WordController@allWordJson',
            ]);
        });

        Route::group(['middleware' => 'auth.pages'], function () {

            Route::get('/admin/sales/salesperson', '\Tsawler\WheelsPackage\SalesController@getSalesAdmin');
            Route::post('/admin/sales/salesperson', '\Tsawler\WheelsPackage\SalesController@postSalesAdmin');
            Route::get('/admin/sales/all-sales-people', '\Tsawler\WheelsPackage\SalesController@getAllSalesStaff');
            Route::get('/admin/sales/delete', '\Tsawler\WheelsPackage\SalesController@deleteSalesStaff');

            Route::get('/admin/testimonials/all-testimonials', '\Tsawler\WheelsPackage\TestimonialController@getTestimonialsForAdmin');
            Route::get('/admin/testimonials/testimonial', '\Tsawler\WheelsPackage\TestimonialController@getTestimonial');
            Route::post('/admin/testimonials/testimonial', '\Tsawler\WheelsPackage\TestimonialController@postTestimonial');
            Route::get('/admin/testimonials/delete', '\Tsawler\WheelsPackage\TestimonialController@deleteTestimonial');
            Route::any('/admin/all-testimonials-apps-json-admin', [
                'as'   => 'datatables-admin-testimonials.allTestimonials',
                'uses' => '\Tsawler\WheelsPackage\TestimonialController@allTestimonialsJson',
            ]);
        });

        Route::group(['middleware' => 'auth.staff'], function () {
            // staff
            Route::get('/admin/staff/all', '\Tsawler\WheelsPackage\StaffController@getAllStaff');
            Route::get('/admin/staff/staff', '\Tsawler\WheelsPackage\StaffController@getStaffMember');
            Route::post('/admin/staff/staff', '\Tsawler\WheelsPackage\StaffController@postStaffMember');
            Route::get('/admin/staff/delete', '\Tsawler\WheelsPackage\StaffController@deleteStaffMember');
            Route::get('/admin/staff/sort', '\Tsawler\WheelsPackage\StaffController@getSetSortOrder');
            Route::post('/admin/staff/sort', '\Tsawler\WheelsPackage\StaffController@postSortOrder');
            Route::any('/admin/all-staff-json-admin', [
                'as'   => 'datatables-admin-staff.allStaffJson',
                'uses' => '\Tsawler\WheelsPackage\StaffController@allStaffJson',
            ]);
        });

        Route::group(['middleware' => 'auth.email'], function () {
            // notifications
            Route::get('/admin/email/welcome-email', '\Tsawler\WheelsPackage\EmailController@getWelcomeEmail');
            Route::post('/admin/email/welcome-email', '\Tsawler\WheelsPackage\EmailController@postWelcomeEmail');

        });

        Route::group(['middleware' => 'auth.credit'], function () {
            // credit apps
            Route::get('/admin/credit/all', '\Tsawler\WheelsPackage\CreditController@getAllCreditApps');
            Route::any('/admin/all-credit-apps-json-admin', [
                'as'   => 'datatables-admin-credit.allCreditAppsJson',
                'uses' => '\Tsawler\WheelsPackage\CreditController@allCreditAppsJson',
            ]);
            Route::get('/admin/credit/app', '\Tsawler\WheelsPackage\CreditController@getApp');
            Route::get('/admin/credit/process', '\Tsawler\WheelsPackage\CreditController@processApp');
            Route::get('/admin/credit/delete', '\Tsawler\WheelsPackage\CreditController@deleteApp');

            Route::any('/admin/all-quick-quotes-admin', [
                'as'   => 'datatables-admin-quickquotes.allQuickQuotesJson',
                'uses' => '\Tsawler\WheelsPackage\QuickQuoteController@allQuickQuotesJson',
            ]);
            Route::get('/admin/quick-quotes/quote', '\Tsawler\WheelsPackage\QuickQuoteController@getQuote');
            Route::get('/admin/quick-quotes/process', '\Tsawler\WheelsPackage\QuickQuoteController@processQuote');
            Route::get('/admin/quick-quotes/delete', '\Tsawler\WheelsPackage\QuickQuoteController@deleteQuote');
            Route::get('/admin/quick-quotes/all', '\Tsawler\WheelsPackage\QuickQuoteController@getAllQuickQuotes');
        });

        Route::group(['middleware' => 'auth.finder'], function () {
            // vehicle finder
            Route::get('/admin/finder/all', '\Tsawler\WheelsPackage\VehicleFinderController@getAllVehicleFinders');
            Route::any('/admin/all-vehicle-finders-json-admin', [
                'as'   => 'datatables-admin-credit.allVehicleFindersJson',
                'uses' => '\Tsawler\WheelsPackage\VehicleFinderController@allVehicleFindersJson',
            ]);
            Route::get('/admin/finder/finder', '\Tsawler\WheelsPackage\VehicleFinderController@getFinder');
            Route::get('/admin/finder/process', '\Tsawler\WheelsPackage\VehicleFinderController@processFinder');
            Route::get('/admin/finder/delete', '\Tsawler\WheelsPackage\VehicleFinderController@deleteFinder');
        });

        Route::group(['middleware' => 'auth.members'], function () {
            // notifications
            Route::get('/admin/members/all-members', '\Tsawler\WheelsPackage\MemberController@getAllMembers');
            Route::get('/admin/members/member', '\Tsawler\WheelsPackage\MemberController@getMember');
            Route::post('/admin/members/member', '\Tsawler\WheelsPackage\MemberController@postMember');
            Route::get('/admin/members/delete-member', '\Tsawler\WheelsPackage\MemberController@deleteMember');
            Route::any('/admin/json/all-members-json', [
                'as'   => 'datatables-admin-all-members.allMembersJson',
                'uses' => '\Tsawler\WheelsPackage\MemberController@allMembersJson',
            ]);
        });

        Route::group(['middleware' => 'auth.inventory'], function () {
            // notifications
            Route::get('/admin/inventory/all-vehicles', '\Tsawler\WheelsPackage\VehicleController@getInventoryAdmin');
            Route::get('/admin/inventory/all-vehicles-for-sale', '\Tsawler\WheelsPackage\VehicleController@getForSaleAdmin');
            Route::get('/admin/inventory/all-powersports-for-sale', '\Tsawler\WheelsPackage\VehicleController@getForSalePowerSportsAdmin');
            Route::get('/admin/inventory/all-vehicles-sold', '\Tsawler\WheelsPackage\VehicleController@getSoldAdmin');
            Route::get('/admin/inventory/all-vehicles-pending', '\Tsawler\WheelsPackage\VehicleController@getPendingAdmin');
            Route::get('/admin/inventory/all-vehicles-trade-in', '\Tsawler\WheelsPackage\VehicleController@getTradeInsAdmin');
            Route::get('/admin/inventory/vehicle', '\Tsawler\WheelsPackage\VehicleController@getVehicleAdmin');
            Route::post('/admin/inventory/vehicle', '\Tsawler\WheelsPackage\VehicleController@postVehicleAdmin');
            Route::get('/admin/inventory/modelsJson', '\Tsawler\WheelsPackage\VehicleController@getModelsJson');
            Route::get('/admin/inventory/delete-image', '\Tsawler\WheelsPackage\VehicleController@getDeleteImage');
            Route::get('/admin/inventory/refresh-from-pbs', '\Tsawler\WheelsPackage\VehicleController@getRefreshFromPBS');
            Route::any('/admin/json/all-inventory-json', [
                'as'   => 'datatables-admin-all-inventory.allInventoryJson',
                'uses' => '\Tsawler\WheelsPackage\VehicleController@allInventoryJson',
            ]);
            Route::any('/admin/json/all-inventory-json-powersports', [
                'as'   => 'datatables-admin-all-inventory.allInventoryPowerSportsJson',
                'uses' => '\Tsawler\WheelsPackage\VehicleController@allInventoryPowerSportsJson',
            ]);

            Route::any('/all-options-json', [
                'as'   => 'datatables-admin-options.allOptionsJson',
                'uses' => '\Tsawler\WheelsPackage\OptionsController@allOptionsJson',
            ]);
            Route::get('/admin/inventory/options-all', '\Tsawler\WheelsPackage\OptionsController@getAllOptions');
            Route::get('/admin/inventory/option', '\Tsawler\WheelsPackage\OptionsController@getOption');
            Route::post('/admin/inventory/option', '\Tsawler\WheelsPackage\OptionsController@postOption');
            Route::get('/admin/inventory/delete-option', '\Tsawler\WheelsPackage\OptionsController@deleteOption');
            Route::get('/admin/vehicles/video-list', '\Tsawler\WheelsPackage\VehicleController@getVideosJson');

            // power sports
            Route::get('/admin/powersports/item', '\Tsawler\WheelsPackage\PowerSportsController@getPowerSportAdmin');
            Route::post('/admin/powersports/item', '\Tsawler\WheelsPackage\PowerSportsController@postPowerSportAdmin');
            Route::get('/admin/powersports/all', '\Tsawler\WheelsPackage\PowerSportsController@getItems');
            Route::any('/admin/json/all-powersports-json', [
                'as'   => 'datatables-admin-all-powersports.allitemsJson',
                'uses' => '\Tsawler\WheelsPackage\PowerSportsController@allitemsJson',
            ]);

        });

        Route::group(['middleware' => 'auth.test_drive'], function () {
            Route::any('/wheels/all-test-drives-admin', [
                'as'   => 'datatables-admin-testdrives.allTestDrivesJson',
                'uses' => '\Tsawler\WheelsPackage\TestDriveController@allTestDrivesJson',
            ]);

            Route::get('/admin/test-drives/all', '\Tsawler\WheelsPackage\TestDriveController@getAllTestDrives');
            Route::get('/admin/test-drives/test-drive', '\Tsawler\WheelsPackage\TestDriveController@getTestDrive');
            Route::get('/admin/test-drives/process', '\Tsawler\WheelsPackage\TestDriveController@markAsProcessed');
            Route::get('/admin/test-drives/delete', '\Tsawler\WheelsPackage\TestDriveController@deleteTestDrive');
        });

    });

});
