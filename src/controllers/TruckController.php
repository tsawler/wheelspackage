<?php namespace Tsawler\WheelsPackage;

use App\Events\RecordPageViewEvent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;


/**
 * Class TruckController
 * @package Tsawler\WheelsPackage
 */
class TruckController extends BaseController
{

    /**
     * @return mixed
     */
    public static function getEmbeddedInventory()
    {
        $make = 0;
        $year = 0;
        $model = 0;
        $price = 0;

        $filter = [];

        $makes = [
            0 => 'All makes',
        ];

        $models = [
            0 => 'All models',
        ];

        $years = [
            0 => 'All Years',
        ];

        $results = Vehicle::select('year')
            ->distinct()
            ->where('vehicle_type', '=', '2')
            ->where('status', '=', '1')
            ->orderBy('year', 'desc')->get();

        foreach ($results as $result) {
            $years[$result->year] = $result->year;
        }

        if (Input::has('price')) {
            // we are filtering input
            $year = Input::get('year');
            $make = Input::get('make');
            $model = Input::get('model');
            $price = Input::get('price');
            $filter = [
                'year'  => $year,
                'make'  => $make,
                'model' => $model,
                'price' => $price,
            ];
            Session::put('filter_vehicles', $filter);
        }

        if (Session::has('filter_vehicles')) {
            $filter = Session::get('filter_vehicles');
            $year = $filter['year'];
            $make = $filter['make'];
            $model = $filter['model'];
            $price = $filter['price'];
        }

        $active_makes = [];
        $results = DB::table('vehicles')
            ->select('vehicle_makes_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('vehicle_type', '=', '2')
            ->get();

        foreach ($results as $result) {
            $active_makes[] = $result->vehicle_makes_id;
        }

        $results = VehicleMake::whereIn('id', $active_makes)->orderBy('make')->get();

        foreach ($results as $result) {
            $makes[$result->id] = $result->make;
        }

        if ($make > 0) {
            $active_models = [];

            $results = DB::table('vehicles')
                ->select('vehicle_models_id')
                ->distinct()
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '2')
                ->where('vehicle_makes_id', '=', $make)->get();

            foreach ($results as $result) {
                $active_models[] = $result->vehicle_models_id;
            }

            $results = VehicleModel::whereIn('id', $active_models)->orderBy('model')->get();

            foreach ($results as $result) {
                $models[$result->id] = $result->model;
            }
        }


        if (sizeof($filter) > 0) {

            $t = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '2');
            if ($make > 0) {
                $t = $t->where('vehicle_makes_id', '=', $make);
            }

            if ($model > 0) {
                $t = $t->where('vehicle_models_id', '=', $model);
            }

            if ($year > 0) {
                $t = $t->where('year', '=', $year);
            }

            if ($price == 0) {
            } else if ($price == 1) {
                $t = $t->orderBy('cost', 'asc');
            } else if ($price == 2) {
                $t = $t->orderBy('cost', 'desc');
            }

            $inventory = $t->orderBy('year', 'desc')->paginate(20);

        } else {
            $inventory = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '2')
                ->orderBy('year', 'desc')->paginate(20);
        }

        return View::make('wheelspackage::public.inventory-embedded')
            ->with('inventory', $inventory)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('years', $years)
            ->with('year', $year)
            ->with('make', $make)
            ->with('model', $model)
            ->with('price', $price);
    }


    /**
     * @return mixed
     */
    public function getVehicleForPublic()
    {
        $id = Request::segment(3);
        $item = Vehicle::with('make', 'vehicleModel', 'options', 'images')->find($id);

        return View::make('public.vehicle')
            ->with('item', $item)
            ->with('vehicle_id', $id);
    }


    /**
     * @return mixed
     */
    public function getModelsPublicJson()
    {
        $id = Input::get('id');
        $active_models = [];

        $results = DB::table('vehicles')
            ->select('vehicle_models_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('vehicle_type', '=', '2')
            ->where('vehicle_makes_id', '=', $id)->get();

        foreach ($results as $result) {
            $active_models[] = $result->vehicle_models_id;
        }

        $results = VehicleModel::whereIn('id', $active_models)->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();

        return $results->toJson();
    }


    /**
     * @return mixed
     */
    public function getCompareVehicles()
    {
        $idString = Input::get('ids');
        $ids = explode(",", $idString);
        $vehicle_count = sizeof($ids);

        $vehicle_1 = Vehicle::with('make', 'vehicleModel', 'options')->find($ids[0]);
        $vehicle_2 = Vehicle::with('make', 'vehicleModel', 'options')->find($ids[1]);

        if (sizeof($ids) == 3) {
            $vehicle_3 = Vehicle::with('make', 'vehicleModel', 'options')->find($ids[2]);
            $width = 33;
        } else {
            $vehicle_3 = null;
            $width = 50;
        }

        return View::make('public.compare')
            ->with('vehicle_count', $vehicle_count)
            ->with('vehicle_1', $vehicle_1)
            ->with('vehicle_2', $vehicle_2)
            ->with('vehicle_3', $vehicle_3)
            ->with('width', $width);
    }

}
