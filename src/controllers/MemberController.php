<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class MemberController
 * @package Tsawler\WheelsPackage
 */
class MemberController extends Controller
{

    /**
     * @return mixed
     */
    public function getAllMembers()
    {

        return View::make('wheelspackage::admin.members-all-members');
    }


    /**
     * @return mixed
     */
    public function allMembersJson()
    {
        $members = User::select(['id', 'first_name', 'last_name', 'created_at', 'updated_at', 'login_types_id'])
            ->where('access_level', '=', '1');

        return DataTables::of($members)
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function getMember()
    {
        $id = Input::get('id');
        $member_id = 0;

        if ($id > 0) {
            $member = User::find($id);
            $member_id = $member->id;
        } else {
            $member = new User();
        }

        return View::make('wheelspackage::admin.members-edit-member')
            ->with('member', $member)
            ->with('member_id', $member_id);
    }


    /**
     * @return mixed
     */
    public function postMember()
    {
        $id = Input::get('id');

        $member = User::find($id);

        $member->first_name = Input::get('first_name');
        $member->last_name = Input::get('last_name');
        $member->email = Input::get('email');
        $member->user_active = Input::get('user_active');
        $member->access_level = Input::get('access_level');

        $member->save();

        return Redirect::to('/admin/members/all-members')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function deleteMember()
    {
        $id = Input::get('id');
        User::find($id)->delete();

        return Redirect::to('/admin/members/all-members')
            ->with('message', 'Changes saved');

    }

}