<?php namespace Tsawler\WheelsPackage;

use App\DbView;
use App\Http\Controllers\Controller;
use App\Localize;
use App\Page;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;


/**
 * Class BaseController
 * @package Tsawler\WheelsPackage
 */
class BaseController extends Controller
{

    /**
     * @var Page
     */
    protected $page;


    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $page = new Page;
        $page->active = 1;
        $this->page = $page;
    }


    /**
     * @param $slug
     * @return array
     * @throws \Exception
     */
    protected function generatePage($slug): array
    {
        if (Cache::has('page_' . $slug . '_' . App::getLocale())) {
            $page = Cache::get('page_' . $slug . '_' . App::getLocale());
        } else {
            $page = Page::with('slider.slides')->where('slug', '=', $slug)->first();
            Cache::forever('page_' . $slug . '_' . App::getLocale(), $page);
        }

        if ($page) {
            $page_title_field = Localize::localize('page_title');
            $page_content_field = Localize::localize('page_content');
            $page_title = $page->$page_title_field;
            $page_content = $page->$page_content_field;
            $slider = $page->slider;
            $active = $page->active;
        }

        if ($slider != null) {
            $slider_id = $page->slider->id;
            $slider_sort = "{";
            foreach ($slider->slides as $item) {
                $slider_sort .= '"' . $item->id . '":' . $item->sort_order . ",";
            }
            $slider_sort = rtrim($slider_sort, ",");
            $slider_sort .= "}";
        } else {
            $slider_id = 0;
        }
        $generated_content = DbView::blade($page_content);

        return [$page, $page_title, $generated_content];
    }
}
