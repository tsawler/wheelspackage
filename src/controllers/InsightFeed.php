<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use App\User;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use SoapBox\Formatter\Formatter;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Spatie\ArrayToXml\ArrayToXml;

/**
 * Class GlastronController
 * @package Tsawler\WheelsPackage
 */
class InsightFeed extends Controller
{

    public function getFeed()
    {
        $query = "
        select
            0001 as dealer_id, 
            'Jim Gilbert\'s Wheels and Deals' as dealer_name,
            '402 St. Mary\'s Street, Fredericton NB' as address,
            '5064596832' as phone,
            'E3A 8H5' as postalcode,
            'salesmanager@wheelsanddeals.ca' as email,
            v.id, vin, stock_no as stockid, used as is_used, 1 as is_certified, year,
            vm.make, vmod.model, v.body, v.trim, v.transmission, v.odometer as kilometers,
            v.exterior_color, v.cost as price, '' as model_code,
            v.drive_train as drivetrain
        
        
        from vehicles v
        left join vehicle_makes vm on (v.vehicle_makes_id = vm.id)
        left join vehicle_models vmod on (v.vehicle_models_id = vmod.id)
        
        where v.status = 1 and v.vehicle_type < 7
        ";
        $r = DB::select($query);
        $results = json_decode(json_encode($r), true);

        $formatter = Formatter::make($results, Formatter::ARR);
        $csv = $formatter->toXml();
        return response($csv, 200, [
            'Content-Type' => 'application/xml'
        ]);
    }
}