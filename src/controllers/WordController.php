<?php namespace Tsawler\WheelsPackage;

use App\Events\RecordPageViewEvent;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class WordController
 * @package Tsawler\WheelsPackage
 */
class WordController extends Controller
{

    /**
     * @return mixed
     */
    public static function getAll()
    {

        $results = Word::where('active', '=', '1')->orderBy('id', 'desc')->paginate(10);

        return View::make('wheelspackage::public.word-of-mouth')
            ->with('results', $results);
    }


    /**
     * @return mixed
     */
    public function getEdit()
    {
        $id = Input::get('id');

        if ($id > 0) {
            $item = Word::find($id);
        } else {
            $item = new Word();
        }

        return View::make('wheelspackage::admin.word-edit')
            ->with('item', $item)
            ->with('id', $id);
    }


    /**
     * @return mixed
     */
    public function postEdit()
    {
        $id = Input::get('id');

        if ($id > 0) {
            $item = Word::find($id);
        } else {
            $item = new Word();
        }


        $item->title = Input::get('title');
        $item->content = Input::get('content');
        $item->active = Input::get('active');
        $item->save();

        return Redirect::to('/admin/word/all')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function delete()
    {
        Word::find(Input::get('id'))->delete();

        return Redirect::to('/admin/word/all-word')
            ->with('message', 'Item deleted');
    }


    /**
     * @return mixed
     */
    public function getAllAdmin()
    {
        return View::make('wheelspackage::admin.word-all');
    }

    /**
     * @return mixed
     */
    public function allWordJson()
    {
        $apps = Word::select(['id', 'title', 'created_at', 'updated_at', 'active']);

        return DataTables::of($apps)
            ->editColumn('updated_at', function ($data) {
                return $data->updated_at->diffForHumans();
            })
            ->make(true);
    }

}
