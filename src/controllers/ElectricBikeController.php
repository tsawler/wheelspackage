<?php namespace Tsawler\WheelsPackage;

use App\Events\RecordPageViewEvent;
use App\Http\Controllers\Controller;
use App\User;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

/**
 * Class ElectricBikeController
 * @package Tsawler\WheelsPackage
 */
class ElectricBikeController extends Controller
{



    public function getItem()
    {
        $id = Request::segment(3);

        $page = new Page();
        $page->active = 1;

        $item = Vehicle::find($id);

        $images = explode("|", $item->images);

        return View::make('wheelspackage::public.ebike')
            ->with('page', $page)
            ->with('item', $item)
            ->with('images', $images);
    }


    /**
     * @return mixed
     */
    public static function getEmbeddedInventory()
    {
        $make = 0;
        $year = 0;
        $model = 0;
        $price = 0;

        $filter = [];

        $makes = [
            0 => 'All makes',
        ];

        $models = [
            0 => 'All models',
        ];

        $years = [
            0 => 'All Years',
        ];

        $results = Vehicle::select('year')
            ->distinct()
            ->where('vehicle_type', '=', '16')
            ->where('status', '=', '1')
            ->orderBy('year', 'desc')->get();

        foreach ($results as $result) {
            $years[$result->year] = $result->year;
        }

        if (Input::has('price')) {
            // we are filtering input
            $year = Input::get('year');
            $make = Input::get('make');
            $model = Input::get('model');
            $price = Input::get('price');
            $filter = [
                'year'  => $year,
                'make'  => $make,
                'model' => $model,
                'price' => $price,
            ];
            Session::put('filter_vehicles', $filter);
        }

        if (Session::has('filter_vehicles')) {
            $filter = Session::get('filter_vehicles');
            $year = $filter['year'];
            $make = $filter['make'];
            $model = $filter['model'];
            $price = $filter['price'];
        }

        $active_makes = [];
        $results = DB::table('vehicles')
            ->select('vehicle_makes_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('vehicle_type', '=', '16')
            ->get();

        foreach ($results as $result) {
            $active_makes[] = $result->vehicle_makes_id;
        }

        $results = VehicleMake::whereIn('id', $active_makes)->orderBy('make')->get();

        foreach ($results as $result) {
            $makes[$result->id] = $result->make;
        }

        if ($make > 0) {
            $active_models = [];

            $results = DB::table('vehicles')
                ->select('vehicle_models_id')
                ->distinct()
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '16')
                ->where('vehicle_makes_id', '=', $make)->get();

            foreach ($results as $result) {
                $active_models[] = $result->vehicle_models_id;
            }

            $results = VehicleModel::whereIn('id', $active_models)->orderBy('model')->get();

            foreach ($results as $result) {
                $models[$result->id] = $result->model;
            }
        }


        if (sizeof($filter) > 0) {

            $t = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '16');
            if ($make > 0) {
                $t = $t->where('vehicle_makes_id', '=', $make);
            }

            if ($model > 0) {
                $t = $t->where('vehicle_models_id', '=', $model);
            }

            if ($year > 0) {
                $t = $t->where('year', '=', $year);
            }

            if ($price == 0) {
            } else if ($price == 1) {
                $t = $t->orderBy('cost', 'asc');
            } else if ($price == 2) {
                $t = $t->orderBy('cost', 'desc');
            }

            $inventory = $t->orderBy('year', 'desc')->paginate(20);

        } else {
            $inventory = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '16')
                ->orderBy('year', 'desc')->paginate(20);
        }

        return View::make('wheelspackage::public.inventory-embedded-ebike')
            ->with('inventory', $inventory)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('years', $years)
            ->with('year', $year)
            ->with('make', $make)
            ->with('model', $model)
            ->with('price', $price);
    }

    /**
     * @return mixed
     */
    public static function getEmbeddedVespaInventory()
    {
        $make = 0;
        $year = 0;
        $model = 0;
        $price = 0;

        $filter = [];

        $makes = [
            0 => 'All makes',
        ];

        $models = [
            0 => 'All models',
        ];

        $years = [
            0 => 'All Years',
        ];

        $results = Vehicle::select('year')
            ->distinct()
            ->where('vehicle_type', '=', '17')
            ->where('status', '=', '1')
            ->orderBy('year', 'desc')->get();

        foreach ($results as $result) {
            $years[$result->year] = $result->year;
        }

        if (Input::has('price')) {
            // we are filtering input
            $year = Input::get('year');
            $make = Input::get('make');
            $model = Input::get('model');
            $price = Input::get('price');
            $filter = [
                'year'  => $year,
                'make'  => $make,
                'model' => $model,
                'price' => $price,
            ];
            Session::put('filter_vehicles', $filter);
        }

        if (Session::has('filter_vehicles')) {
            $filter = Session::get('filter_vehicles');
            $year = $filter['year'];
            $make = $filter['make'];
            $model = $filter['model'];
            $price = $filter['price'];
        }

        $active_makes = [];
        $results = DB::table('vehicles')
            ->select('vehicle_makes_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('vehicle_type', '=', '17')
            ->get();

        foreach ($results as $result) {
            $active_makes[] = $result->vehicle_makes_id;
        }

        $results = VehicleMake::whereIn('id', $active_makes)->orderBy('make')->get();

        foreach ($results as $result) {
            $makes[$result->id] = $result->make;
        }

        if ($make > 0) {
            $active_models = [];

            $results = DB::table('vehicles')
                ->select('vehicle_models_id')
                ->distinct()
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '17')
                ->where('vehicle_makes_id', '=', $make)->get();

            foreach ($results as $result) {
                $active_models[] = $result->vehicle_models_id;
            }

            $results = VehicleModel::whereIn('id', $active_models)->orderBy('model')->get();

            foreach ($results as $result) {
                $models[$result->id] = $result->model;
            }
        }


        if (sizeof($filter) > 0) {

            $t = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '17');
            if ($make > 0) {
                $t = $t->where('vehicle_makes_id', '=', $make);
            }

            if ($model > 0) {
                $t = $t->where('vehicle_models_id', '=', $model);
            }

            if ($year > 0) {
                $t = $t->where('year', '=', $year);
            }

            if ($price == 0) {
            } else if ($price == 1) {
                $t = $t->orderBy('cost', 'asc');
            } else if ($price == 2) {
                $t = $t->orderBy('cost', 'desc');
            }

            $inventory = $t->orderBy('year', 'desc')->paginate(20);

        } else {
            $inventory = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '17')
                ->orderBy('year', 'desc')->paginate(20);
        }

        return View::make('wheelspackage::public.inventory-embedded-ebike')
            ->with('inventory', $inventory)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('years', $years)
            ->with('year', $year)
            ->with('make', $make)
            ->with('model', $model)
            ->with('price', $price);
    }


    public function getModelsPublicJson()
    {
        $id = Input::get('id');
        $active_models = [];

        $results = DB::table('vehicles')
            ->select('vehicle_models_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('vehicle_type', '=', '16')
            ->where('vehicle_makes_id', '=', $id)->get();

        foreach ($results as $result) {
            $active_models[] = $result->vehicle_models_id;
        }

        $results = VehicleModel::whereIn('id', $active_models)->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();

        return $results->toJson();
    }


    protected function getAllSalesStaffForVehicles() {
        $sales = Sale::where('active','=','1')
            ->limit(6)
            ->orderBy(DB::raw('RAND()'))
            ->get();

        return $sales;
    }

    /**
     * @return mixed
     */
    public function getVehicleForPublic()
    {
        $page = new Page;
        $page->active = 1;
        $id = Request::segment(3);
        $item = Vehicle::with('make', 'vehicleModel', 'options', 'images', 'video')->find($id);

        if ($item->video !== null)
            $video_id = $item->video->video_id;
        else
            $video_id = 0;

        $sales = $this->getAllSalesStaffForVehicles();

        event(new RecordPageViewEvent(Request::ip(), Request::url(), $item->year . " " . $item->make->make . " " . $item->vehicleModel->model . ": " . $item->stock_no));

        return View::make('wheelspackage::public.ebike-inventory')
            ->with('item', $item)
            ->with('vehicle_id', $id)
            ->with('page', $page)
            ->with('page_name', '')
            ->with('video_id', $video_id)
            ->with('sales', $sales);
    }
}