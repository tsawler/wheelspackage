<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use App\User;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

/**
 * Class GlastronController
 * @package Tsawler\WheelsPackage
 */
class GlastronController extends Controller
{

    public function getCatalog()
    {
        $page = new Page();
        $page->active = 1;

        $bowriders = PowerSport::where('category','=','glastron-bowrider')->orderBy('id')->get();
        $skifish = PowerSport::where('category','=','glastron-skifish')->orderBy('id')->get();
        $deckboats = PowerSport::where('category','=','glastron-deckboat')->orderBy('id')->get();
        $cruisers = PowerSport::where('category','=','glastron-cruiser')->orderBy('id')->get();

        return View::make('wheelspackage::public.glastron')
            ->with('page', $page)
            ->with('bowriders', $bowriders)
            ->with('skifish', $skifish)
            ->with('deckboats', $deckboats)
            ->with('cruisers', $cruisers);
    }

    public static function getEmbeddedInventory()
    {
        $make = 0;
        $year = 0;
        $model = 0;
        $price = 0;

        $filter = [];

        $makes = [
            0 => 'All makes',
        ];

        $models = [
            0 => 'All models',
        ];

        $years = [
            0 => 'All Years',
        ];

        $results = Vehicle::select('year')
            ->distinct()
            ->where('vehicle_type', '=', '15')
            ->where('status', '=', '1')
            ->where('used','=','0')
            ->orderBy('year', 'desc')->get();

        foreach ($results as $result) {
            $years[$result->year] = $result->year;
        }

        if (Input::has('price')) {
            // we are filtering input
            $year = Input::get('year');
            $make = Input::get('make');
            $model = Input::get('model');
            $price = Input::get('price');
            $filter = [
                'year'  => $year,
                'make'  => $make,
                'model' => $model,
                'price' => $price,
            ];
            Session::put('filter_vehicles', $filter);
        }

        if (Session::has('filter_vehicles')) {
            $filter = Session::get('filter_vehicles');
            $year = $filter['year'];
            $make = $filter['make'];
            $model = $filter['model'];
            $price = $filter['price'];
        }

        $active_makes = [];
        $results = DB::table('vehicles')
            ->select('vehicle_makes_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('used','=','0')
            ->where('vehicle_type', '=', '15')
            ->get();

        foreach ($results as $result) {
            $active_makes[] = $result->vehicle_makes_id;
        }

        $results = VehicleMake::whereIn('id', $active_makes)->orderBy('make')->get();

        foreach ($results as $result) {
            $makes[$result->id] = $result->make;
        }

        if ($make > 0) {
            $active_models = [];

            $results = DB::table('vehicles')
                ->select('vehicle_models_id')
                ->distinct()
                ->where('status', '=', '1')
                ->where('used','=','0')
                ->where('vehicle_type', '=', '15')
                ->where('vehicle_makes_id', '=', $make)->get();

            foreach ($results as $result) {
                $active_models[] = $result->vehicle_models_id;
            }

            $results = VehicleModel::whereIn('id', $active_models)->orderBy('model')->get();

            foreach ($results as $result) {
                $models[$result->id] = $result->model;
            }
        }


        if (sizeof($filter) > 0) {

            $t = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('used','=','0')
                ->where('vehicle_type', '=', '15');
            if ($make > 0) {
                $t = $t->where('vehicle_makes_id', '=', $make);
            }

            if ($model > 0) {
                $t = $t->where('vehicle_models_id', '=', $model);
            }

            if ($year > 0) {
                $t = $t->where('year', '=', $year);
            }

            if ($price == 0) {
            } else if ($price == 1) {
                $t = $t->orderBy('cost', 'asc');
            } else if ($price == 2) {
                $t = $t->orderBy('cost', 'desc');
            }

            $inventory = $t->orderBy('year', 'desc')->paginate(20);

        } else {
            $inventory = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '15')
                ->where('used','=','0')
                ->orderBy('year', 'desc')->paginate(20);
        }

        return View::make('wheelspackage::public.inventory-embedded')
            ->with('inventory', $inventory)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('years', $years)
            ->with('year', $year)
            ->with('make', $make)
            ->with('model', $model)
            ->with('price', $price);
    }


    public static function getEmbeddedUsedPowerSportsInventory()
    {
        $make = 0;
        $year = 0;
        $model = 0;
        $price = 0;

        $filter = [];

        $makes = [
            0 => 'All makes',
        ];

        $models = [
            0 => 'All models',
        ];

        $years = [
            0 => 'All Years',
        ];

        $results = Vehicle::select('year')
            ->distinct()
            ->whereIn('vehicle_type', [8,11,12,15,9,13,10,7,14])
            ->where('status', '=', '1')
            ->where('used','=','1')
            ->orderBy('year', 'desc')->get();

        foreach ($results as $result) {
            $years[$result->year] = $result->year;
        }

        if (Input::has('price')) {
            // we are filtering input
            $year = Input::get('year');
            $make = Input::get('make');
            $model = Input::get('model');
            $price = Input::get('price');
            $filter = [
                'year'  => $year,
                'make'  => $make,
                'model' => $model,
                'price' => $price,
            ];
            Session::put('filter_vehicles', $filter);
        }

        if (Session::has('filter_vehicles')) {
            $filter = Session::get('filter_vehicles');
            $year = $filter['year'];
            $make = $filter['make'];
            $model = $filter['model'];
            $price = $filter['price'];
        }

        $active_makes = [];
        $results = DB::table('vehicles')
            ->select('vehicle_makes_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('used','=','1')
            ->whereIn('vehicle_type', [8,11,12,15,9,13,10,7,14])
            ->get();

        foreach ($results as $result) {
            $active_makes[] = $result->vehicle_makes_id;
        }

        $results = VehicleMake::whereIn('id', $active_makes)->orderBy('make')->get();

        foreach ($results as $result) {
            $makes[$result->id] = $result->make;
        }

        if ($make > 0) {
            $active_models = [];

            $results = DB::table('vehicles')
                ->select('vehicle_models_id')
                ->distinct()
                ->where('status', '=', '1')
                ->where('used','=','1')
                ->whereIn('vehicle_type', [8,11,12,15,9,13,10,7,14])
                ->where('vehicle_makes_id', '=', $make)->get();

            foreach ($results as $result) {
                $active_models[] = $result->vehicle_models_id;
            }

            $results = VehicleModel::whereIn('id', $active_models)->orderBy('model')->get();

            foreach ($results as $result) {
                $models[$result->id] = $result->model;
            }
        }


        if (sizeof($filter) > 0) {

            $t = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('used','=','1')
                ->whereIn('vehicle_type', [8,11,12,15,9,13,10,7,14]);
            if ($make > 0) {
                $t = $t->where('vehicle_makes_id', '=', $make);
            }

            if ($model > 0) {
                $t = $t->where('vehicle_models_id', '=', $model);
            }

            if ($year > 0) {
                $t = $t->where('year', '=', $year);
            }

            if ($price == 0) {
            } else if ($price == 1) {
                $t = $t->orderBy('cost', 'asc');
            } else if ($price == 2) {
                $t = $t->orderBy('cost', 'desc');
            }

            $inventory = $t->orderBy('year', 'desc')->paginate(20);

        } else {
            $inventory = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->whereIn('vehicle_type', [8,11,12,15,9,13,10,7,14])
                ->where('used','=','1')
                ->orderBy('year', 'desc')->paginate(20);
        }

        return View::make('wheelspackage::public.inventory-embedded')
            ->with('inventory', $inventory)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('years', $years)
            ->with('year', $year)
            ->with('make', $make)
            ->with('model', $model)
            ->with('price', $price);
    }


    public static function getEmbeddedUsedInventory()
    {
        $make = 0;
        $year = 0;
        $model = 0;
        $price = 0;

        $filter = [];

        $makes = [
            0 => 'All makes',
        ];

        $models = [
            0 => 'All models',
        ];

        $years = [
            0 => 'All Years',
        ];

        $results = Vehicle::select('year')
            ->distinct()
            ->whereRaw('vehicle_type = 9 or vehicle_type = 15')
            ->where('status', '=', '1')
            ->where('used','=','1')
            ->orderBy('year', 'desc')->get();

        foreach ($results as $result) {
            $years[$result->year] = $result->year;
        }

        if (Input::has('price')) {
            // we are filtering input
            $year = Input::get('year');
            $make = Input::get('make');
            $model = Input::get('model');
            $price = Input::get('price');
            $filter = [
                'year'  => $year,
                'make'  => $make,
                'model' => $model,
                'price' => $price,
            ];
            Session::put('filter_vehicles', $filter);
        }

        if (Session::has('filter_vehicles')) {
            $filter = Session::get('filter_vehicles');
            $year = $filter['year'];
            $make = $filter['make'];
            $model = $filter['model'];
            $price = $filter['price'];
        }

        $active_makes = [];
        $results = DB::table('vehicles')
            ->select('vehicle_makes_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('used','=','1')
            ->whereRaw('vehicle_type = 9 or vehicle_type = 15')
            ->get();

        foreach ($results as $result) {
            $active_makes[] = $result->vehicle_makes_id;
        }

        $results = VehicleMake::whereIn('id', $active_makes)->orderBy('make')->get();

        foreach ($results as $result) {
            $makes[$result->id] = $result->make;
        }

        if ($make > 0) {
            $active_models = [];

            $results = DB::table('vehicles')
                ->select('vehicle_models_id')
                ->distinct()
                ->where('status', '=', '1')
                ->where('used','=','1')
                ->whereRaw('vehicle_type = 9 or vehicle_type = 15')
                ->where('vehicle_makes_id', '=', $make)->get();

            foreach ($results as $result) {
                $active_models[] = $result->vehicle_models_id;
            }

            $results = VehicleModel::whereIn('id', $active_models)->orderBy('model')->get();

            foreach ($results as $result) {
                $models[$result->id] = $result->model;
            }
        }


        if (sizeof($filter) > 0) {

            $t = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('used','=','1')
                ->whereRaw('vehicle_type = 9 or vehicle_type = 15');
            if ($make > 0) {
                $t = $t->where('vehicle_makes_id', '=', $make);
            }

            if ($model > 0) {
                $t = $t->where('vehicle_models_id', '=', $model);
            }

            if ($year > 0) {
                $t = $t->where('year', '=', $year);
            }

            if ($price == 0) {
            } else if ($price == 1) {
                $t = $t->orderBy('cost', 'asc');
            } else if ($price == 2) {
                $t = $t->orderBy('cost', 'desc');
            }

            $inventory = $t->orderBy('year', 'desc')->paginate(20);

        } else {
            $inventory = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->whereRaw('(vehicle_type = 9 or vehicle_type = 15) and used = 1 and status = 1')
                ->orderBy('year', 'desc')->paginate(20);
        }

        return View::make('wheelspackage::public.inventory-embedded')
            ->with('inventory', $inventory)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('years', $years)
            ->with('year', $year)
            ->with('make', $make)
            ->with('model', $model)
            ->with('price', $price);
    }


    public function getModelsPublicJson()
    {
        $id = Input::get('id');
        $active_models = [];

        $results = DB::table('vehicles')
            ->select('vehicle_models_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('vehicle_type', '=', '15')
            ->where('vehicle_makes_id', '=', $id)->get();

        foreach ($results as $result) {
            $active_models[] = $result->vehicle_models_id;
        }

        $results = VehicleModel::whereIn('id', $active_models)->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();

        return $results->toJson();
    }
}