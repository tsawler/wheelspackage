<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use App\Page;

/**
 * Class SalesController
 * @package Tsawler\WheelsPackage
 */
class SalesController extends Controller
{

    /**
     * @return mixed
     */
    public function getSalesPerson($slug)
    {

        $staff = Sale::where('active', '=', '1')
            ->where('slug','=',$slug)
            ->first();

        if ($staff) {
            $page = new Page();
            return View::make('wheelspackage::public.sales-person')
                ->with('staff', $staff)
                ->with('page', $page);
        } else {
            abort(404);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function getSalesAdmin()
    {
        $id = Input::get('id');
        if ($id > 0) {
            $item = Sale::find($id);
        } else {
            $item = new Sale;
        }

        return View::make('wheelspackage::admin.sales-person-admin')
            ->with("item", $item)
            ->with("id", $id);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSalesAdmin()
    {
        $path = public_path('salesstaff');

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        $id = Input::get('id');
        if ($id > 0) {
            $item = Sale::find($id);
        } else {
            $item = new Sale;
        }

        $item->salesperson_name = Input::get('salesperson_name');
        $item->phone = Input::get('phone');
        $item->active = Input::get('active');
        $item->save();
        $id = $item->id;

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $destinationPath = public_path() . "/salesstaff/";
            if (!\File::exists($destinationPath)) {
                \File::makeDirectory($destinationPath, 0755, true);
            }

            $filename = $id . "-" . str_random(10) . "-" . $file->getClientOriginalName();
            $upload_success = Input::file('image')->move($destinationPath, $filename);

            if ($upload_success) {
                $item->image = $filename;
            }
        }

        $item->save();
        return Redirect::to('/admin/sales/all-sales-people')
            ->with('message', 'Changes saved');
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function getAllSalesStaff() {
        $staff = Sale::orderBy("salesperson_name")->get();

        return View::make('wheelspackage::admin.sales-staff-all')
            ->with('staff', $staff);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteSalesStaff(){
        $id = Input::get('id');
        $result = Sale::find($id)->delete();
        return Redirect::to('/admin/sales/all-sales-people')
            ->with('message', 'Deleted');
    }
}