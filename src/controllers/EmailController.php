<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

/**
 * Class EmailController
 * @package Tsawler\WheelsPackage
 */
class EmailController extends Controller
{

    /**
     * @return mixed
     */
    public function getWelcomeEmail()
    {
        $email = Email::find(1);

        return View::make('wheelspackage::admin.email-welcome-email')
            ->with('email', $email);
    }


    /**
     * @return mixed
     */
    public function postWelcomeEmail()
    {
        $email = Email::find(1);

        $email->subject = Input::get('subject');
        $email->message = Input::get('message');
        $email->save();

        return redirect()->back()->with('message', 'Changes saved');
    }


}