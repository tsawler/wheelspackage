<?php namespace Tsawler\WheelsPackage;

use App\Events\RecordPageViewEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class TestDriveController
 * @package Tsawler\WheelsPackage
 */
class TestDriveController extends Controller
{

    /**
     * @return string
     */
    public function sendMessage()
    {
        $email = Input::get('email');
        $phone = Input::get('phone');
        $preferred_date = Input::get('preferred_date');
        $preferred_time = Input::get('preferred_time');
        $name = Input::get('users_name');
        $id = Input::get('id');

        $test_drive = new TestDrive();
        $test_drive->users_name = $name;
        $test_drive->email = $email;
        $test_drive->phone = $phone;
        $test_drive->preferred_date = $preferred_date;
        $test_drive->preferred_time = $preferred_time;
        $test_drive->vehicle_id = $id;
        $test_drive->save();

        $data = [
            'users_name'     => $name,
            'users_phone'    => $phone,
            'users_email'    => $email,
            'vehicle_id'     => $id,
            'preferred_date' => $preferred_date,
            'preferred_time' => $preferred_time,
        ];

        //$to_addresses = ['john.eliakis@wheelsanddeals.ca', 'alex.gilbert@wheelsanddeals.ca', 'wheelsanddeals@pbssystems.com'];
        $to_addresses = ['alex.gilbert@wheelsanddeals.ca', 'wheelsanddeals@pbssystems.com'];

        foreach ($to_addresses as $to) {
            Mail::to($to)->queue(new TestDriveRequestMailable(($data)));
        }

//        Mail::queue(new TestDriveRequestMailable($data));

        event(new RecordPageViewEvent(\Illuminate\Support\Facades\Request::ip(), \Illuminate\Support\Facades\Request::url(), "Test Drive Form filled out"));

        return "OK";
    }


    /**
     * @return mixed
     */
    public function getTestDrive()
    {
        $app = TestDrive::find(Input::get('id'));
        $car = Vehicle::find($app->vehicle_id);

        return View::make('wheelspackage::admin.test-drive')
            ->with('app', $app)
            ->with('car', $car);
    }


    /**
     * @return mixed
     */
    public function markAsProcessed()
    {
        $id = Input::get('id');
        $app = TestDrive::find($id);
        $app->processed = 1;
        $app->save();

        return Redirect::to('/admin/test-drives/all')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function deleteTestDrive()
    {
        $id = Input::get('id');
        TestDrive::find($id)->delete();

        return Redirect::to('/admin/test-drives/all')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function getAllTestDrives()
    {
        return View::make('wheelspackage::admin.test-drives-all');
    }


    /**
     * @return mixed
     */
    public function allTestDrivesJson()
    {
//        $apps = TestDrive::select(['id', 'users_name', 'created_at', 'updated_at', 'processed']);
//
//        return DataTables::eloquent($apps)
//            ->editColumn('updated_at', function ($data) {
//                return $data->updated_at->diffForHumans();
//            })
//            ->make(true);

        $apps = TestDrive::query();
        return DataTables::eloquent($apps)->make();
    }
}
