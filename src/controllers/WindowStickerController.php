<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

/**
 * Class WindowStickerController
 * @package Tsawler\WheelsPackage
 */
class WindowStickerController extends Controller
{

    /**
     * @throws \setasign\Fpdi\PdfReader\PdfReaderException
     */
    public function getSticker()
    {
        $id = Input::get('id');
        $v = Vehicle::with('make', 'vehicleModel', 'options')->find($id);

        if ($v->hand_picked == 0)
            $this->getStickerNormal($v, $id);
        else
            $this->getStickerHandPicked($v, $id);
    }

    /**
     * @param $v
     * @param $id
     * @throws \setasign\Fpdi\PdfReader\PdfReaderException
     */
    public function getStickerHandPicked($v, $id)
    {
        $pdf = new WindowSticker('P', 'mm', 'Letter');
        $pdf->AddFont('CenturyGothic', '', 'ufonts.com_century-gothic.php');
        $pdf->AddFont('CenturyGothic-Bold', '', 'ufonts.com_century-gothic-bold.php');

        $pdf->SetStyleWriteTag("p", "Helvetica", "N", 10, "0,0,0", 2);
        $pdf->SetStyleWriteTag("ital", "Helvetica", "I", 8, "0,0,0");
        $pdf->SetStyleWriteTag("b", "Helvetica", "B", 10, "0,0,0");

        $pdf->SetMargins(0, 0, 0);
        $pdf->SetAutoPageBreak(true, 28);
        $pdf->setSourceFile(public_path() . "/vendor/wheelspackage/templates/mvi-plus-select.pdf");

        $tplIdx = $pdf->importPage(1);
        $pdf->AddPage();
        $pdf->useTemplate($tplIdx, 0, 0, null, null, true);

        $pdf->setY(13);
        $pdf->setX(10);
        $pdf->SetFont('Helvetica', 'IB', 24);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Write(0, iconv('UTF-8', 'windows-1252', $v->year . " " . $v->make->make . " " . $v->vehicleModel->model));

        $pdf->setX(152);
        $pdf->SetFont('Helvetica', 'IB', 32);
        $pdf->Write(0, iconv('UTF-8', 'windows-1252', "$" . number_format($v->cost, 0)));

        // get options into 3 arrays
        $col1 = [];
        $col3 = [];
        $col2 = [];

        $iteration = 1;

        // now get active vehicle options
        $active_options = [];
        foreach ($v->options()->get() as $option) {
            $active_options[] = $option->option->id;
        }

        foreach ($v->options()->get() as $option) {
            if ($iteration == 1) {
                $col1[] = $option->option->option_name;
            } else if ($iteration == 2) {
                $col2[] = $option->option->option_name;
            } else {
                $col3[] = $option->option->option_name;
            }
            $iteration ++;
            if ($iteration > 3)
                $iteration = 1;
        }

        if (sizeof($col1) > 15)
            $spacing = 2.3;
        else if (sizeof($col1) > 10)
            $spacing = 3.5;
        else if (sizeof($col1) > 5)
            $spacing = 3.8;
        else
            $spacing = 4;

        // print first column
        $pdf->SetTextColor(0, 0, 0);
        $pdf->setFont('CenturyGothic-Bold', '', '12');
        $pdf->SetY(250);
        $pdf->setX(157);
        $pdf->Write(0, iconv('UTF-8', 'windows-1252', "Stock #: " . $v->stock_no));

        $pdf->setFont('CenturyGothic-Bold', '', '20');
        $pdf->SetY(24);
        $pdf->setX(10);
        $pdf->Write(0, iconv('UTF-8', 'windows-1252', number_format($v->odometer, 0) . " Km"));

        $pdf->SetY(35);
        $pdf->setFont('CenturyGothic', '', '11');

        foreach ($col1 as $name => $value) {
            if (in_array($name, $active_options)) {
                $pdf->SetX(10);
                $pdf->SetFont('ZapfDingbats', '', 10);
                $pdf->SetTextColor(60, 169, 0);
                $pdf->Write($spacing, '4');
                $pdf->setFont('CenturyGothic', '', '11');
                $pdf->SetTextColor(0, 0, 0);
            }
            $pdf->setX(10);
            $pdf->MultiCell(0, $spacing, iconv('UTF-8', 'windows-1252', $value), 0, 'L');
            $pdf->Ln();
        }

        $pdf->SetY(35);
        foreach ($col2 as $name => $value) {
            if (in_array($name, $active_options)) {
                $pdf->SetX(75);
                $pdf->SetFont('ZapfDingbats', '', 10);
                $pdf->SetTextColor(60, 169, 0);
                $pdf->Cell(11, 11, '4', 0, 0);
                $pdf->setFont('CenturyGothic', '', '11');
                $pdf->SetTextColor(0, 0, 0);
            }
            $pdf->setX(80);
            $pdf->MultiCell(0, $spacing, iconv('UTF-8', 'windows-1252', $value), 0, 'L');
            $pdf->Ln();
        }

        $pdf->SetY(35);
        foreach ($col3 as $name => $value) {
            if (in_array($name, $active_options)) {
                $pdf->SetX(140);
                $pdf->SetFont('ZapfDingbats', '', 10);
                $pdf->SetTextColor(60, 169, 0);
                $pdf->Cell(11, 11, '4', 0, 0);
                $pdf->setFont('CenturyGothic', '', '11');
                $pdf->SetTextColor(0, 0, 0);
            }
            $pdf->setX(145);
            $pdf->MultiCell(0, $spacing, iconv('UTF-8', 'windows-1252', $value), 0, 'L');
            $pdf->Ln();
        }

        $file = base_path() . "/storage/window_sticker_" . $id . ".pdf";
        $pdf->Output($file, 'I');
    }

    /**
     * @throws \setasign\Fpdi\PdfReader\PdfReaderException
     */
    public function getStickerNormal($v, $id)
    {

        $pdf = new WindowSticker('P', 'mm', 'Letter');
        $pdf->AddFont('CenturyGothic', '', 'ufonts.com_century-gothic.php');
        $pdf->AddFont('CenturyGothic-Bold', '', 'ufonts.com_century-gothic-bold.php');

        $pdf->SetStyleWriteTag("p", "Helvetica", "N", 10, "0,0,0", 2);
        $pdf->SetStyleWriteTag("ital", "Helvetica", "I", 8, "0,0,0");
        $pdf->SetStyleWriteTag("b", "Helvetica", "B", 10, "0,0,0");

        $pdf->SetMargins(0, 0, 0);
        $pdf->SetAutoPageBreak(true, 28);
        $pdf->setSourceFile(public_path() . "/vendor/wheelspackage/templates/window-sticker-oct-2019.pdf");

        $tplIdx = $pdf->importPage(1);
        $pdf->AddPage();
        $pdf->useTemplate($tplIdx, 0, 0, null, null, true);

        $pdf->setY(13);
        $pdf->setX(10);
        $pdf->SetFont('Helvetica', 'IB', 24);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Write(0, iconv('UTF-8', 'windows-1252', $v->year . " " . $v->make->make . " " . $v->vehicleModel->model));

        $pdf->setX(152);
        $pdf->SetFont('Helvetica', 'IB', 32);
        $pdf->Write(0, iconv('UTF-8', 'windows-1252', "$" . number_format($v->total_msr, 0)));

        // get options into 3 arrays
        $col1 = [];
        $col3 = [];
        $col2 = [];

        $iteration = 1;

        // now get active vehicle options
        $active_options = [];
        foreach ($v->options()->get() as $option) {
            $active_options[] = $option->option->id;
        }

        foreach ($v->options()->get() as $option) {
            if ($iteration == 1) {
                $col1[] = $option->option->option_name;
            } else if ($iteration == 2) {
                $col2[] = $option->option->option_name;
            } else {
                $col3[] = $option->option->option_name;
            }
            $iteration ++;
            if ($iteration > 3)
                $iteration = 1;
        }

        if (sizeof($col1) > 15)
            $spacing = 2.3;
        else if (sizeof($col1) > 10)
            $spacing = 3.5;
        else if (sizeof($col1) > 5)
            $spacing = 3.8;
        else
            $spacing = 4;

        // print first column
        $pdf->SetTextColor(0, 0, 0);
        $pdf->setFont('CenturyGothic-Bold', '', '12');
        $pdf->SetY(245);
        $pdf->setX(157);
        $pdf->Write(0, iconv('UTF-8', 'windows-1252', "Stock #: " . $v->stock_no));

        $pdf->setFont('CenturyGothic-Bold', '', '20');
        $pdf->SetY(24);
        $pdf->setX(10);
        $pdf->Write(0, iconv('UTF-8', 'windows-1252', number_format($v->odometer, 0) . " Km"));

        $pdf->setFont('CenturyGothic-Bold', '', '16');

        if ($v->price_for_display == "") {
            $pdf->SetY(22);
            $pdf->setX(10);
            $pdf->MultiCell(193, 3, iconv('UTF-8', 'windows-1252', "$" . number_format($v->cost)), 0, 'R');
        } else if ($v->price_for_display != "") {
            $pdf->SetY(22);
            $pdf->setX(10);
            $pdf->MultiCell(193, 3, iconv('UTF-8', 'windows-1252', $v->price_for_display . " OFF NEW MSRP = $" . number_format($v->cost)), 0, 'R');
        }
        $pdf->SetLineWidth(1.5);
        $pdf->Line(150, 13, 200, 13);

        $pdf->SetY(35);
        $pdf->setFont('CenturyGothic', '', '11');

        foreach ($col1 as $name => $value) {
            if (in_array($name, $active_options)) {
                $pdf->SetX(10);
                $pdf->SetFont('ZapfDingbats', '', 10);
                $pdf->SetTextColor(60, 169, 0);
                $pdf->Write($spacing, '4');
                $pdf->setFont('CenturyGothic', '', '11');
                $pdf->SetTextColor(0, 0, 0);
            }
            $pdf->setX(10);
            $pdf->MultiCell(0, $spacing, iconv('UTF-8', 'windows-1252', $value), 0, 'L');
            $pdf->Ln();
        }

        $pdf->SetY(35);
        foreach ($col2 as $name => $value) {
            if (in_array($name, $active_options)) {
                $pdf->SetX(75);
                $pdf->SetFont('ZapfDingbats', '', 10);
                $pdf->SetTextColor(60, 169, 0);
                $pdf->Cell(11, 11, '4', 0, 0);
                $pdf->setFont('CenturyGothic', '', '11');
                $pdf->SetTextColor(0, 0, 0);
            }
            $pdf->setX(80);
            $pdf->MultiCell(0, $spacing, iconv('UTF-8', 'windows-1252', $value), 0, 'L');
            $pdf->Ln();
        }

        $pdf->SetY(35);
        foreach ($col3 as $name => $value) {
            if (in_array($name, $active_options)) {
                $pdf->SetX(140);
                $pdf->SetFont('ZapfDingbats', '', 10);
                $pdf->SetTextColor(60, 169, 0);
                $pdf->Cell(11, 11, '4', 0, 0);
                $pdf->setFont('CenturyGothic', '', '11');
                $pdf->SetTextColor(0, 0, 0);
            }
            $pdf->setX(145);
            $pdf->MultiCell(0, $spacing, iconv('UTF-8', 'windows-1252', $value), 0, 'L');
            $pdf->Ln();
        }

        $file = base_path() . "/storage/window_sticker_" . $id . ".pdf";
        $pdf->Output($file, 'I');
    }

}
