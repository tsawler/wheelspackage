<?php namespace Tsawler\WheelsPackage;

use App\Events\RecordPageViewEvent;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class TestimonialController
 * @package Tsawler\WheelsPackage
 */
class TestimonialController extends Controller
{

    /**
     * @return mixed
     */
    public static function getTestimonials()
    {
        $results = Testimonial::where('active', '=', '1')->orderBy('id', 'desc')->paginate(10);

        return View::make('wheelspackage::public.customer-testimonials')
            ->with('results', $results);
    }


    /**
     * @return mixed
     */
    public function getTestimonial()
    {
        $testimonial_id = Input::get('id');

        if ($testimonial_id > 0) {
            $testimonial = Testimonial::find($testimonial_id);
        } else {
            $testimonial = new Testimonial();
        }

        return View::make('wheelspackage::admin.customer-testimonial-edit')
            ->with('testimonial', $testimonial)
            ->with('testimonial_id', $testimonial_id);
    }


    /**
     * @return mixed
     */
    public function postTestimonial()
    {
        $id = Input::get('id');

        if ($id > 0) {
            $testimonial = Testimonial::find($id);
        } else {
            $testimonial = new Testimonial();
        }

        $url = Input::get('url');
        $fixed = str_replace("/watch?v=", "/embed/", $url);

        $testimonial->url = $fixed;
        $testimonial->label = Input::get('label');
        $testimonial->active = Input::get('active');
        $testimonial->save();

        return Redirect::to('/admin/testimonials/all-testimonials')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function deleteTestimonial()
    {
        Testimonial::find(Input::get('id'))->delete();

        return Redirect::to('/admin/testimonials/all-testimonials')
            ->with('message', 'Item deleted');
    }


    /**
     * @return mixed
     */
    public function getTestimonialsForAdmin()
    {
        return View::make('wheelspackage::admin.customer-testimonials-all');
    }


    /**
     * @return mixed
     */
    public function allTestimonialsJson()
    {
        $apps = Testimonial::select(['id', 'label', 'created_at', 'updated_at', 'active']);

        return DataTables::of($apps)
            ->editColumn('updated_at', function ($data) {
                return $data->updated_at->diffForHumans();
            })
            ->make(true);
    }

}
