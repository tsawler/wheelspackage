<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class VehicleFinderController
 * @package Tsawler\WheelsPackage
 */
class VehicleFinderController extends Controller
{

    /**
     * @return mixed
     */
    public static function getVehicleFinder()
    {

        $methods = ['email' => 'Email', 'phone' => 'Phone', 'either' => 'Either'];

        return View::make('wheelspackage::public.vehicle-finder-embedded')
            ->with('page_title', 'Huggable Vehicle Finder')
            ->with('methods', $methods);
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function postVehicleFinder(Request $request)
    {

        $this->validate($request, [
            'first_name'           => 'required',
            'last_name'            => 'required',
            'email'                => 'email',
            'g-recaptcha-response' => 'required|captcha',
        ]);

        // save to database
        $finder = new Finder;
        $finder->first_name = Input::get('first_name');
        $finder->last_name = Input::get('last_name');
        $finder->email = Input::get('email');
        $finder->phone = Input::get('phone');
        $finder->contact_method = Input::get('contact_method');
        $finder->year = Input::get('year');
        $finder->make = Input::get('make');
        $finder->model = Input::get('model');
        $finder->save();

        $data = [
            'id'         => $finder->id,
            'first_name' => Input::get('first_name'),
            'last_name'  => Input::get('last_name'),
            'phone'      => Input::get('phone'),
            'email'      => Input::get('email'),
            'contact_method'    => Input::get('contact_method'),
            'year'       => Input::get('year'),
            'make'   => Input::get('make'),
            'model'        => Input::get('model'),
        ];

        Mail::queue(new VehicleFinderMailable($data));

        // give reponse
        return Redirect::to('/vehicle-finder-thanks');
    }


    /**
     * @return mixed
     */
    public function getAllVehicleFinders()
    {
        return View::make('wheelspackage::admin.finder-all');
    }


    /**
     * @return mixed
     */
    public function allVehicleFindersJson()
    {
        $apps = Finder::select(['id', 'first_name', 'last_name', 'created_at', 'updated_at', 'processed']);

        return DataTables::of($apps)
            ->editColumn('updated_at', function ($data) {
                return $data->updated_at->diffForHumans();
            })
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function getFinder()
    {
        $id = Input::get('id');
        $finder = Finder::find($id);

        return View::make('wheelspackage::admin.finder')
            ->with('finder', $finder);
    }


    /**
     * @return mixed
     */
    public function deleteFinder()
    {
        $id = Input::get('id');
        Finder::find($id)->delete();

        return Redirect::to('/admin/finder/all')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function processFinder()
    {
        $id = Input::get('id');
        $item = Finder::find($id);
        $item->processed = 1;
        $item->save();

        return Redirect::to('/admin/finder/all')
            ->with('message', 'Changes saved');
    }

}
