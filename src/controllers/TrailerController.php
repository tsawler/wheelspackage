<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use App\User;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

/**
 * Class TrailerController
 * @package Tsawler\WheelsPackage
 */
class TrailerController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public static function getEmbeddedInventory()
    {
        $make = 0;
        $year = 0;
        $model = 0;
        $price = 0;

        $filter = [];

        $makes = [
            0 => 'All makes',
        ];

        $models = [
            0 => 'All models',
        ];

        $years = [
            0 => 'All Years',
        ];

        $results = Vehicle::select('year')
            ->distinct()
            ->where('vehicle_type', '=', '14')
            ->where('status', '=', '1')
            ->orderBy('year', 'desc')->get();

        foreach ($results as $result) {
            $years[$result->year] = $result->year;
        }

        if (Input::has('price')) {
            // we are filtering input
            $year = Input::get('year');
            $make = Input::get('make');
            $model = Input::get('model');
            $price = Input::get('price');
            $filter = [
                'year'  => $year,
                'make'  => $make,
                'model' => $model,
                'price' => $price,
            ];
            Session::put('filter_vehicles', $filter);
        }

        if (Session::has('filter_vehicles')) {
            $filter = Session::get('filter_vehicles');
            $year = $filter['year'];
            $make = $filter['make'];
            $model = $filter['model'];
            $price = $filter['price'];
        }

        $active_makes = [];
        $results = DB::table('vehicles')
            ->select('vehicle_makes_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('vehicle_type', '=', '14')
            ->get();

        foreach ($results as $result) {
            $active_makes[] = $result->vehicle_makes_id;
        }

        $results = VehicleMake::whereIn('id', $active_makes)->orderBy('make')->get();

        foreach ($results as $result) {
            $makes[$result->id] = $result->make;
        }

        if ($make > 0) {
            $active_models = [];

            $results = DB::table('vehicles')
                ->select('vehicle_models_id')
                ->distinct()
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '14')
                ->where('vehicle_makes_id', '=', $make)->get();

            foreach ($results as $result) {
                $active_models[] = $result->vehicle_models_id;
            }

            $results = VehicleModel::whereIn('id', $active_models)->orderBy('model')->get();

            foreach ($results as $result) {
                $models[$result->id] = $result->model;
            }
        }


        if (sizeof($filter) > 0) {

            $t = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '14');
            if ($make > 0) {
                $t = $t->where('vehicle_makes_id', '=', $make);
            }

            if ($model > 0) {
                $t = $t->where('vehicle_models_id', '=', $model);
            }

            if ($year > 0) {
                $t = $t->where('year', '=', $year);
            }

            if ($price == 0) {
            } else if ($price == 1) {
                $t = $t->orderBy('cost', 'asc');
            } else if ($price == 2) {
                $t = $t->orderBy('cost', 'desc');
            }

            $inventory = $t->orderBy('year', 'desc')->paginate(20);

        } else {
            $inventory = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('vehicle_type', '=', '14')
                ->orderBy('year', 'desc')->paginate(20);
        }

        return View::make('wheelspackage::public.inventory-embedded')
            ->with('inventory', $inventory)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('years', $years)
            ->with('year', $year)
            ->with('make', $make)
            ->with('model', $model)
            ->with('price', $price);
    }


    /**
     * @return mixed
     */
    public function getModelsPublicJson()
    {
        $id = Input::get('id');
        $active_models = [];

        $results = DB::table('vehicles')
            ->select('vehicle_models_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('vehicle_type', '=', '14')
            ->where('vehicle_makes_id', '=', $id)->get();

        foreach ($results as $result) {
            $active_models[] = $result->vehicle_models_id;
        }

        $results = VehicleModel::whereIn('id', $active_models)->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();

        return $results->toJson();
    }
}