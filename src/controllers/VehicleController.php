<?php namespace Tsawler\WheelsPackage;

use App\Events\RecordPageViewEvent;
use App\Page;
use App\Video;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class VehicleController
 * @package Tsawler\WheelsPackage
 */
class VehicleController extends BaseController
{


    /**
     * @return mixed
     */
    public function getInventoryAdmin()
    {
        return View::make('wheelspackage::admin.vehicles-all')
            ->with('status', - 1)
            ->with('json_route', 'datatables-admin-all-inventory.allInventoryJson');
    }


    /**
     * @return mixed
     */
    public function getSoldAdmin()
    {
        return View::make('wheelspackage::admin.vehicles-all')
            ->with('status', 0)
            ->with('json_route', 'datatables-admin-all-inventory.allInventoryJson');
    }


    /**
     * @return mixed
     */
    public function getPendingAdmin()
    {
        return View::make('wheelspackage::admin.vehicles-all')
            ->with('status', 2)
            ->with('json_route', 'datatables-admin-all-inventory.allInventoryJson');
    }


    /**
     * @return mixed
     */
    public function getTradeInsAdmin()
    {
        return View::make('wheelspackage::admin.vehicles-all')
            ->with('status', 3)
            ->with('json_route', 'datatables-admin-all-inventory.allInventoryJson');
    }


    /**
     * @return mixed
     */
    public function getForSaleAdmin()
    {
        return View::make('wheelspackage::admin.vehicles-all')
            ->with('status', 1)
            ->with('json_route', 'datatables-admin-all-inventory.allInventoryJson');
    }


    /**
     * @return mixed
     */
    public function getForSalePowerSportsAdmin()
    {
        return View::make('wheelspackage::admin.vehicles-all-powersports');
    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public static function getEmbeddedHandPickedInventory()
    {
        $make = 0;
        $year = 0;
        $model = 0;
        $price = 0;

        $filter = [];

        $makes = [
            0 => 'All makes',
        ];

        $models = [
            0 => 'All models',
        ];

        $years = [
            0 => 'All Years',
        ];

        $results = Vehicle::select('year')->distinct()
            ->where('status', '=', '1')
            ->where('hand_picked', '=', '1')
            ->orderBy('year', 'desc')->get();

        foreach ($results as $result) {
            $years[$result->year] = $result->year;
        }

        if (Input::has('price')) {
            // we are filtering input
            $year = Input::get('year');
            $make = Input::get('make');
            $model = Input::get('model');
            $price = Input::get('price');
            $filter = [
                'year'  => $year,
                'make'  => $make,
                'model' => $model,
                'price' => $price,
            ];
            Session::put('filter_vehicles', $filter);
        } else {
            Session::forget('filter_vehicles');
        }

        if (Session::has('filter_vehicles')) {
            $filter = Session::get('filter_vehicles');
            $year = $filter['year'];
            $make = $filter['make'];
            $model = $filter['model'];
            $price = $filter['price'];
        }

        $active_makes = [];
        $results = DB::table('vehicles')->select('vehicle_makes_id')
            ->distinct()
            ->where('status', '=', '1')
            ->where('hand_picked', '=', '1')
            ->get();

        foreach ($results as $result) {
            $active_makes[] = $result->vehicle_makes_id;
        }

        $results = VehicleMake::whereIn('id', $active_makes)->orderBy('make')->get();

        foreach ($results as $result) {
            $makes[$result->id] = $result->make;
        }

        if ($make > 0) {
            $active_models = [];

            $results = DB::table('vehicles')
                ->select('vehicle_models_id')
                ->distinct()
                ->where('status', '=', '1')
                ->where('hand_picked', '=', '1')
                ->where('vehicle_makes_id', '=', $make)->get();

            foreach ($results as $result) {
                $active_models[] = $result->vehicle_models_id;
            }

            $results = VehicleModel::whereIn('id', $active_models)->orderBy('model')->get();

            foreach ($results as $result) {
                $models[$result->id] = $result->model;
            }
        }


        if (sizeof($filter) > 0) {

            $t = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')->where('status', '=', '1')->where('hand_picked', '=', '1');
            if ($make > 0) {
                $t = $t->where('vehicle_makes_id', '=', $make);
            }

            if ($model > 0) {
                $t = $t->where('vehicle_models_id', '=', $model);
            }

            if ($year > 0) {
                $t = $t->where('year', '=', $year);
            }

            if ($price == 0) {
            } else if ($price == 1) {
                $t = $t->orderBy('cost', 'asc');
            } else if ($price == 2) {
                $t = $t->orderBy('cost', 'desc');
            }

            $inventory = $t->orderBy('year', 'desc')->paginate(20);

        } else {
            $inventory = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')
                ->where('status', '=', '1')
                ->where('hand_picked', '=', '1')
                ->orderBy('year', 'desc')
                ->paginate(20);
        }

        return View::make('wheelspackage::public.inventory-embedded')
            ->with('inventory', $inventory)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('years', $years)
            ->with('year', $year)
            ->with('make', $make)
            ->with('model', $model)
            ->with('price', $price);
    }


    /**
     * @return mixed
     */
    public static function getEmbeddedInventory()
    {
        $make = 0;
        $year = 0;
        $model = 0;
        $price = 0;

        $filter = [];

        $makes = [
            0 => 'All makes',
        ];

        $models = [
            0 => 'All models',
        ];

        $years = [
            0 => 'All Years',
        ];

        $results = Vehicle::select('year')->distinct()->where('status', '=', '1')->where('vehicle_type', '<', '7')->orderBy('year', 'desc')->get();

        foreach ($results as $result) {
            $years[$result->year] = $result->year;
        }

        if (Input::has('price')) {
            // we are filtering input
            $year = Input::get('year');
            $make = Input::get('make');
            $model = Input::get('model');
            $price = Input::get('price');
            $filter = [
                'year'  => $year,
                'make'  => $make,
                'model' => $model,
                'price' => $price,
            ];
            Session::put('filter_vehicles', $filter);
        }

        if (Session::has('filter_vehicles')) {
            $filter = Session::get('filter_vehicles');
            $year = $filter['year'];
            $make = $filter['make'];
            $model = $filter['model'];
            $price = $filter['price'];
        }

        $active_makes = [];
        $results = DB::table('vehicles')->select('vehicle_makes_id')->distinct()->where('status', '=', '1')->where('vehicle_type', '<', '7')->get();

        foreach ($results as $result) {
            $active_makes[] = $result->vehicle_makes_id;
        }

        $results = VehicleMake::whereIn('id', $active_makes)->orderBy('make')->get();

        foreach ($results as $result) {
            $makes[$result->id] = $result->make;
        }

        if ($make > 0) {
            $active_models = [];

            $results = DB::table('vehicles')
                ->select('vehicle_models_id')
                ->distinct()->where('status', '=', '1')
                ->where('vehicle_makes_id', '=', $make)
                ->where('vehicle_type', '<', '7')->get();

            foreach ($results as $result) {
                $active_models[] = $result->vehicle_models_id;
            }

            $results = VehicleModel::whereIn('id', $active_models)->orderBy('model')->get();

            foreach ($results as $result) {
                $models[$result->id] = $result->model;
            }
        }


        if (sizeof($filter) > 0) {

            $t = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')->where('status', '=', '1')->where('vehicle_type', '<', '7');
            if ($make > 0) {
                $t = $t->where('vehicle_makes_id', '=', $make);
            }

            if ($model > 0) {
                $t = $t->where('vehicle_models_id', '=', $model);
            }

            if ($year > 0) {
                $t = $t->where('year', '=', $year);
            }

            if ($price == 0) {
            } else if ($price == 1) {
                $t = $t->orderBy('cost', 'asc');
            } else if ($price == 2) {
                $t = $t->orderBy('cost', 'desc');
            }

            $inventory = $t->orderBy('year', 'desc')->paginate(20);

        } else {
            $inventory = Vehicle::with('make', 'vehicleModel', 'options', 'firstImage')->where('vehicle_type', '<', '7')->where('status', '=', '1')->orderBy('year', 'desc')->paginate(20);
        }

        return View::make('wheelspackage::public.inventory-embedded')
            ->with('inventory', $inventory)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('years', $years)
            ->with('year', $year)
            ->with('make', $make)
            ->with('model', $model)
            ->with('price', $price);
    }


    /**
     * @return mixed
     */
    public function allInventoryJson()
    {
        $status = 0;



        if (Input::has('status'))
            $status = Input::get('status');

        if ($status == 2) {
            $inventory = Vehicle::with('make', 'vehicleModel')
                ->where('status', '=', $status);
        } else  if ($status > - 1) {
            if ($status == 0) {
                $inventory = Vehicle::with('make', 'vehicleModel')
                    ->where('status', '=', $status)
                    ->where('vehicles.updated_at', '>=', Carbon::now()->subMonth())
                    ->where('vehicle_type', '<', '7');
            } else {
                $inventory = Vehicle::with('make', 'vehicleModel')
                    ->where('status', '=', $status)
                    ->where('vehicle_type', '<', '7');
            }
        } else {
            $inventory = Vehicle::with('make', 'vehicleModel')->where('vehicle_type', '<', '7');
        }


        return DataTables::of($inventory)
            ->addColumn('make', function (Vehicle $vehicle) {
                if ($vehicle->make->make) {
                    return $vehicle->make->make;
                } else {
                    return "";
                }
            })
            ->addColumn('model', function (Vehicle $vehicle) {
//                if ($vehicle->vehicleModel->model->exists()) {
                    return $vehicle->vehicleModel->model;
//                } else {
//                    return "";
//                }
            })
            ->make(true);
    }



    public function allInventoryPowerSportsJson()
    {

        $inventory = Vehicle::with('make', 'vehicleModel')->where('status','=','1')->where('vehicle_type', '>', '6');



        return DataTables::of($inventory)
            ->addColumn('make', function (Vehicle $vehicle) {
                return $vehicle->vehicleModel->make;
            })
            ->addColumn('model', function (Vehicle $vehicle) {
                return $vehicle->vehicleModel->model;
            })
            ->make(true);
    }

    protected function getAllSalesStaffForVehicles() {
        $sales = Sale::where('active','=','1')
            ->limit(6)
            ->orderBy(DB::raw('RAND()'))
            ->get();

        return $sales;
    }


    /**
     * @return mixed
     */
    public function getVehicleForPublic()
    {
        $page = new Page;
        $page->active = 1;
        $id = Request::segment(3);
        $item = Vehicle::with('make', 'vehicleModel', 'options', 'images', 'video')->find($id);

        if ($item->video !== null)
            $video_id = $item->video->video_id;
        else
            $video_id = 0;

        $sales = $this->getAllSalesStaffForVehicles();

        event(new RecordPageViewEvent(Request::ip(), Request::url(), $item->year . " " . $item->make->make . " " . $item->vehicleModel->model . ": " . $item->stock_no));

        return View::make('wheelspackage::public.vehicle')
            ->with('item', $item)
            ->with('vehicle_id', $id)
            ->with('page', $page)
            ->with('page_name', '')
            ->with('video_id', $video_id)
            ->with('sales', $sales);
    }


    /**
     * @return mixed
     */
    public function getVehicleAdmin()
    {
        $makes = [];
        $models = [];
        $options = [];
        $tab = "basic";

        $id = Input::get('id');

        if (Input::has('tab'))
            $tab = Input::get('tab');

        if ($id > 0) {
            $item = Vehicle::with('make', 'vehicleModel', 'images', 'video')->find($id);
            $results = VehicleMake::orderBy('make')->get();

            foreach ($results as $make) {
                $makes[$make->id] = $make->make;
            }

            $results = VehicleModel::where('vehicle_makes_id', '=', $item->vehicle_makes_id)->orderBy('model')->get();

            foreach ($results as $model) {
                $models[$model->id] = $model->model;
            }
        } else {
            $item = new Vehicle();
            $results = VehicleMake::orderBy('make')->get();

            foreach ($results as $make) {
                $makes[$make->id] = $make->make;
            }

            $results = VehicleModel::where('vehicle_makes_id', '=', '1')->orderBy('model')->get();

            foreach ($results as $model) {
                $models[$model->id] = $model->model;
            }
        }

        // get options
        foreach ($item->options()->get() as $result) {
            $options[] = $result->option_id;
        }

        $results = Video::where('converted_for_streaming_at', '!=', null)->orderBy('video_name')->get();

        $videos[0] = "Do not display a video";

        foreach($results as $result) {
            $videos[$result->id] = $result->video_name;
        }



        if ($item->video !== null)
            $video_id = $item->video->video_id;
        else
            $video_id = 0;


        return View::make('wheelspackage::admin.vehicle')
            ->with('item', $item)
            ->with('makes', $makes)
            ->with('models', $models)
            ->with('tab', $tab)
            ->with('vehicle_id', $id)
            ->with('video_id', $video_id)
            ->with('videos', $videos)
            ->with('options', $options);
    }


    /**
     * @return mixed
     */
    public function postVehicleAdmin()
    {
        $id = Input::get('id');
        $tab = Input::get('tab');

        if ($id > 0) {
            $item = Vehicle::find($id);
        } else {
            $item = new Vehicle();
        }

        $item->fill(Input::all());
        $item->description = str_replace("/laravel-filemanager/", "/storage/", Input::get('description'));
        $item->save();
        $id = $item->id;

        // save options
        VehicleOption::where('vehicle_id', '=', $id)->delete();

        foreach (Input::all() as $name => $value) {
            if (starts_with($name, 'option_')) {
                $option = new VehicleOption();
                $option->vehicle_id = $id;
                $option->option_id = $value;
                $option->save();
                Vehicle::find($id)->touch();
            }
        }


        $error = '';

        // images
        if (Input::hasFile('image_name')) {

            $success = FileUploader::uploadFileOnCanvas('image_name', 480, 1200, 'inventory/' . $id, 240, 320);

            if ($success['response'] == 'true') {
                foreach ($success['file_name'] as $item) {
                    $image = new VehicleImage();
                    $image->vehicle_id = $id;
                    $image->image = $item;
                    $image->save();
                }
                Vehicle::find($id)->touch();
            } else {
                $error = "One  or more images could not be processed because the file is not large enough.";
            }
        }

        $sort = json_decode(Input::get('sort_list'));;

        foreach ($sort as $name => $value) {
            $vehicle_image = VehicleImage::find($name);
            $vehicle_image->sort_order = $value;
            $vehicle_image->save();
        }

        // handle video
        $result = VehicleVideo::where('vehicle_id','=',$id)->delete();

        if(Input::get('video_id') > 0) {
            $video = new VehicleVideo;
            $video->vehicle_id = $id;
            $video->video_id = Input::get('video_id');
            $video->save();
        }

        // handle panorama
        if (Input::hasFile('panorama')) {
            $file = Input::file('panorama');
            $extension = $file->getClientOriginalExtension();
            $file_name = $id . "-" . str_random(20) . "." . $extension;

            $success = FileUploader::uploadRegularFile('panorama', 'storage/app/public/panoramas', $file_name);

            if ($success['response'] == 'true') {
                $result = VehiclePanorama::where('vehicle_id','=', $id)->delete();
                $pan = new VehiclePanorama;
                $pan->vehicle_id = $id;
                $pan->panorama = $file_name;
                $pan->save();
                Vehicle::find($id)->touch();
            } else {
                dd("failed");
            }
        }

        if ($error === '') {
            return Redirect::to('/admin/inventory/vehicle?id=' . $id . "&tab=" . $tab)
                ->with('message', 'Changes saved');
        } else {
            return Redirect::to('/admin/inventory/vehicle?id=' . $id . "&tab=" . $tab)
                ->with('message', 'Changes saved')
                ->with('error', $error);
        }

    }


    /**
     * @return mixed
     */
    public function getModelsJson()
    {
        $id = Input::get('id');
        $results = VehicleModel::select('id', 'model')->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();
        $models = [];

        foreach ($results as $item) {
            $models[$item->id] = $item->model;
        }

        return $results->toJson();
    }
    
    /**
     * @return mixed
     */
    public function getModelsPublicJson()
    {
        $id = Input::get('id');
        $active_models = [];

        $results = DB::table('vehicles')
            ->select('vehicle_models_id')
            ->distinct()->where('status', '=', '1')
            ->where('vehicle_makes_id', '=', $id)->get();

        foreach ($results as $result) {
            $active_models[] = $result->vehicle_models_id;
        }

        $results = VehicleModel::whereIn('id', $active_models)->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();

        return $results->toJson();
    }


    /**
     * @return mixed
     */
    public function getModelsHandPickedPublicJson()
    {
        $id = Input::get('id');
        $active_models = [];

        $results = DB::table('vehicles')
            ->select('vehicle_models_id')
            ->distinct()->where('status', '=', '1')
            ->where('hand_picked', '=', '1)')
            ->where('vehicle_makes_id', '=', $id)->get();

        foreach ($results as $result) {
            $active_models[] = $result->vehicle_models_id;
        }

        $results = VehicleModel::whereIn('id', $active_models)->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();

        return $results->toJson();
    }


    /**
     * @return mixed
     */
    public function getDeleteImage()
    {
        $id = Input::get('id');
        $vehicle_id = Input::get('vid');
        $image = VehicleImage::find($id);
        $vid = $image->vehicle_id;
        $filename = $image->image;
        if (File::exists(public_path() . "/storage/inventory/" . $vid . "/" . $filename)) {
            File::delete(public_path() . "/storage/inventory/" . $vid . "/" . $filename);
        }
        if (File::exists(public_path() . "/storage/inventory/" . $vid . "/thumbs/" . $filename)) {
            File::delete(public_path() . "/storage/inventory/" . $vid . "/thumbs/" . $filename);
        }

        $result = VehicleImage::find($id)->delete();

        return Redirect::to('/admin/inventory/vehicle?tab=images&id=' . $vehicle_id)
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function getCompareVehicles()
    {
        $idString = Input::get('ids');
        $ids = explode(",", $idString);
        $vehicle_count = sizeof($ids);

        $vehicle_1 = Vehicle::with('make', 'vehicleModel', 'options')->find($ids[0]);
        $vehicle_2 = Vehicle::with('make', 'vehicleModel', 'options')->find($ids[1]);

        if (sizeof($ids) == 3) {
            $vehicle_3 = Vehicle::with('make', 'vehicleModel', 'options')->find($ids[2]);
            $width = 33;
        } else {
            $vehicle_3 = null;
            $width = 50;
        }

        return View::make('wheelspackage::public.compare')
            ->with('vehicle_count', $vehicle_count)
            ->with('vehicle_1', $vehicle_1)
            ->with('vehicle_2', $vehicle_2)
            ->with('vehicle_3', $vehicle_3)
            ->with('width', $width);
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getRefreshFromPBS()
    {
        Artisan::call('load-inventory');

        return back()->with('message', 'Refresh from PBS completed');
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVideosJson()
    {
        $results = Video::orderBy('video_name')->get();

        $videos = [];

        foreach ($results as $result) {
            $item = [];
            $item['id'] = $result->id;
            $item['name'] = $result->video_name;
            $videos[] = $item;
        }

        return response()->json($videos);
    }
}

