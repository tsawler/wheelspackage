<?php namespace Tsawler\WheelsPackage;

use App\Video;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class PowerSportsController
 * @package Tsawler\WheelsPackage
 */
class PowerSportsController extends BaseController
{


    /**
     * @return mixed
     */
    public function getItems()
    {
        return View::make('wheelspackage::admin.power-sports-all')
            ->with('json_route', 'datatables-admin-all-powersports.allPowerSports');
    }


    /**
     * @return mixed
     */
    public function allitemsJson()
    {

        $inventory = PowerSport::select(['id', 'product_name', 'created_at', 'updated_at', 'year', 'category']);

        return DataTables::of($inventory)
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function getPowerSportAdmin()
    {

        $tab = 'basic';
        $is_new = false;

        if (Input::has('tab'))
            $tab = Input::get('tab');

        $id = Input::get('id');

        if ($id > 0) {
            $item = PowerSport::find($id);
        } else {
            $is_new = true;
            $item = new PowerSport();
        }

        //  get videos
        $results = Video::where('converted_for_streaming_at', '!=', null)->orderBy('video_name')->get();
        $videos[0] = "Do not display a video";

        foreach ($results as $result) {
            $videos[$result->id] = $result->video_name;
        }


        $categories = [
            'bennington-qx'      => 'Bennington QX',
            'bennington-q'       => 'Bennington Q',
            'bennington-r'       => 'Bennington R',
            'bennington-g'       => 'Bennington G',
            'bennington-sx'      => 'Bennington SX',
            'bennington-s'       => 'Bennington S',
            'glastron-bowrider'  => 'Glastron - Bowrider',
            'glastron-skifish'   => 'Glastron - Ski Fish',
            'glastron-deckboat'  => 'Glastron - Deck Boat',
            'glastron-cruiser'   => 'Glastron - Cruiser',
            'supersport'         => 'Kawasaki Motorcycle - Supersport',
            'street'             => 'Kawasaki Motorcycle - Street/Touring',
            'cruiser'            => 'Kawasaki Motorcycle - Cruiser',
            'motocross'          => 'Kawasaki Motorcycle - Motocross',
            'dual'               => 'Kawasaki Motorcycle - Dual Purpose',
            'offroad'            => 'Kawasaki Motorcycle - Offroad',
            'utility'            => 'Kawasaki ATV - Utility',
            'sport'              => 'Kawasaki ATV - Sport',
            'sport'              => 'Kawasaki ATV - Sport',
            'side_utility'       => 'Kawasaki ATV - Side Utility',
            'side_sport_2'       => 'Kawasaki ATV - Side Sport 2',
            'side_sport_4'       => 'Kawasaki ATV - Side Sport 4',
            'jet_ski'            => 'Kawasaki Jet Ski',
            'mercury-pro-xs'     => 'Mercury Pro XS',
            'mercury-verado'     => 'Mercury Verado',
            'mercury-optimax'    => 'Mercury Optimax',
            'mercury-fourstroke' => 'Mercury Fourstroke',
            'mercury-jet'        => 'Mercury Jet',
            'mercury-seapro'     => 'Mercury Seapro Fourstroke',

        ];

        if ($is_new) {
            $images = [];
        } else {
            $temp = $item->images;
            $images = explode("|", $temp);
        }

        return View::make('wheelspackage::admin.power-sports-item')
            ->with('item', $item)
            ->with('tab', $tab)
            ->with('categories', $categories)
            ->with('videos', $videos)
            ->with('images', $images);
    }


    /**
     * @return mixed
     */
    public function postPowerSportAdmin()
    {
        $id = Input::get('id');
        $tab = Input::get('tab');

        if ($id > 0) {
            $item = PowerSport::find($id);
        } else {
            $item = new PowerSport();
        }

        $item->year = Input::get('year');
        $item->product_name = Input::get('product_name');
        $item->msrp = Input::get('msrp');
        $item->category = Input::get('category');
        $item->url = Input::get('url');
        $item->description = strip_tags(Input::get('description'));
        if (Input::get('video_id') > 0)
            $item->video_id = Input::get('video_id');
        else
            $item->video_id = null;
        $item->save();
        $id = $item->id;

        // thumbnail
        if (Input::hasFile('thumbnail')) {
            $uploaded_file = Input::file('thumbnail');
            $destinationPath = base_path() . '/storage/app/public/motorcycles/';

            if (!File::exists($destinationPath . "thumbs")) {
                File::makeDirectory($destinationPath . "thumbs");
            }

            $upload_success = $uploaded_file->move($destinationPath, $id . ".jpg");
        }

        // images
        if (Input::hasFile('image_name')) {
            $uploaded_files = Input::file('image_name');
            $files = array_reverse($uploaded_files);

            $destinationPath = base_path() . '/storage/app/public/motorcycles/' . $id . '/';
            $i = 1;
            if (!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath);
            }

            foreach ($files as $file) {
                $filename_to_save = $i . ".jpg";
                $upload_success = $file->move($destinationPath, $filename_to_save);
                $i ++;
            }
            $images = "";
            for ($x = 1; $x < $i; $x ++) {
                $images .= $x . "|";
            }
            $images = substr($images, 0, - 1);
            $item->images = $images;
            $item->save();
        }


        return Redirect::to('/admin/powersports/item?id=' . $id . "&tab=" . $tab)
            ->with('message', 'Changes saved');


    }


    /**
     * @return mixed
     */
    public function getModelsJson()
    {
        $id = Input::get('id');
        $results = VehicleModel::select('id', 'model')->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();
        $models = [];

        foreach ($results as $item) {
            $models[$item->id] = $item->model;
        }

        return $results->toJson();
    }


    /**
     * @return mixed
     */
    public function getModelsPublicJson()
    {
        $id = Input::get('id');
        $active_models = [];

        $results = DB::table('vehicles')
            ->select('vehicle_models_id')
            ->distinct()->where('status', '=', '1')
            ->where('vehicle_makes_id', '=', $id)->get();

        foreach ($results as $result) {
            $active_models[] = $result->vehicle_models_id;
        }

        $results = VehicleModel::whereIn('id', $active_models)->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();

        return $results->toJson();
    }


    /**
     * @return mixed
     */
    public function getModelsHandPickedPublicJson()
    {
        $id = Input::get('id');
        $active_models = [];

        $results = DB::table('vehicles')
            ->select('vehicle_models_id')
            ->distinct()->where('status', '=', '1')
            ->where('hand_picked', '=', '1)')
            ->where('vehicle_makes_id', '=', $id)->get();

        foreach ($results as $result) {
            $active_models[] = $result->vehicle_models_id;
        }

        $results = VehicleModel::whereIn('id', $active_models)->where('vehicle_makes_id', '=', $id)->orderBy('model')->get();

        return $results->toJson();
    }


    /**
     * @return mixed
     */
    public function getDeleteImage()
    {
        $id = Input::get('id');
        $vehicle_id = Input::get('vid');
        $image = VehicleImage::find($id);
        $vid = $image->vehicle_id;
        $filename = $image->image;
        if (File::exists(public_path() . "/storage/inventory/" . $vid . "/" . $filename)) {
            File::delete(public_path() . "/storage/inventory/" . $vid . "/" . $filename);
        }
        if (File::exists(public_path() . "/storage/inventory/" . $vid . "/thumbs/" . $filename)) {
            File::delete(public_path() . "/storage/inventory/" . $vid . "/thumbs/" . $filename);
        }

        $result = VehicleImage::find($id)->delete();

        return Redirect::to('/admin/inventory/vehicle?tab=images&id=' . $vehicle_id)
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function getCompareVehicles()
    {
        $idString = Input::get('ids');
        $ids = explode(",", $idString);
        $vehicle_count = sizeof($ids);

        $vehicle_1 = Vehicle::with('make', 'vehicleModel', 'options')->find($ids[0]);
        $vehicle_2 = Vehicle::with('make', 'vehicleModel', 'options')->find($ids[1]);

        if (sizeof($ids) == 3) {
            $vehicle_3 = Vehicle::with('make', 'vehicleModel', 'options')->find($ids[2]);
            $width = 33;
        } else {
            $vehicle_3 = null;
            $width = 50;
        }

        return View::make('wheelspackage::public.compare')
            ->with('vehicle_count', $vehicle_count)
            ->with('vehicle_1', $vehicle_1)
            ->with('vehicle_2', $vehicle_2)
            ->with('vehicle_3', $vehicle_3)
            ->with('width', $width);
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getRefreshFromPBS()
    {
        Artisan::call('load-inventory');

        return back()->with('message', 'Refresh from PBS completed');
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVideosJson()
    {
        $results = Video::orderBy('video_name')->get();

        $videos = [];

        foreach ($results as $result) {
            $item = [];
            $item['id'] = $result->id;
            $item['name'] = $result->video_name;
            $videos[] = $item;
        }

        return response()->json($videos);
    }
}
