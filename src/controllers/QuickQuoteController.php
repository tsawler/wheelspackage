<?php namespace Tsawler\WheelsPackage;

use App\Events\RecordPageViewEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class QuickQuoteController
 * @package Tsawler\WheelsPackage
 */
class QuickQuoteController extends Controller
{

    /**
     * @return string
     */
    public function quickQuote()
    {
        $name = Input::get('name');
        $phone = Input::get('phone');
        $id = Input::get('id');
        $email = Input::get('email');
        $msg = Input::get('msg');

        if ($phone == "") {
            $phone = "Phone not supplied";
        }

        if ($email == "") {
            $email = "Email not supplied";
        }

        $qq = new QuickQuote();

        $qq->vehicle_id = $id;
        $qq->users_name = $name;
        $qq->phone = $phone;
        $qq->email = $email;
        $qq->save();

        $data = [
            'users_name'  => $name,
            'users_phone' => $phone,
            'users_email' => $email,
            'vehicle_id'  => $id,
            'talk_about' => $msg,
        ];

        $to_addresses = ['alex.gilbert@wheelsanddeals.ca', 'wheelsanddeals@pbssystems.com'];

        foreach ($to_addresses as $to) {
            Mail::to($to)->queue(new QuickQuoteMailable(($data)));
        }

//        Mail::queue(new QuickQuoteMailable($data));

        event(new RecordPageViewEvent(\Illuminate\Support\Facades\Request::ip(), \Illuminate\Support\Facades\Request::url(), "Quick Quote form filled out"));

        return "OK";
    }


    /**
     * @return mixed
     */
    public function allQuickQuotesJson()
    {
        $apps = QuickQuote::select(['id', 'users_name', 'created_at', 'updated_at', 'processed']);

        return DataTables::of($apps)
            ->editColumn('updated_at', function ($data) {
                return $data->updated_at->diffForHumans();
            })
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function getAllQuickQuotes()
    {
        return View::make('wheelspackage::admin.quick-quotes-all');
    }


    /**
     * @return mixed
     */
    public function getQuote()
    {
        $id = Input::get('id');
        $app = QuickQuote::find($id);

        return View::make('wheelspackage::admin.quick-quote')
            ->with('app', $app);
    }


    /**
     * @return mixed
     */
    public function processQuote()
    {
        $id = Input::get('id');
        $app = QuickQuote::find($id);
        $app->processed = 1;
        $app->save();

        return Redirect::to('/admin/quick-quotes/all')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function deleteQuote()
    {
        $id = Input::get('id');
        QuickQuote::find($id)->delete();

        return Redirect::to('/admin/quick-quotes/all')
            ->with('message', 'Changes saved');
    }
}