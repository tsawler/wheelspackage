<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class StaffController
 * @package Tsawler\WheelsPackage
 */
class StaffController extends Controller
{

    /**
     * @return mixed
     */
    public static function getStaff()
    {
        $staff = Employee::where('active', '=', '1')
            ->orderby('sort_order')
            ->get();

        return View::make('wheelspackage::public.staff')
            ->with('staff', $staff);
    }


    /**
     * @return mixed
     */
    public function getAllStaff()
    {
        return View::make('wheelspackage::admin.staff-all');
    }


    /**
     * @return mixed
     */
    public function allStaffJson()
    {
        $staff = Employee::select(['id', 'first_name', 'last_name', 'position', 'active']);

        return DataTables::of($staff)
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function getStaffMember()
    {
        $id = Input::get('id');
        $staff_id = 0;

        if ($id > 0) {
            $staff = Employee::find($id);
            $staff_id = $staff->id;
        } else {
            $staff = new Employee();
        }

        return View::make('wheelspackage::admin.staff-edit')
            ->with('staff', $staff)
            ->with('staff_id', $staff_id);
    }


    /**
     * @return mixed
     */
    public function postStaffMember()
    {
        $id = Input::get('employee_id');

        if ($id == 0) {
            $staff = new Employee();
        } else {
            $staff = Employee::find($id);
        }

        $staff->first_name = Input::get('first_name');
        $staff->last_name = Input::get('last_name');
        $staff->description = Input::get('description');
        $staff->position = Input::get('position');
        $staff->email = Input::get('email');
        $staff->active = Input::get('active');

        $staff->save();


        if (Input::hasFile('image')) {
            $success = FileUploader::uploadFile('image', '500', '600', 'staff');

            if ($success['response'] == 'true') {
                $staff->image = $success['file_name'];
                $staff->save();
            } else {
                dd("Error!");
            }
        }

        return Redirect::to('/admin/staff/all')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function deleteStaffMember()
    {
        $id = Input::get('id');
        Employee::find($id)->delete();

        return Redirect::to('/admin/staff/all')
            ->with('message', 'Changes saved');

    }


    /**
     * @return mixed
     */
    public function getSetSortOrder()
    {
        $staff = Employee::orderBy('sort_order')->get();

        return View::make('wheelspackage::admin.staff-sort-order')
            ->with('staff', $staff);
    }


    /**
     * @return mixed
     */
    public function postSortOrder()
    {
        // handle sorting
        $sort = json_decode(Input::get('sort_list'));;

        foreach ($sort as $name => $value) {
            $employee = Employee::find($name);
            $employee->sort_order = $value;
            $employee->save();
        }

        return redirect()->back()
            ->with('message', 'Changes saved');
    }

}
