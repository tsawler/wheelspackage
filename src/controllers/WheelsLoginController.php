<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use App\Page;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as RequestFacade;
use Illuminate\Support\Facades\View;

/**
 * Class WheelsLoginController
 * @package Tsawler\WheelsPackage
 */
class WheelsLoginController extends Controller
{

    /**
     * @param AuthenticateUserSocial $authenticateUser
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getSocialLogin(AuthenticateUserSocial $authenticateUser, Request $request)
    {
        $api = RequestFacade::segment(2);

        return $authenticateUser->execute($request->has('code'), $api);
    }



    /**
     * @return mixed
     */
    public function getCreateAccount()
    {
        $page = new Page();
        $page->active = 1;

        return View::make('wheelspackage::public.create-account')
            ->with('page', $page);
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function postCreateAccount(Request $request)
    {

            $this->validate($request, [
                'first_name'           => 'required',
                'last_name'            => 'required',
                'email'                => 'email|unique:users,email',
                'password'             => 'required',
                'g-recaptcha-response' => 'required|captcha',
            ]);


        $user = new User();
        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->login_types_id = 1;
        $user->access_level = 1;
        $user->user_active = 1;
        $user->save();

        Auth::login($user, true);

        // send welcome email
        $mail = Email::find(1);
        $content = $mail->message;
        $subject = $mail->subject;

        $data = [
            'name'  => Input::get('first_name'),
            'content' => $content,
            'subject' => $subject,
        ];

        Mail::to(Input::get('email'))
            ->queue(new GenericMailable($data));

        return Redirect::to('/')
            ->with('modal_alert', 'Your account has been created. Welcome to Wheels and Deals!');

    }


}
