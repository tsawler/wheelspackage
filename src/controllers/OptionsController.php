<?php namespace Tsawler\WheelsPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class OptionsController
 * @package Tsawler\WheelsPackage
 */
class OptionsController extends Controller
{

    /**
     * @return mixed
     */
    public function allOptionsJson()
    {
        $options = Option::select(['id', 'option_name', 'created_at', 'updated_at', 'active']);

        return DataTables::of($options)
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function getAllOptions()
    {
        $options = Option::orderBy('option_name')->get();

        return View::make('admin.options-all-options')
            ->with('options', $options);
    }


    /**
     * @return mixed
     */
    public function getOption()
    {
        $id = Input::get('id');

        if ($id > 0) {
            $option = Option::find($id);
        } else {
            $option = new Option();
        }

        return View::make('admin.option-edit')
            ->with('option', $option)
            ->with('option_id', $id);
    }


    /**
     * @return mixed
     */
    public function postOption()
    {
        $id = Input::get('id');
        if ($id > 0) {
            $option = Option::find($id);
        } else {
            $option = new Option();
        }
        $option->option_name = Input::get('option_name');
        $option->active = Input::get('active');
        $option->save();

        return Redirect::to('/admin/inventory/options-all')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function deleteOption()
    {
        $id = Input::get('id');
        Option::find($id)->delete();

        return Redirect::to('/admin/inventory/options-all')
            ->with('message', 'Option deleted');
    }

}
