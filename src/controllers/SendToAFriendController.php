<?php namespace Tsawler\WheelsPackage;

use App\Events\RecordPageViewEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendToAFriendController
 * @package Tsawler\WheelsPackage
 */
class SendToAFriendController extends Controller
{

    public function sendMessage()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $name = Input::get('name');
        $id = Input::get('id');

        $data = [
            'users_name' => $name,
            'vehicle_id' => $id,
        ];

        Mail::to(Input::get('to'))
            ->queue(new SendToAFriendMailable($data));

        event(new RecordPageViewEvent(\Illuminate\Support\Facades\Request::ip(), \Illuminate\Support\Facades\Request::url(), "Send to a Friend form filled out"));

        return "OK";
    }
}
