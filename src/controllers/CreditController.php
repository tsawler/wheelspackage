<?php namespace Tsawler\WheelsPackage;

use App\Events\RecordPageViewEvent;
use Aws\Credentials\CredentialProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class CreditController
 * @package Tsawler\WheelsPackage
 */
class CreditController extends BaseController
{


    /**
     * @return mixed
     */
    public static function getCreditApp()
    {

        return View::make('wheelspackage::public.credit-app-embedded');
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function postCreditApp(Request $request)
    {
        if (env('APP_DEBUG')) {
            $this->validate($request, [
                'first_name' => 'required',
                'last_name'  => 'required',
                'email'      => 'email',
                'phone'      => 'required',
                'address'    => 'required',
                'city'       => 'required',
                'province'   => 'required',
                'zip'        => 'required',
                'authorize'  => 'required',
            ]);
        } else {
            $this->validate($request, [
                'first_name'           => 'required',
                'last_name'            => 'required',
                'dob'                  => 'required',
                'email'                => 'email',
                'phone'                => 'required',
                'address'              => 'required',
                'city'                 => 'required',
                'province'             => 'required',
                'zip'                  => 'required',
                'authorize'            => 'required',
                'g-recaptcha-response' => 'required|captcha',
            ]);
        }

        $app = new CreditApplication;
        $app->first_name = Input::get('first_name');
        $app->last_name = Input::get('last_name');
        $app->email = Input::get('email');
        $app->phone = Input::get('phone');
        $app->address = Input::get('address');
        $app->city = Input::get('city');
        $app->province = Input::get('province');
        $app->zip = Input::get('zip');
        $app->vehicle = Input::get('vehicle');
        $app->save();

        Mail::to(Input::get('email'))
            ->queue(new CreditAppThanksMailable(Input::get('first_name')));

        $data = [
            'id'         => $app->id,
            'first_name' => Input::get('first_name'),
            'last_name'  => Input::get('last_name'),
            'phone'      => Input::get('phone'),
            'email'      => Input::get('email'),
            'address'    => Input::get('address'),
            'city'       => Input::get('city'),
            'province'   => Input::get('province'),
            'zip'        => Input::get('zip'),
            'vehicle'    => Input::get('vehicle'),
            'income'     => Input::get('income'),
            'rent'       => Input::get('rent'),
            'dob'        => Input::get('dob'),
            'employer'   => Input::get('employer'),
        ];

//        $to_addresses = ['john.eliakis@wheelsanddeals.ca', 'alex.gilbert@wheelsanddeals.ca', 'chelsea.gilbert@wheelsanddeals.ca', 'wheelsanddeals@pbssystems.com'];
        $to_addresses = ['alex.gilbert@wheelsanddeals.ca', 'chelsea.gilbert@wheelsanddeals.ca', 'wheelsanddeals@pbssystems.com'];

        foreach ($to_addresses as $to) {
            Mail::to($to)->queue(new CreditAppMailable(($data)));
        }

        event(new RecordPageViewEvent(\Illuminate\Support\Facades\Request::ip(), \Illuminate\Support\Facades\Request::url(), "Credit Application filled out"));

        // give reponse
        return Redirect::to('/credit-app-thanks');

    }


    /**
     * @return mixed
     */
    public function getAllCreditApps()
    {
        return View::make('wheelspackage::admin.credit-all-apps');
    }


    /**
     * @return mixed
     */
    public function allCreditAppsJson()
    {
        $apps = CreditApplication::select(['id', 'first_name', 'last_name', 'created_at', 'updated_at', 'processed']);

        return DataTables::of($apps)
            ->editColumn('updated_at', function ($data) {
                return $data->updated_at->diffForHumans();
            })
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function getApp()
    {
        $id = Input::get('id');
        $app = CreditApplication::find($id);

        return View::make('wheelspackage::admin.credit-app')
            ->with('app', $app);
    }


    /**
     * @return mixed
     */
    public function processApp()
    {
        $id = Input::get('id');
        $app = CreditApplication::find($id);
        $app->processed = 1;
        $app->save();

        return Redirect::to('/admin/credit/all')
            ->with('message', 'Changes saved');
    }


    /**
     * @return mixed
     */
    public function deleteApp()
    {
        $id = Input::get('id');
        CreditApplication::find($id)->delete();

        return Redirect::to('/admin/credit/all')
            ->with('message', 'Changes saved');
    }

}
