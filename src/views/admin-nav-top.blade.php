@if (Auth::user()->hasRole('inventory'))
    @if (Request::segment(2) == 'inventory')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-car"></i> <span class="nav-label">Vehicles</span><span
                    class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/inventory/all-vehicles">All Vehicles <span class="badge">{!! \Tsawler\WheelsPackage\Vehicle::count() !!}</span></a></li>
            <li><a href="/admin/inventory/all-vehicles-for-sale">For Sale <span class="badge">{!! \Tsawler\WheelsPackage\Vehicle::where('status','=', '1')->where('vehicle_type', '<', '7')->count() !!}</span></a></li>
            <li><a href="/admin/inventory/all-powersports-for-sale">For Sale Powersports <span class="badge">{!! \Tsawler\WheelsPackage\Vehicle::where('status','=', '1')->where('vehicle_type', '>', '6')->count() !!}</span></a></li>
            <li><a href="/admin/inventory/all-vehicles-sold">Sold (this month) <span class="badge">{!! \Tsawler\WheelsPackage\Vehicle::where('status','=', '0')->where('updated_at', '>=', \Carbon\Carbon::now()->subMonth())->count() !!}</span></a></li>
            <li><a href="/admin/inventory/all-vehicles-pending">Pending <span class="badge">{!! \Tsawler\WheelsPackage\Vehicle::where('status','=', '2')->count() !!}</span></a></li>
            <li><a href="/admin/inventory/all-vehicles-trade-in">Trade ins <span class="badge">{!! \Tsawler\WheelsPackage\Vehicle::where('status','=', '3')->where('vehicle_type', '<', '7')->count() !!}</span></a></li>
            <li><a href="/admin/inventory/vehicle?id=0">Add Vehicle</a></li>
            <li><a href="/admin/inventory/options-all">All Options</a></li>
            <li><a href="/admin/inventory/option?id=0">Add Option</a></li>
            <li><a href="/admin/inventory/refresh-from-pbs">Force PBS update</a></li>
        </ul>
    </li>
@endif


@if (Auth::user()->hasRole('credit'))
    @if (Request::segment(2) == 'credit')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-dollar fa-fw"></i> <span
                    class="nav-label">Credit Apps</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/credit/all">All Credit Applications</a></li>
        </ul>
    </li>
@endif

@if (Auth::user()->hasRole('credit'))
    @if (Request::segment(2) == 'credit')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-dollar fa-fw"></i> <span
                    class="nav-label">Quick Quotes</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/quick-quotes/all">All Quick Quotes</a></li>
        </ul>
    </li>
@endif

@if (Auth::user()->hasRole('finder'))
    @if (Request::segment(2) == 'finder')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-search"></i> <span
                    class="nav-label">Vehicle Finder</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/finder/all">All Vehicle Finder Requests</a></li>
        </ul>
    </li>
@endif

@if (Auth::user()->hasRole('test_drive'))
    @if (Request::segment(2) == 'test-drives')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-calendar fa-fw"></i> <span
                    class="nav-label">Test Drives</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/test-drives/all">All Test Drive Requests</a></li>
        </ul>
    </li>
@endif

@if (Auth::user()->hasRole('staff'))
    @if (Request::segment(2) == 'staff')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-user fa-fw"></i> <span
                    class="nav-label">Staff</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/staff/all">All Staff</a></li>
            <li><a href="/admin/staff/staff?id=0">Add Staff</a></li>
            <li><a href="/admin/staff/sort">Set Sort Order</a></li>
        </ul>
    </li>
@endif

@if (Auth::user()->hasRole('staff'))
    @if (Request::segment(2) == 'sales')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-user fa-fw"></i> <span
                    class="nav-label">Sales People</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/sales/all-sales-people">All Sales People</a></li>
            <li><a href="/admin/sales/salesperson?id=0">Add Sales Person</a></li>
        </ul>
    </li>
@endif

@if (Auth::user()->hasRole('pages'))
    @if (Request::segment(2) == 'testimonials')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Testimonials</span><span
                    class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/testimonials/all-testimonials">All Testimonials</a></li>
            <li><a href="/admin/testimonials/testimonial?id=0">Add Testimonial</a></li>
        </ul>
    </li>
@endif

@if (Auth::user()->hasRole('word'))
    @if (Request::segment(2) == 'word')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Word of Mouth</span><span
                    class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/word/all">All Items</a></li>
            <li><a href="/admin/word/edit?id=0">Add Item</a></li>
        </ul>
    </li>
@endif

@if (Auth::user()->hasRole('email'))
    @if (Request::segment(2) == 'email')
        <li class='active'>
    @else
        <li>
        @endif
        <a href=""><i class="fa fa-envelope fa-fw"></i> <span class="nav-label">Notifications</span><span
                    class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/email/welcome-email">Welcome Email</a></li>
        </ul>
    </li>
@endif