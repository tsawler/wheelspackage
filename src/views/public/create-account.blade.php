@extends('public.base-inside')

@section('css')
    <link href="/css/bootstrap-social.css" rel="stylesheet">
@stop

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <div class="center-heading">
                <h2>Create Account</h2>
                <span class="center-line"></span>
            </div>
            <!-- Form -->
        {!! Form::open(array(
        'url' => '/wheels/user/create',
        'role' => 'form',
        'name' => 'bookform',
        'id' => 'bookform',
        'method' => 'post',
        'class' => 'form-horizontal'
        ))
        !!}

        <!-- Form Group -->

            <div class="form-group">
                <!-- Label -->
                <label for="first_name" class="col-sm-3 control-label">First Name</label>

                <div class="col-sm-6">
                    <!-- Input -->
                    {!! Form::text('first_name', null, array('class' => 'required form-control',
                    '',
                    'autofocus'=>'autofocus')) !!}
                </div>
            </div>

            <div class="form-group">
                <!-- Label -->
                <label for="last_name" class="col-sm-3 control-label">Last Name</label>

                <div class="col-sm-6">
                    <!-- Input -->
                    {!! Form::text('last_name', null, array('class' => 'required form-control',
                    '')) !!}
                </div>
            </div>


            <div class="form-group">
                <!-- Label -->
                <label for="email" class="col-sm-3 control-label">Email</label>

                <div class="col-sm-6">
                    <!-- Input -->
                    {!! Form::email('email', null, array('class' => 'required email form-control',
                    'placeholder' => 'you@example.com',
                    'id' => 'email')) !!}
                </div>
            </div>

            <div class="form-group">
                <!-- Label -->
                <label for="verify_email" class="col-sm-3 control-label">Verify Email</label>

                <div class="col-sm-6">
                    <!-- Input -->
                    {!! Form::email('verify_email', null, array('class' => 'required email form-control',
                    'placeholder' => 'you@example.com',
                    'autofocus'=>'autofocus')) !!}
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Password</label>

                <div class="col-sm-6">
                    {!! Form::password('password', array('class' => 'form-control required',
                    'placeholder' => 'Password', 'id' => 'password')) !!}
                </div>
            </div>

            <div class="form-group">
                <label for="verify_password" class="col-sm-3 control-label">Verify Password</label>

                <div class="col-sm-6">
                    {!! Form::password('verify_password', array('class' => 'form-control required',
                    'placeholder' => 'Verify Password')) !!}
                </div>
            </div>

            <br>
            <div class="form-group">
                <label for="model" class="col-sm-3 control-label">Prove you're human:</label>
                <div class="col-sm-9">
                    {!! NoCaptcha::display() !!}
                </div>
            </div>

            <hr>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <!-- Button -->
                    <button type="submit" class="btn btn-primary">Create Account</button>
                </div>
            </div>

            {!! Form::close() !!}
            <br/>
        </div>
    </div>
    </div>


@stop

@section('bottom-js')
    <script>
        $(document).ready(function () {
            $("#bookform").validate({
                rules: {
                    verify_email: {
                        equalTo: "#email"
                    },
                    verify_password: {
                        equalTo: "#password"
                    }
                },
                errorClass:'text-danger',
                validClass:'text-success',
                errorElement:'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".text-danger").removeClass(errorClass).addClass(validClass);
                }
            });
        });
    </script>
    {!! NoCaptcha::renderJs() !!}
@stop