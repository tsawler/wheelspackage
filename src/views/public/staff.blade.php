
<div class="row">
    @foreach($staff as $item)
        <div class="col-md-4 margin20">
            <div class="team-wrap">
                <img src="/storage/staff/{!! $item->image !!}" class="img img-responsive center-block" alt="">
                <h4><a href="#staff_{{ $item->id }}" data-toggle="collapse">
                    {!! $item->first_name !!} {!! $item->last_name !!}
                </a></h4>
                <span>{!! $item->position !!}</span>
                <div class="collapse" id="staff_{{ $item->id }}">
                    <p>
                        {!! $item->description !!}
                    </p>
                    <ul class="list-inline">
                        <li><a href="#"><i class="fa fa-envelope"></i> {!! $item->email !!}</a></li>
                    </ul><!--social-->
                </div>
            </div><!--team-wrap-->
        </div><!--team col-->
        @if($loop->iteration %3 == 0)
            </div>
            <div class="row">
        @endif
    @endforeach
</div>

