@extends('public.base-inside')

@section('css')
    <style>
        .center-image {
            margin-left: auto !important;
            margin-right: auto !important;
        }
    </style>
@stop

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="center-heading">
                    <h2>Kawasaki Motorcycles</h2>
                    <span class="center-line"></span>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <ul class="nav nav-pills" role="tablist" id="myTab">
                            <li role="presentation" class="active">
                            	<a href="#super" aria-controls="supersports" role="tab" data-toggle="tab">Supersport</a>
                            </li>
                            <li role="presentation">
                            	<a href="#street" aria-controls="street" role="tab" data-toggle="tab">Street/Touring</a>
                            </li>
                            <li role="presentation">
                            	<a href="#cruiser" aria-controls="cruiser" role="tab" data-toggle="tab">Cruiser</a>
                            </li>
                            <li role="presentation"><a href="#motocross" aria-controls="motocross" role="tab" data-toggle="tab">Motocross</a>
                            </li>
                            <li role="presentation">
                            	<a href="#dual" aria-controls="dual" role="tab" data-toggle="tab">Dual Purpose</a>
                            </li>
                            <li role="presentation">
                            	<a href="#offroad" aria-controls="offroad" role="tab" data-toggle="tab">Offroad</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="super">
                                <div class="row">

                                                    @if(sizeof($supersport) > 0)
														@foreach($supersport as $i)
															<div class="col-md-4 text-center">
																<a href="/wheels/motorcycle/{!! $i->id !!}">
																	<img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
																		 class="img img-responsive thumbnail center-image">
																</a>
																{!! $i->product_name !!}<br>
																Starting at <strong>{!! $i->msrp !!}</strong>
															</div>

															@if(($loop->iteration % 3) == 0)
												</div>
																@if(! $loop->last)
												<div class="row">
												
																@endif
															@endif
															
															@if(($loop->last) && (($loop->iteration % 3) != 0))
																</div>
															@endif

														@endforeach
                                @else
                            </div>
                            @endif



                                </div>

                                <div role="tabpanel" class="tab-pane " id="street">
                                    <div class="row">
                                        @if(sizeof($street) >0)
                                        @foreach($street as $i)
                                            <div class="col-md-4 text-center">
                                                <a href="/wheels/motorcycle/{!! $i->id !!}">
                                                    <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                         class="img img-responsive thumbnail center-image">
                                                </a>
                                                {!! $i->product_name !!}<br>
                                                Starting at <strong>{!! $i->msrp !!}</strong>
                                            </div>

                                            @if(($loop->iteration % 3) == 0)
                                </div>
                                                @if(! $loop->last)
                                <div class="row">
                                                @endif
                                            @endif
                                            
                                            @if(($loop->last) && (($loop->iteration % 3) != 0))
																</div>
															@endif


                                        @endforeach
                                    @else
                                </div>
                        @endif


                                </div>
                                <div role="tabpanel" class="tab-pane " id="cruiser">
									<div class="row">

                                        @if(sizeof($cruiser) > 0)
                                        @foreach($cruiser as $i)
                                            <div class="col-md-4 text-center">
                                                <a href="/wheels/motorcycle/{!! $i->id !!}">
                                                    <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                         class="img img-responsive thumbnail center-image">
                                                </a>
                                                {!! $i->product_name !!}<br>
                                                Starting at <strong>{!! $i->msrp !!}</strong>
                                            </div>

                                            @if(($loop->iteration % 3) == 0)
                                </div>
                                                @if(! $loop->last)
                                <div class="row">
                                                @endif
                                            @endif
                                            
                                            @if(($loop->last) && (($loop->iteration % 3) != 0))
																</div>
															@endif

                                        @endforeach
                                    @else
                                        Nothing here
                                </div>
                    @endif

                                
                                </div>

                                <div role="tabpanel" class="tab-pane " id="motocross">
									<div class="row">

                                        @if(sizeof($motocross) > 0)
                                        @foreach($motocross as $i)
                                            <div class="col-md-4 text-center">
                                                <a href="/wheels/motorcycle/{!! $i->id !!}">
                                                    <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                         class="img img-responsive thumbnail center-image">
                                                </a>
                                                {!! $i->product_name !!}<br>
                                                Starting at <strong>{!! $i->msrp !!}</strong>
                                            </div>

                                            @if(($loop->iteration % 3) == 0)
                                </div>
                                                @if(! $loop->last)
                                <div class="row">
                                                @endif
                                            @endif
											@if(($loop->last) && (($loop->iteration % 3) != 0))
																</div>
															@endif
                                        @endforeach
                                    @else
                                        Nothing here
                                </div>
                @endif


                                </div>

                                <div role="tabpanel" class="tab-pane " id="dual">
									<div class="row">

                                        @if(sizeof($dual) > 0)
                                        @foreach($dual as $i)
                                            <div class="col-md-4 text-center">
                                                <a href="/wheels/motorcycle/{!! $i->id !!}">
                                                    <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                         class="img img-responsive thumbnail center-image">
                                                </a>
                                                {!! $i->product_name !!}<br>
                                                Starting at <strong>{!! $i->msrp !!}</strong>
                                            </div>

                                            @if(($loop->iteration % 3) == 0)
                                </div>
                                                @if(! $loop->last)
                                <div class="row">
                                                @endif
                                            @endif
                                            
                                            @if(($loop->last) && (($loop->iteration % 3) != 0))
																</div>
															@endif

                                        @endforeach
                                    @else
                                        Nothing here
                                </div>
            @endif



                                </div>

                                <div role="tabpanel" class="tab-pane " id="offroad">
									<div class="row">

                                        @if(sizeof($offroad) > 0)
                                        @foreach($offroad as $i)
                                            <div class="col-md-4 text-center">
                                                <a href="/wheels/motorcycle/{!! $i->id !!}">
                                                    <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                         class="img img-responsive thumbnail center-image">
                                                </a>
                                                {!! $i->product_name !!}<br>
                                                Starting at <strong>{!! $i->msrp !!}</strong>
                                            </div>

                                            @if(($loop->iteration % 3) == 0)
                                </div>
                                                @if(! $loop->last)
                                <div class="row">
                                                @endif
                                            @endif
                                            
                                            @if(($loop->last) && (($loop->iteration % 3) != 0))
																</div>
															@endif

                                        @endforeach
                                    @else
                                        Nothing here
                                </div>
        @endif


                                </div>


                            </div>
                        </div>

                </div>
                    </div>
                </div>
            </div>
    </div>

@stop

@section('bottom-js')
<script>
    if (location.hash) {
        $('a[href=\'' + location.hash + '\']').tab('show');
    }
    var activeTab = localStorage.getItem('activeTab');
    if (activeTab) {
        $('a[href="' + activeTab + '"]').tab('show');
    }

    $('body').on('click', 'a[data-toggle=\'tab\']', function (e) {
        e.preventDefault()
        var tab_name = this.getAttribute('href')
        if (history.pushState) {
            history.pushState(null, null, tab_name)
        }
        else {
            location.hash = tab_name
        }
        localStorage.setItem('activeTab', tab_name)

        $(this).tab('show');
        return false;
    });
    $(window).on('popstate', function () {
        var anchor = location.hash ||
            $('a[data-toggle=\'tab\']').first().attr('href');
        $('a[href=\'' + anchor + '\']').tab('show');
    });
</script>
@stop