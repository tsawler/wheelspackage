
<div class="row">
    <div class="col-md-12">

        <div class="pull-right">{{ $results->links() }}</div>
        <div class="clearfix"></div>
        @foreach ($results as $item)
            <h3>{!! $item->title !!}</h3>
            {!! $item->content !!}
            <hr>
        @endforeach

        <div class="pull-right">{{ $results->links() }}</div>

    </div>
</div>
