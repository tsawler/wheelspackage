@extends('public.base-inside')

@section('css')
    <style>
        .center-image {
            margin-left: auto !important;
            margin-right: auto !important;
        }
    </style>
@stop

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="center-heading">
                    <h2>Kawasaki ATV/Teryx/Mule/Jet Ski</h2>
                    <span class="center-line"></span>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <ul class="nav nav-pills" role="tablist" id="myTab">
                            <li role="presentation" class="active">
                                <a href="#utility" aria-controls="#utility" role="tab" data-toggle="tab">ATV Utility</a>
                            </li>
                            <li role="presentation">
                                <a href="#sport" aria-controls="#sport" role="tab" data-toggle="tab">ATV Sport</a>
                            </li>
                            <li role="presentation">
                                <a href="#side_utility" aria-controls="#side_utility" role="tab" data-toggle="tab">Side X Side Utility</a>
                            </li>
                            <li role="presentation"><a href="#side_sport_2" aria-controls="side sport 2" role="tab" data-toggle="tab">Side X Side Sport 2</a>
                            </li>
                            <li role="presentation">
                                <a href="#side_sport_4" aria-controls="side sport 4" role="tab" data-toggle="tab">Side X Side Sport 4</a>
                            </li>
                            <li role="presentation">
                                <a href="#jet_ski" aria-controls="jet ski" role="tab" data-toggle="tab">Jet Ski</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="utility">
                                <div class="row">

                                    @if(sizeof($utility) > 0)
                                        @foreach($utility as $i)
                                            <div class="col-md-4 text-center">
                                                <a href="/wheels/atv/{!! $i->id !!}">
                                                    <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                         class="img img-responsive thumbnail center-image">
                                                </a>
                                                {!! $i->product_name !!}<br>
                                                Starting at <strong>{!! $i->msrp !!}</strong>
                                            </div>

                                            @if(($loop->iteration % 3) == 0)
                                </div>
                                @if(! $loop->last)
                                    <div class="row">

                                        @endif
                                        @endif

                                        @if(($loop->last) && (($loop->iteration % 3) != 0))
                                    </div>
                                @endif

                                @endforeach
                                @else
                            </div>
                            @endif



                        </div>

                        <div role="tabpanel" class="tab-pane " id="sport">
                            <div class="row">
                                @if(sizeof($sport) >0)
                                    @foreach($sport as $i)
                                        <div class="col-md-4 text-center">
                                            <a href="/wheels/atv/{!! $i->id !!}">
                                                <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                     class="img img-responsive thumbnail center-image">
                                            </a>
                                            {!! $i->product_name !!}<br>
                                            Starting at <strong>{!! $i->msrp !!}</strong>
                                        </div>

                                        @if(($loop->iteration % 3) == 0)
                            </div>
                            @if(! $loop->last)
                                <div class="row">
                                    @endif
                                    @endif

                                    @if(($loop->last) && (($loop->iteration % 3) != 0))
                                </div>
                            @endif


                            @endforeach
                            @else
                        </div>
                        @endif


                    </div>
                    <div role="tabpanel" class="tab-pane " id="side_utility">
                        <div class="row">

                            @if(sizeof($side_utility) > 0)
                                @foreach($side_utility as $i)
                                    <div class="col-md-4 text-center">
                                        <a href="/wheels/atv/{!! $i->id !!}">
                                            <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                 class="img img-responsive thumbnail center-image">
                                        </a>
                                        {!! $i->product_name !!}<br>
                                        Starting at <strong>{!! $i->msrp !!}</strong>
                                    </div>

                                    @if(($loop->iteration % 3) == 0)
                        </div>
                        @if(! $loop->last)
                            <div class="row">
                                @endif
                                @endif

                                @if(($loop->last) && (($loop->iteration % 3) != 0))
                            </div>
                        @endif

                        @endforeach
                        @else
                            Nothing here
                    </div>
                    @endif


                </div>

                <div role="tabpanel" class="tab-pane " id="side_sport_2">
                    <div class="row">

                        @if(sizeof($side_sport_2) > 0)
                            @foreach($side_sport_2 as $i)
                                <div class="col-md-4 text-center">
                                    <a href="/wheels/atv/{!! $i->id !!}">
                                        <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                             class="img img-responsive thumbnail center-image">
                                    </a>
                                    {!! $i->product_name !!}<br>
                                    Starting at <strong>{!! $i->msrp !!}</strong>
                                </div>

                                @if(($loop->iteration % 3) == 0)
                    </div>
                    @if(! $loop->last)
                        <div class="row">
                            @endif
                            @endif
                            @if(($loop->last) && (($loop->iteration % 3) != 0))
                        </div>
                    @endif
                    @endforeach
                    @else
                        Nothing here
                </div>
                @endif


            </div>

            <div role="tabpanel" class="tab-pane " id="side_sport_4">
                <div class="row">

                    @if(sizeof($side_sport_4) > 0)
                        @foreach($side_sport_4 as $i)
                            <div class="col-md-4 text-center">
                                <a href="/wheels/atv/{!! $i->id !!}">
                                    <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                         class="img img-responsive thumbnail center-image">
                                </a>
                                {!! $i->product_name !!}<br>
                                Starting at <strong>{!! $i->msrp !!}</strong>
                            </div>

                            @if(($loop->iteration % 3) == 0)
                </div>
                @if(! $loop->last)
                    <div class="row">
                        @endif
                        @endif

                        @if(($loop->last) && (($loop->iteration % 3) != 0))
                    </div>
                @endif

                @endforeach
                @else
                    Nothing here
            </div>
            @endif



        </div>

        <div role="tabpanel" class="tab-pane " id="jet_ski">
            <div class="row">

                @if(sizeof($jetski) > 0)
                    @foreach($jetski as $i)
                        <div class="col-md-4 text-center">
                            <a href="/wheels/atv/{!! $i->id !!}">
                                <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                     class="img img-responsive thumbnail center-image">
                            </a>
                            {!! $i->product_name !!}<br>
                            Starting at <strong>{!! $i->msrp !!}</strong>
                        </div>

                        @if(($loop->iteration % 3) == 0)
            </div>
            @if(! $loop->last)
                <div class="row">
                    @endif
                    @endif

                    @if(($loop->last) && (($loop->iteration % 3) != 0))
                </div>
            @endif

            @endforeach
            @else
                Nothing here
        </div>
        @endif


    </div>


    </div>
    </div>

    </div>
    </div>
    </div>
    </div>
    </div>

@stop

@section('bottom-js')
    <script>
        if (location.hash) {
            $('a[href=\'' + location.hash + '\']').tab('show');
        }
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('a[href="' + activeTab + '"]').tab('show');
        }

        $('body').on('click', 'a[data-toggle=\'tab\']', function (e) {
            e.preventDefault()
            var tab_name = this.getAttribute('href')
            if (history.pushState) {
                history.pushState(null, null, tab_name)
            }
            else {
                location.hash = tab_name
            }
            localStorage.setItem('activeTab', tab_name)

            $(this).tab('show');
            return false;
        });
        $(window).on('popstate', function () {
            var anchor = location.hash ||
                $('a[data-toggle=\'tab\']').first().attr('href');
            $('a[href=\'' + anchor + '\']').tab('show');
        });
    </script>
@stop