@extends('public.base-inside')

@section('css')
    <style>
        .center-image {
            margin-left: auto !important;
            margin-right: auto !important;
        }
    </style>
@stop

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="center-heading">
                    <h2>Glastron</h2>
                    <span class="center-line"></span>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <ul class="nav nav-pills" role="tablist" id="myTab">
                            <li role="presentation" class="active">
                                <a href="#bowriders" aria-controls="#bowriders" role="tab" data-toggle="tab">Bow Riders</a>
                            </li>
                            <li role="presentation">
                                <a href="#skifish" aria-controls="#skifish" role="tab" data-toggle="tab">Ski Fish</a>
                            </li>
                            <li role="presentation">
                                <a href="#deckboats" aria-controls="#deckboats" role="tab" data-toggle="tab">Deck Boats</a>
                            </li>
                            <li role="cruisers"><a href="#cruisers" aria-controls="side sport 2" role="tab" data-toggle="tab">Cruisers</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="bowriders">
                                <div class="row">

                                    @if(sizeof($bowriders) > 0)
                                        @foreach($bowriders as $i)
                                            <div class="col-md-4 text-center">
                                                <a href="/wheels/atv/{!! $i->id !!}">
                                                    <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                         class="img img-responsive thumbnail center-image">
                                                </a>
                                                {!! $i->product_name !!}<br>
                                                Starting at <strong>{!! $i->msrp !!}</strong>
                                            </div>

                                            @if(($loop->iteration % 3) == 0)
                                </div>
                                @if(! $loop->last)
                                    <div class="row">

                                        @endif
                                        @endif

                                        @if(($loop->last) && (($loop->iteration % 3) != 0))
                                    </div>
                                @endif

                                @endforeach
                                @else
                            </div>
                            @endif



                        </div>

                        <div role="tabpanel" class="tab-pane " id="skifish">
                            <div class="row">
                                @if(sizeof($skifish) >0)
                                    @foreach($skifish as $i)
                                        <div class="col-md-4 text-center">
                                            <a href="/wheels/atv/{!! $i->id !!}">
                                                <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                     class="img img-responsive thumbnail center-image">
                                            </a>
                                            {!! $i->product_name !!}<br>
                                            Starting at <strong>{!! $i->msrp !!}</strong>
                                        </div>

                                        @if(($loop->iteration % 3) == 0)
                            </div>
                            @if(! $loop->last)
                                <div class="row">
                                    @endif
                                    @endif

                                    @if(($loop->last) && (($loop->iteration % 3) != 0))
                                </div>
                            @endif


                            @endforeach
                            @else
                        </div>
                        @endif


                    </div>
                    <div role="tabpanel" class="tab-pane " id="deckboats">
                        <div class="row">

                            @if(sizeof($deckboats) > 0)
                                @foreach($deckboats as $i)
                                    <div class="col-md-4 text-center">
                                        <a href="/wheels/atv/{!! $i->id !!}">
                                            <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                                 class="img img-responsive thumbnail center-image">
                                        </a>
                                        {!! $i->product_name !!}<br>
                                        Starting at <strong>{!! $i->msrp !!}</strong>
                                    </div>

                                    @if(($loop->iteration % 3) == 0)
                        </div>
                        @if(! $loop->last)
                            <div class="row">
                                @endif
                                @endif

                                @if(($loop->last) && (($loop->iteration % 3) != 0))
                            </div>
                        @endif

                        @endforeach
                        @else
                            Nothing here
                    </div>
                    @endif


                </div>

                <div role="tabpanel" class="tab-pane " id="cruisers">
                    <div class="row">

                        @if(sizeof($cruisers) > 0)
                            @foreach($cruisers as $i)
                                <div class="col-md-4 text-center">
                                    <a href="/wheels/atv/{!! $i->id !!}">
                                        <img alt="" src="/storage/motorcycles/{!! $i->id !!}.jpg"
                                             class="img img-responsive thumbnail center-image">
                                    </a>
                                    {!! $i->product_name !!}<br>
                                    Starting at <strong>{!! $i->msrp !!}</strong>
                                </div>

                                @if(($loop->iteration % 3) == 0)
                    </div>
                    @if(! $loop->last)
                        <div class="row">
                            @endif
                            @endif
                            @if(($loop->last) && (($loop->iteration % 3) != 0))
                        </div>
                    @endif
                    @endforeach
                    @else
                        Nothing here
                </div>
                @endif


            </div>






    </div>
    </div>

    </div>
    </div>
    </div>
    </div>
    </div>

@stop

@section('bottom-js')
    <script>
        if (location.hash) {
            $('a[href=\'' + location.hash + '\']').tab('show');
        }
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('a[href="' + activeTab + '"]').tab('show');
        }

        $('body').on('click', 'a[data-toggle=\'tab\']', function (e) {
            e.preventDefault()
            var tab_name = this.getAttribute('href')
            if (history.pushState) {
                history.pushState(null, null, tab_name)
            }
            else {
                location.hash = tab_name
            }
            localStorage.setItem('activeTab', tab_name)

            $(this).tab('show');
            return false;
        });
        $(window).on('popstate', function () {
            var anchor = location.hash ||
                $('a[data-toggle=\'tab\']').first().attr('href');
            $('a[href=\'' + anchor + '\']').tab('show');
        });
    </script>
@stop