<div class="pull-right">
    <a class="btn btn-info" style="color: white" id="compare" data-toggle="modal" data-target="#compareModal" disabled="disabled"
       data-backdrop="false"
       href="#!">Compare checked</a>
</div>

<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>

<table class="table table-compact table-striped">
    <tbody>
    @foreach($inventory as $item)
        @if ($item->vehicle_type < 7)
            <tr class="visible-xs">
                <td colspan="3">
                    @if(isset($item->firstImage->image))
                        <a href="/vehicles/vehicle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $item->firstImage->image !!}"
                                 class="img img-responsive" style="max-width: 320px" alt="image">
                        </a>
                    @else
                        <a href="/vehicles/vehicle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/vendor/wheelspackage/hug-in-progress.jpg" class="img img-responsive"
                                 style="min-width: 300px;">
                        </a>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="hidden-xs">
                    @if(isset($item->firstImage->image))
                        <a href="/vehicles/vehicle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $item->firstImage->image !!}"
                                 class="img img-responsive" style="max-width: 320px" alt="image">
                        </a>
                    @else
                        <a href="/vehicles/vehicle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/vendor/wheelspackage/hug-in-progress.jpg" class="img img-responsive"
                                 style="min-width: 300px;">
                        </a>
                    @endif
                </td>
                <td>
                    <a href="/vehicles/vehicle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                        . " "
                                        . $item->make->make
                                        . " "
                                        . $item->vehicleModel->model) !!}">
                        <h3 class="text-danger">{!! $item->year !!} {!! $item->make->make !!} {!! $item->vehicleModel->model !!} {{ $item->trim ?? ''}}</h3>
                    </a>
                    {!! $item->description !!}
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li>Exterior Color: {!! $item->exterior_color !!}</li>
                                <li>Interior Color: {!! $item->interior_color !!}</li>
                                <li>Engine: {!! $item->engine !!}</li>
                                <li>Transmission: {!! $item->transmission !!}</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li>Stock #: {!! $item->stock_no !!}</li>
                                <li>Drivetrain: {!! $item->drive_train !!}</li>
                                <li>Body Style: {!! $item->body !!}</li>
                                <li>Kilometers: {!! $item->odometer !!}</li>
                            </ul>
                        </div>
                    </div>
                </td>
                <td class="hidden-xs">
                    <h3 class="text-danger text-center">${!! number_format($item->cost) !!}</h3>
                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="/vehicles/vehicle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                       . " "
                                       . $item->make->make
                                       . " "
                                       . $item->vehicleModel->model) !!}">Vehicle
                        Details</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openTempFinance('{{ $item->id }}', '{!! $item->stock_no !!}, {!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick
                        Click Inquiry</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick Quote</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="/get-pre-approved">0 Down Pre-approval</a>
                    <br>
                    <div class="text-center">
                        <input type="checkbox" name="compare[]" value="{!! $item->id !!}" class="compare"
                               data-toggle="tooltip" data-placement="top"
                               title="You can compare a maximum of 3 vehicles at a time"> Compare
                        <br>
                        <br>
                        <strong>Your Benefits when buying from
                            Wheels and Deals Include:</strong>
                        @if($item->hand_picked == 0)
                            <div data-hide="true" class="hidden">
                                <span class="rotator" style="color: #a94442; font-weight: bold;">Full Tank of Gas,WalkaWay Credit & Job Loss Protection,Paint Sealant Shine Guard Protection,No charge Nitrogen for life,Factory Warranty,Carfax History Report,Two Key Guarantee,Fresh MVI 200% above industry Standards,Fully Serviced and Guaranteed,All Fluids inspected and topped up,Refreshed Cabin air filter,Professional Detailed,Best in Class Service</span>
                            </div>
                        @else
                            <div data-hide="true" class="hidden">
                                <span class="rotator" style="color: #a94442; font-weight: bold;">Full tank of Gas,Powertrain Warranty,WalkaWay Credit & Job Loss Protection,No charge Nitrogen tire program  for life,Carfax History Report,New Updated MVI,Oil & Filter changed,Fluids Topped up,Nicely Cleaned</span>
                            </div>
                        @endif
                    </div>
                </td>
            </tr>
            <tr class="visible-xs">
                <td colspan="3">
                    <h3 class="text-danger text-center">${!! number_format($item->cost) !!}</h3>
                    <a class="btn btn-block btn-danger"
                       href="/vehicles/vehicle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                       . " "
                                       . $item->make->make
                                       . " "
                                       . $item->vehicleModel->model) !!}">Vehicle
                        Details</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openTempFinance('{{ $item->id }}', '{!! $item->stock_no !!}, {!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick
                        Click Inquiry</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick Quote</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="/get-pre-approved">0 Down Pre-approval</a>

                    <input type="checkbox" name="compare[]" value="{!! $item->id !!}" class="compare"
                           data-toggle="tooltip" data-placement="top"
                           title="You can compare a maximum of 3 vehicles at a time"> Compare
                    <br>
                    <br>
                    <strong>Your Benefits when buying from
                        Wheels and Deals Include:</strong>
                    @if($item->hand_picked == 0)
                        <div data-hide="true" class="hidden">
                            <span class="rotator" style="color: #a94442; font-weight: bold;">Two Key Guarantee,Free Protec Paint Sealant,Free Full Tank of Gas,Free Nitrogen Tire Program,Free CarFax History Report,Free Walkaway Credit Protection,Guaranteed MVI,Best in Class Serviced,Professional Detailed,Free Delivery,Singing Huggable Teddy Bear,Yearly Thank you gifts on your birthday</span>
                        </div>
                    @else
                        <div data-hide="true" class="hidden">
                            <span class="rotator" style="color: #a94442; font-weight: bold;">Free Full Tank of Gas,Free CarFax History Report,Free Nitrogen Tire Program,Free Walkaway Credit Protection,Fresh MVI,Properly Serviced,Professional Detailed,Free Delivery,Minimum 1 Year Warranty,Singing Huggable Teddy Bear,Yearly Thank You gifts on your birthday</span>
                        </div>
                    @endif
                </td>
            </tr>

        @elseif(($item->vehicle_type == 15) || ($item->vehicle_type = 9))

            <tr class="visible-xs">
                <td colspan="3">
                    @if(isset($item->firstImage->image))
                        <a href="/vehicles/boats/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $item->firstImage->image !!}"
                                 class="img img-responsive" style="max-width: 320px" alt="image">
                        </a>
                    @else
                        <a href="/vehicles/boats/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/vendor/wheelspackage/hug-in-progress.jpg" class="img img-responsive"
                                 style="min-width: 300px;">
                        </a>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="hidden-xs">
                    @if(isset($item->firstImage->image))
                        <a href="/vehicles/boats/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $item->firstImage->image !!}"
                                 class="img img-responsive" style="max-width: 320px" alt="image">
                        </a>
                    @else
                        <a href="/vehicles/boats/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/vendor/wheelspackage/hug-in-progress.jpg" class="img img-responsive"
                                 style="min-width: 300px;">
                        </a>
                    @endif
                </td>
                <td>
                    <a href="/vehicles/boats/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                        . " "
                                        . $item->make->make
                                        . " "
                                        . $item->vehicleModel->model) !!}">
                        <h3 class="text-danger">{!! $item->year !!} {!! $item->make->make !!} {!! $item->vehicleModel->model !!}</h3>
                    </a>
                    {!! $item->description !!}
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li>Exterior Color: {!! $item->exterior_color !!}</li>
                                <li>Interior Color: {!! $item->interior_color !!}</li>
                                <li>Engine: {!! $item->engine !!}</li>
                                <li>Transmission: {!! $item->transmission !!}</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li>Stock #: {!! $item->stock_no !!}</li>
                                <li>Drivetrain: {!! $item->drive_train !!}</li>
                                <li>Body Style: {!! $item->body !!}</li>
                                <li>Kilometers: {!! $item->odometer !!}</li>
                            </ul>
                        </div>
                    </div>
                </td>
                <td class="hidden-xs">
                    <h3 class="text-danger text-center">${!! number_format($item->cost) !!}</h3>
                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="/vehicles/boats/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                       . " "
                                       . $item->make->make
                                       . " "
                                       . $item->vehicleModel->model) !!}">Vehicle
                        Details</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openTempFinance('{{ $item->id }}', '{!! $item->stock_no !!}, {!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick
                        Click Inquiry</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick Quote</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="/get-pre-approved">0 Down Pre-approval</a>
                    <br>
                    <div class="text-center">
                        <input type="checkbox" name="compare[]" value="{!! $item->id !!}" class="compare"
                               data-toggle="tooltip" data-placement="top"
                               title="You can compare a maximum of 3 vehicles at a time"> Compare
                        @if($item->used == 0)
                            <br>
                            <br>
                            <strong>Your Benefits when buying from
                                Wheels and Deals Include:</strong>
                            <div data-hide="true" class="hidden">
                                <span class="rotator" style="color: #a94442; font-weight: bold;">VIP Discounts & Savings on Accessories,VIP Discounts & Savings on Parts & Labor,On the water Orientation,Full tank of Gas</span>
                            </div>
                        @endif
                    </div>
                </td>
            </tr>
            <tr class="visible-xs">
                <td colspan="3">
                    <h3 class="text-danger text-center">${!! number_format($item->cost) !!}</h3>
                    <a class="btn btn-block btn-danger"
                       href="/vehicles/boats/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                       . " "
                                       . $item->make->make
                                       . " "
                                       . $item->vehicleModel->model) !!}">Vehicle
                        Details</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openTempFinance('{{ $item->id }}', '{!! $item->stock_no !!}, {!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick
                        Click Inquiry</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick Quote</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="/get-pre-approved">0 Down Pre-approval</a>
                    <br>
                    <input type="checkbox" name="compare[]" value="{!! $item->id !!}" class="compare"
                           data-toggle="tooltip" data-placement="top"
                           title="You can compare a maximum of 3 vehicles at a time"> Compare
                    @if($item->used == 0)
                        <br>
                        <br>
                        <div data-hide="true" class="hidden">
                            <span class="rotator" style="color: #a94442; font-weight: bold;">Free Two-year Warranty Extension,Preferred discount rate on service,Preferred discount rate on accessories,Preferred discount rate on parts,Free Full Tank of Gas,Free Detailing Kit,Free Delivery,Free Boat Safety Kit,Free Anchor, Chain & Rope,Free Life Vests (4),Free Fire Extinguisher,Free Dock lines (2),Free Paddle,Free Air Horn,On the water Personalized Orientation,Singing Huggable Teddy Bear,Yearly Thank you gifts on your birthday</span>
                        </div>
                    @endif
                </td>
            </tr>
        @else
            <tr class="visible-xs">
                <td colspan="3">
                    @if(isset($item->firstImage->image))
                        <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $item->firstImage->image !!}"
                                 class="img img-responsive" style="max-width: 320px" alt="image">
                        </a>
                    @else
                        <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/vendor/wheelspackage/hug-in-progress.jpg" class="img img-responsive"
                                 style="min-width: 300px;">
                        </a>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="hidden-xs">
                    @if(isset($item->firstImage->image))
                        <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $item->firstImage->image !!}"
                                 class="img img-responsive" style="max-width: 320px" alt="image">
                        </a>
                    @else
                        <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                            <img src="/vendor/wheelspackage/hug-in-progress.jpg" class="img img-responsive"
                                 style="min-width: 300px;">
                        </a>
                    @endif
                </td>
                <td>
                    <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                        . " "
                                        . $item->make->make
                                        . " "
                                        . $item->vehicleModel->model) !!}">
                        <h3 class="text-danger">{!! $item->year !!} {!! $item->make->make !!} {!! $item->vehicleModel->model !!}</h3>
                    </a>
                    {!! $item->description !!}
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li>Exterior Color: {!! $item->exterior_color !!}</li>
                                <li>Interior Color: {!! $item->interior_color !!}</li>
                                <li>Engine: {!! $item->engine !!}</li>
                                <li>Transmission: {!! $item->transmission !!}</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li>Stock #: {!! $item->stock_no !!}</li>
                                <li>Drivetrain: {!! $item->drive_train !!}</li>
                                <li>Body Style: {!! $item->body !!}</li>
                                <li>Kilometers: {!! $item->odometer !!}</li>
                            </ul>
                        </div>
                    </div>
                </td>
                <td class="hidden-xs">
                    <h3 class="text-danger text-center">${!! number_format($item->cost) !!}</h3>
                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                       . " "
                                       . $item->make->make
                                       . " "
                                       . $item->vehicleModel->model) !!}">Vehicle
                        Details</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openTempFinance('{{ $item->id }}', '{!! $item->stock_no !!}, {!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick
                        Click Inquiry</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick Quote</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="/get-pre-approved">0 Down Pre-approval</a>
                    <br>
                    <div class="text-center">
                        <input type="checkbox" name="compare[]" value="{!! $item->id !!}" class="compare"
                               data-toggle="tooltip" data-placement="top"
                               title="You can compare a maximum of 3 vehicles at a time"> Compare
                        <br>
                        <br>
                        <div data-hide="true" class="hidden">
                            <span class="rotator" style="color: #a94442; font-weight: bold;">Two Key Guarantee,Free Protec Paint Sealant,Free Full Tank of Gas,Free Nitrogen Tire Program,Free CarFax History Report,Free Walkaway Credit Protection,Guaranteed MVI,Best in Class Serviced,Professional Detailed,Free Delivery,Singing Huggable Teddy Bear,Yearly Thank you gifts on your birthday</span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr class="visible-xs">
                <td colspan="3">
                    <h3 class="text-danger text-center">${!! number_format($item->cost) !!}</h3>
                    <a class="btn btn-block btn-danger"
                       href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                       . " "
                                       . $item->make->make
                                       . " "
                                       . $item->vehicleModel->model) !!}">Vehicle
                        Details</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openTempFinance('{{ $item->id }}', '{!! $item->stock_no !!}, {!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick
                        Click Inquiry</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick Quote</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick Quote</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="/get-pre-approved">0 Down Pre-approval</a>

                    <input type="checkbox" name="compare[]" value="{!! $item->id !!}" class="compare"
                           data-toggle="tooltip" data-placement="top"
                           title="You can compare a maximum of 3 vehicles at a time"> Compare
                    <br>
                    <br>
                    <div data-hide="true" class="hidden">
                        <span class="rotator" style="color: #a94442; font-weight: bold;">Two Key Guarantee,Free Protec Paint Sealant,Free Full Tank of Gas,Free Nitrogen Tire Program,Free CarFax History Report,Free Walkaway Credit Protection,Guaranteed MVI,Best in Class Serviced,Professional Detailed,Free Delivery,Singing Huggable Teddy Bear,Yearly Thank you gifts on your birthday</span>
                    </div>
                </td>
            </tr>
        @endif



    @endforeach
    </tbody>
</table>

{{ $inventory->links() }}