<script>
    $(document).ready(function () {
        $("#bookform").validate({
            errorClass: 'text-danger',
            validClass: 'text-success',
            errorElement: 'span',
            highlight: function (element, errorClass, validClass) {
                $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".text-danger").removeClass(errorClass).addClass(validClass);
            }
        });
    });

    $(".compare").click(function () {
        var numberOfChecked = $('.compare:checkbox:checked').length;
        if (numberOfChecked == 3) {
            $('.compare:not(:checked)').attr('disabled', 'disabled');
        } else {
            $('input.compare').removeAttr('disabled');
        }

        if (numberOfChecked > 1) {
            $('#compare').removeAttr('disabled');
        } else {
            $('#compare').attr('disabled', 'disabled');
        }
    });

    $("#compare").click(function () {
        var allValues = [];
        $('.compare:checked').each(function () {
            allValues.push($(this).val());
        });
        var a = allValues + "";
        $.ajax({
            url: '/vehicles/compare-vehicles',
            type: 'get',
            data: 'ids=' + a,
            success: function (returned) {
                $("#compareDiv").html(returned);
            }
        });
    });

    function openQQ(id, v) {
        $("#vehicle").html(v);
        $("#qq_id").val(id);
        $("#qq").modal({backdrop: false});
        $("#qqform").validate({
            errorClass: 'text-danger',
            validClass: 'text-success',
            errorElement: 'span',
            highlight: function (element, errorClass, validClass) {
                $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".text-danger").removeClass(errorClass).addClass(validClass);
            }
        });
    }

    function sendQQ() {
        var okay = false;
        okay = $("#qqform").validate().form();
        if (okay) {
            $("#qq").modal('hide');
            $.ajax({
                url: '/vehicles/quick-quote',
                type: 'get',
                data: 'id=' + $('#qq_id').val()
                    + '&email=' + $("#email").val()
                    + '&phone=' + $("#phone").val()
                    + '&name=' + $('#name').val(),
                success: function (json) {
                    bootbox.alert('Thanks. Someone will be in touch shortly');
                }
            });
        }
    }
</script>