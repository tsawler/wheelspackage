@if(($page->slider != null) && ($page->slider->slides->count() > 0))
    @if($page->slider->width == 'boxed')
        <div class="row">
            <div class="col-md-12">
                <div style="max-width: 960px; margin: 0 auto;">

                    @endif
                    <div class="rev_slider_wrapper @if($page->slider->height == 1000) fullscreen-container @elseif($page->slider->width == 'full')fullwidthbanner-container @endif"
                         @if($page->slider->height == 1000) style="margin-top: -115px" @endif>

                        <div id="rev_slider_1" class="rev_slider @if($page->slider->height == 1000) fullscreenbanner @elseif($page->slider->width == 'full') fullwidthbanner @endif"
                             data-version="5.4.6.4" style="visibility: hidden; min-height: @if($page->slider->height == 1000) 100vh; @else {{ $page->slider->height }}px; @endif">
                            <ul>

                                @if($page->slider != null)
                                    @foreach($page->slider->slides as $item)
                                        @if($item->active == 1)
                                        <li data-transition="{!! $page->slider->transition_type !!}" data-delay="{!! $item->duration !!}">
                                            @if($item->is_video == 0)

                                                @if(($agent->isMobile()) && (!$agent->isTablet()))
                                                    <img src="/sliders/{!! $page->id !!}/{!! $item->image !!}?w=960&dpr=1&q=30&bri={!! $item->brightness !!}&con={!! $item->contrast !!}&filt={!! $item->filter !!}" alt="" class="rev-slidebg">
                                                @elseif($agent->isTablet())
                                                    <img src="/sliders/{!! $page->id !!}/{!! $item->image !!}?w=1366&dpr=1&q=30&bri={!! $item->brightness !!}&con={!! $item->contrast !!}&filt={!! $item->filter !!}" alt="" class="rev-slidebg">
                                                @else
                                                    <img src="/sliders/{!! $page->id !!}/{!! $item->image !!}?w={!! Config::get('vcms5.min_slider_width') !!}&dpr=1&q=30&bri={!! $item->brightness !!}&con={!! $item->contrast !!}&filt={!! $item->filter !!}" class="rev-slidebg" alt="">
                                                @endif
                                            @else
                                                @if($item->background_video == 1)
                                                    <!-- required for background video, and will serve as the video's "poster/cover" image -->
                                                    <img src="/storage/videos/{!! $item->video->thumb !!}"
                                                    alt="Ocean"
                                                    class="rev-slidebg"
                                                    data-bgposition="center center"
                                                    data-bgfit="cover"
                                                    data-bgrepeat="no-repeat">

                                                    <!-- BACKGROUND VIDEO LAYER -->
                                                    <div class="rs-background-video-layer"
                                                    data-forcerewind="on"
                                                    data-volume="mute"
                                                    data-videowidth="100%"
                                                    data-videoheight="100%"
                                                    data-videomp4="/storage/videos/{!! $item->video->file_name !!}"
                                                    data-videopreload="auto"
                                                    data-videoloop="loop"
                                                    data-nextslideatend="true"
                                                    data-forceCover="1"
                                                    data-dottedoverlay="{!! $item->overlay !!}"
                                                    data-aspectratio="16:9"
                                                    data-autoplay="true"
                                                    data-autoplayonlyfirsttime="false"
                                                    ></div>
                                                @else

                                                    <!-- BEGIN HTML5 VIDEO LAYER -->
                                                    <div class="tp-caption tp-resizeme tp-videolayer"

                                                         data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"}, {"delay": "wait", "speed": 300, "to": "opacity: 0"}]'
                                                         data-type="video"
                                                         data-videomp4="/storage/videos/{!! $item->video->file_name !!}"
                                                         data-videowidth="1270"
                                                         data-videoheight="720"
                                                         data-autoplay="on"
                                                         data-videocontrols="controls"
                                                         data-nextslideatend="true"
                                                         data-forcerewind="on"
                                                         data-volume="20"
                                                         data-videoloop="loop"
                                                         data-allowfullscreenvideo="true"
                                                         data-videopreload="auto"

                                                         data-x="center"
                                                         data-y="center"
                                                         data-hoffset="0"
                                                         data-voffset="0"
                                                         data-basealign="slide">

                                                        <div>
                                                        <!-- END HTML5 VIDEO LAYER -->
                                                @endif

                                            @endif

                                            @if(($item->title != null) && ($item->title != ''))
                                                <div class="tp-caption tp-resizeme very_large_text"
                                                     data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"},
                                {"delay": "wait", "speed": 300, "to": "opacity: 0"}]'
                                                     data-x="center"
                                                     data-y="center"
                                                     data-hoffset="0"
                                                     data-voffset="-20"
                                                     data-width="['auto']"
                                                     data-height="['auto']"
                                                ><span @if($item->title_shadow == 1) class="shadow" @endif style="font-family: 'Lato', sans-serif; color: {!! $item->title_color !!}">{!! $item->title !!}</span></div>

                                                <div class="tp-caption tp-resizeme medium_text"
                                                     data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"},
                                {"delay": "wait", "speed": 300, "to": "opacity: 0"}]'
                                                     data-x="center"
                                                     data-y="center"
                                                     data-hoffset="0"
                                                     data-voffset="50"
                                                     data-width="['auto']"
                                                     data-height="['auto']"
                                                ><span @if($item->subtitle_shadow == 1) class="shadow" @endif style="font-family: 'Lato', sans-serif; color: {!! $item->subtitle_color !!}">{!! $item->subtitle !!}</span></div>
                                            @endif
                                            @if(($item->button_url != null) && ($item->button_url != ''))

                                                <div class="tp-caption tp-resizeme"
                                                     data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"},
                                {"delay": "wait", "speed": 300, "to": "opacity: 0"}]'
                                                     data-x="center"
                                                     data-y="center"
                                                     data-hoffset="0"
                                                     data-voffset="90"
                                                     data-width="['auto']"
                                                     data-height="['auto']"
                                                >
                                                    <p><br><a style="color: white;" class="btn btn-slider ls-btn" href="{!! $item->button_url !!}" @if($item->new_window == 1) target='_blank' @endif>{!! $item->button_text !!} </a></p>
                                                </div>
                                            @endif
                                        </li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>

                        </div>
                    </div>
                    @if($page->slider->width == 'boxed')
                </div>
            </div>
        </div>
    @endif
@endif
