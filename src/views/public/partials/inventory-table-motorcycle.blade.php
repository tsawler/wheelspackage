<div class="pull-right">
    <a class="btn btn-info" style="color: white" id="compare" data-toggle="modal" data-target="#compareModal" disabled="disabled"
       data-backdrop="false"
       href="#!">Compare checked</a>
</div>

<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>

<table class="table table-compact table-striped">
    <tbody>
    @foreach($inventory as $item)
        <tr class="visible-xs">
            <td colspan="3">
                @if(isset($item->firstImage->image))
                    <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                . " "
                                . $item->make->make
                                . " "
                                . $item->vehicleModel->model) !!}">
                        <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $item->firstImage->image !!}"
                             class="img img-responsive" style="max-width: 320px" alt="image">
                    </a>
                @else
                    <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                . " "
                                . $item->make->make
                                . " "
                                . $item->vehicleModel->model) !!}">
                        <img src="/vendor/wheelspackage/hug-in-progress.jpg" class="img img-responsive"
                             style="min-width: 300px;">
                    </a>
                @endif
            </td>
        </tr>
        <tr>
            <td class="hidden-xs">
                @if(isset($item->firstImage->image))
                    <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                . " "
                                . $item->make->make
                                . " "
                                . $item->vehicleModel->model) !!}">
                        <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $item->firstImage->image !!}"
                             class="img img-responsive" style="max-width: 320px" alt="image">
                    </a>
                @else
                    <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                . " "
                                . $item->make->make
                                . " "
                                . $item->vehicleModel->model) !!}">
                        <img src="/vendor/wheelspackage/hug-in-progress.jpg" class="img img-responsive"
                             style="min-width: 300px;">
                    </a>
                @endif
            </td>
            <td>
                <a href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                    . " "
                                    . $item->make->make
                                    . " "
                                    . $item->vehicleModel->model) !!}">
                    <h3 class="text-danger">{!! $item->year !!} {!! $item->make->make !!} {!! $item->vehicleModel->model !!}  {{ $item->trim ?? ''}}</h3>
                </a>
                {!! $item->description !!}
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-unstyled">
                            <li>Exterior Color: {!! $item->exterior_color !!}</li>
                            <li>Engine: {!! $item->engine !!}</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="list-unstyled">
                            <li>Stock #: {!! $item->stock_no !!}</li>
                            <li>Kilometers: {!! $item->odometer !!}</li>
                        </ul>
                    </div>
                </div>
            </td>
            <td class="hidden-xs">
                <h3 class="text-danger text-center">${!! number_format($item->cost) !!}</h3>
                <a class="btn btn-block btn-danger" style="color: white;"
                   href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model) !!}">Vehicle
                    Details</a>

                <a class="btn btn-block btn-danger" style="color: white;"
                   href="#!" onclick="openTempFinance('{{ $item->id }}', '{!! $item->stock_no !!}, {!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick
                    Click Inquiry</a>

                <a class="btn btn-block btn-danger" style="color: white;"
                   href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick Quote</a>

                <a class="btn btn-block btn-danger" style="color: white;"
                   href="/get-pre-approved">0 Down Pre-approval</a>
                <br>
                <div class="text-center">
                    <input type="checkbox" name="compare[]" value="{!! $item->id !!}" class="compare"
                           data-toggle="tooltip" data-placement="top"
                           title="You can compare a maximum of 3 vehicles at a time"> Compare

                    <br>
                    <br>
                    <strong>Your Benefits when buying from
                        Wheels and Deals Include:</strong>
                    @if($item->vehicle_makes_id == 47) <!-- mercury -->
                        <div data-hide="true" class="hidden">
                            <span class="rotator" style="color: #a94442; font-weight: bold;">VIP Discounts & Savings on Parts & Labor,First Service Included,Full Tank of Gas</span>
                        </div>
                    @else
                    <div data-hide="true" class="hidden"> <!-- other powerports -->
                        <span class="rotator" style="color: #a94442; font-weight: bold;">VIP Discounts & Savings on Accessories,VIP Discounts & Savings on Parts & Labor,No Charge 1st Scheduled Service "done by us",Full Tank of Gas</span>
                    </div>
                    @endif
                    </div>
            </td>
        </tr>
        <tr class="visible-xs">
            <td colspan="3">
                <h3 class="text-danger text-center">${!! number_format($item->cost) !!}</h3>
                <a class="btn btn-block btn-danger"
                   href="/vehicles/motorcycle/{{ $item->id }}/{!! \Illuminate\Support\Str::slug($item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model) !!}">Vehicle
                    Details</a>

                <a class="btn btn-block btn-danger"
                   href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick
                    Click Inquiry</a>

                <a class="btn btn-block btn-danger" style="color: white;"
                   href="/get-pre-approved">0 Down Pre-approval</a>

                <input type="checkbox" name="compare[]" value="{!! $item->id !!}" class="compare"
                       data-toggle="tooltip" data-placement="top"
                       title="You can compare a maximum of 3 vehicles at a time"> Compare

                <br>
                <br>
                <div data-hide="true" class="hidden">
                    <span class="rotator" style="color: #a94442; font-weight: bold;">Preferred discount rate on service, Preferred discount rate on accessories, Preferred discount rate on parts, Free Full Tank of Gas, Free Maintenance Kit, Free Detailing Kit, Free Delivery, Singing Huggable Teddy Bear, Yearly Thank you gifts on your birthday</span>
                </div>
            </td>
        </tr>

    @endforeach
    </tbody>
</table>

{{ $inventory->links() }}
