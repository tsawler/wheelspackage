<div class="modal fade" id="compareModal" tabindex="-1" role="dialog" style="z-index: 20000">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Compare Vehicles</h4>
            </div>
            <div class="modal-body" id="compareDiv">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="qq" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Quick Quote</h4>
            </div>

            <div class="modal-body" id="compareDiv">
                <p>Just fill out the form below and click <strong>Request Quick Quote</strong>, and we'll get back
                    to you with a quick quote.</p>
                {!! Form::open(array(
                    'url' => '/quick-quote',
                    'role' => 'form',
                    'name' => 'qqform',
                    'id' => 'qqform',
                    'method' => 'post',
                    ))
                    !!}

                <div class="form-group">
                    {!! Form::label('from', 'Your name:', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            @if(Auth::check())
                                {!! Form::text('name', Auth::user()->first_name . " " . Auth::user()->last_name, array('class' => 'required form-control', 'id' => 'name', 'placeholder' => 'you@example.ca')) !!}
                            @else
                                {!! Form::text('name', null, array('class' => 'required form-control', 'id' => 'name', 'placeholder' => 'Your name')) !!}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Your email address:', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            @if(Auth::check())
                                {!! Form::text('email', Auth::user()->email, array('class' => 'required email form-control', 'id' => 'email', 'placeholder' => 'you@example.ca')) !!}
                            @else
                                {!! Form::text('email', null, array('class' => 'required email form-control', 'id' => 'email', 'placeholder' => 'you@example.ca')) !!}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Your phone number:', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                            {!! Form::text('phone', null, array('class' => 'form-control', 'id' => 'phone', 'placeholder' => '')) !!}
                        </div>
                    </div>
                </div>

                <p>Vehicle: <span id="vehicle"></span></p>
                <input type="hidden" id="qq_id" name="id" value="0">

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="sendQQ()">Request Quick Quote</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->