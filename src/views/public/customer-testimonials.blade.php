
<div class="row">
    <div class="col-md-12">

        {{ $results->links() }}
        @foreach ($results as $item)
            <p class="embed-responsive embed-responsive-16by9">
                <iframe src="{!! $item->url !!}"></iframe>
            </p>
            <p>{!! $item->label !!}</p>
            <hr>
            <p>&nbsp;</p>
        @endforeach

        {{ $results->links() }}
    </div>
</div>
