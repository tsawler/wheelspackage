<div class="row">
    <div class="col-md-12">

    {!! Form::open(array(
            'url' => '/credit/apply',
            'role' => 'form',
            'name' => 'bookform',
            'id' => 'bookform',
            'method' => 'post',
            'class' => 'form-horizontal'
            ))
            !!}
        <!-- Form Group -->
        <div class="form-group">
            <!-- Label -->
            <label for="first_name" class="col-sm-5 control-label">First Name:</label>

            <div class="col-sm-7">
                <!-- Input -->
                @if(Auth::check())
                    {!! Form::text('first_name', Auth::user()->first_name, array('class' => 'required form-control',
                        'placeholder' => 'First Name',
                        'autofocus'=>'autofocus')) !!}
                @else
                    {!! Form::text('first_name', null, array('class' => 'required form-control',
                        'placeholder' => 'First Name',
                        'autofocus'=>'autofocus')) !!}
                @endif
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="last_name" class="col-sm-5 control-label">Last Name:</label>

            <div class="col-sm-7">
                <!-- Input -->
                @if(Auth::check())
                    {!! Form::text('last_name', Auth::user()->last_name, array('class' => 'required form-control',
                    'placeholder' => 'Last Name')) !!}
                @else
                    {!! Form::text('last_name', null, array('class' => 'required form-control',
                    'placeholder' => 'Last Name')) !!}
                @endif
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="dob" class="col-sm-5 control-label">Date of Birth:</label>

            <div class="col-sm-7">
                <!-- Input -->
                {!! Form::text('dob', null, array('class' => 'form-control required',
                'placeholder' => '')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="phone" class="col-sm-5 control-label">Best Contact Phone Number:</label>

            <div class="col-sm-7">
                <!-- Input -->
                {!! Form::text('phone', null, array('class' => 'required form-control',
                'placeholder' => '506 XXX XXXX')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="email" class="col-sm-5 control-label">Email:</label>

            <div class="col-sm-7">
                <!-- Input -->
                @if(Auth::check())
                    {!! Form::text('email', Auth::user()->email, array('class' => 'required email form-control', 'placeholder' => 'you@example.ca')) !!}
                @else
                    {!! Form::text('email', null, array('class' => 'required email form-control', 'placeholder' => 'you@example.ca')) !!}
                @endif
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="address" class="col-sm-5 control-label">Street Address:</label>

            <div class="col-sm-7">
                <!-- Input -->
                {!! Form::text('address', null, array('class' => 'form-control required',
                'placeholder' => '')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="city" class="col-sm-5 control-label">City/Town:</label>

            <div class="col-sm-7">
                <!-- Input -->
                {!! Form::text('city', null, array('class' => 'form-control required',
                'placeholder' => '')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="province" class="col-sm-5 control-label">State/Province:</label>

            <div class="col-sm-7">
                <!-- Input -->
                {!! Form::text('province', null, array('class' => 'form-control required',
                'placeholder' => '')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="zip" class="col-sm-5 control-label">Postal/Zip Code:</label>

            <div class="col-sm-7">
                <!-- Input -->
                {!! Form::text('zip', null, array('class' => 'form-control required',
                'placeholder' => '')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="rent" class="col-sm-5 control-label">Rent/Mortgage payment per month:</label>

            <div class="col-sm-7">
                <!-- Input -->
                {!! Form::text('rent', null, array('class' => 'form-control')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="employer" class="col-sm-5 control-label">Employer:</label>

            <div class="col-sm-7">
                <!-- Input -->
                {!! Form::text('employer', null, array('class' => 'form-control required',
                'placeholder' => '')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="income" class="col-sm-5 control-label">Income - Hourly wage &amp; average hours per week:</label>

            <div class="col-sm-7">
                <!-- Input -->
                {!! Form::text('income', null, array('class' => 'form-control')) !!}
            </div>
        </div>


        <div class="form-group">
            <!-- Label -->
            <label for="vehicle" class="col-sm-5 control-label">Interested In:</label>

            <div class="col-sm-7">
                <!-- Input -->
                <select name="vehicle" class="form-control">
                    <option value="Vehicle">Vehicle</option>
                    <option value="Power Sports">Power Sports</option>
                    <option value="Electric Bike">Electric Bike</option>
                </select>
            </div>
        </div>

        <div class="checkbox">
            <label>
                <input name="authorize" type="checkbox" value="1" class="required">
                I authorize Wheels and Deals to begin a No commitment credit pre-approval and contact lenders,
                financial
                institutions, or other third parties discreetly.
            </label>
        </div>

        <br>
        <div class="form-group">
            <label for="model" class="col-sm-5 control-label">Prove you're human:</label>
            <div class="col-sm-7">
                {!! NoCaptcha::display() !!}
            </div>
        </div>

        <hr>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-7">
                <!-- Button -->
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#bookform").validate({
            errorClass: 'text-danger',
            validClass: 'text-success',
            errorElement: 'span',
            highlight: function (element, errorClass, validClass) {
                $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".text-danger").removeClass(errorClass).addClass(validClass);
            }
        });
    });
</script>

{!! NoCaptcha::renderJs() !!}

