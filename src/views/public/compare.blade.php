<table class="table table-compact table-striped">


        <tbody>
        <tr>
            <td class="text-center" style="width: {{ $width }}%">
                <p><strong>{!! $vehicle_1->year !!} {!! $vehicle_1->make->make !!} {!! $vehicle_1->vehicleModel->model !!}</strong></p>
                @if(isset($vehicle_1->firstImage->image))
                    <a href="/vehicles/vehicle/{{ $vehicle_1->id }}/{!! \Illuminate\Support\Str::slug($vehicle_1->year
                    . " "
                    . $vehicle_1->make->make
                    . " "
                    . $vehicle_1->vehicleModel->model) !!}">
                        <img src="/storage/inventory/{!! $vehicle_1->id !!}/thumbs/{!! $vehicle_1->firstImage->image !!}"
                             class="img img-responsive center-block"  alt="image">
                    </a>
                @else
                    <a href="/vehicles/vehicle/{{ $vehicle_1->id }}/{!! \Illuminate\Support\Str::slug($vehicle_1->year
                    . " "
                    . $vehicle_1->make->make
                    . " "
                    . $vehicle_1->vehicleModel->model) !!}">
                        <img src="https://placehold.it/640x480?text=No+Image" class="img img-responsive">
                    </a>
                @endif
                <br>
                <a class="btn btn-primary" style="color: white;" href="/vehicles/vehicle/{!! $vehicle_1->id !!}/{!! \Illuminate\Support\Str::slug($vehicle_1->year
                    . " "
                    . $vehicle_1->make->make
                    . " "
                    . $vehicle_1->vehicleModel->model) !!}">Vehicle Details</a>
            </td>
            <td class="text-center" style="width: {{ $width }}%">
                <p><strong>{!! $vehicle_2->year !!} {!! $vehicle_2->make->make !!} {!! $vehicle_2->vehicleModel->model !!}</strong></p>

                @if(isset($vehicle_2->firstImage->image))
                    <a href="/vehicles/vehicle/{{ $vehicle_2->id }}/{!! \Illuminate\Support\Str::slug($vehicle_2->year
                    . " "
                    . $vehicle_2->make->make
                    . " "
                    . $vehicle_2->vehicleModel->model) !!}">
                        <img src="/storage/inventory/{!! $vehicle_2->id !!}/thumbs/{!! $vehicle_2->firstImage->image !!}"
                             class="img img-responsive center-block" alt="image">
                    </a>
                @else
                    <a href="/vehicles/vehicle/{{ $vehicle_1->id }}/{!! \Illuminate\Support\Str::slug($vehicle_2->year
                    . " "
                    . $vehicle_2->make->make
                    . " "
                    . $vehicle_2->vehicleModel->model) !!}">
                        <img src="https://placehold.it/640x480?text=No+Image" class="img img-responsive">
                    </a>
                @endif
                <br>
                <a class="btn btn-primary" style="color: white;" href="/vehicles/vehicle/{!! $vehicle_2->id !!}/{!! \Illuminate\Support\Str::slug($vehicle_2->year
                    . " "
                    . $vehicle_2->make->make
                    . " "
                    . $vehicle_2->vehicleModel->model) !!}">Vehicle Details</a>

            </td>
            @if($vehicle_count == 3)
                <td class="text-center" style="width: {{ $width }}%">
                    <p><strong>{!! $vehicle_3->year !!} {!! $vehicle_3->make->make !!} {!! $vehicle_3->vehicleModel->model !!}</strong></p>
                    @if(isset($vehicle_3->firstImage->image))
                        <a href="/vehicles/vehicle/{{ $vehicle_3->id }}/{!! \Illuminate\Support\Str::slug($vehicle_3->year
                    . " "
                    . $vehicle_2->make->make
                    . " "
                    . $vehicle_2->vehicleModel->model) !!}">
                            <img src="/storage/inventory/{!! $vehicle_3->id !!}/thumbs/{!! $vehicle_3->firstImage->image !!}"
                                 class="img img-responsive" alt="image">
                        </a>
                    @else
                        <a href="/vehicles/vehicle/{{ $vehicle_3->id }}/{!! \Illuminate\Support\Str::slug($vehicle_3->year
                    . " "
                    . $vehicle_3->make->make
                    . " "
                    . $vehicle_3->vehicleModel->model) !!}">
                            <img src="https://placehold.it/640x480?text=No+Image" class="img img-responsive">
                        </a>
                    @endif
                    <br>
                    <a class="btn btn-primary" style="color: white;" href="/vehicles/vehicle/{!! $vehicle_3->id !!}/{!! \Illuminate\Support\Str::slug($vehicle_3->year
                    . " "
                    . $vehicle_3->make->make
                    . " "
                    . $vehicle_3->vehicleModel->model) !!}">Vehicle Details</a>
                </td>
            @endif
        </tr>


        <tr>
            <td class="text-center">
                <strong>Price:</strong> ${!! number_format($vehicle_1->cost) !!}
            </td>
            <td class="text-center">
                <strong>Price:</strong> ${!! number_format($vehicle_2->cost) !!}
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>Price:</strong> ${!! number_format($vehicle_3->cost) !!}
                </td>
            @endif
        </tr>


        <tr>
            <td class="text-center">
                <strong>MSRP:</strong> ${!! number_format($vehicle_1->total_msr) !!}
            </td>
            <td class="text-center">
                <strong>MSRP:</strong> ${!! number_format($vehicle_2->total_msr) !!}
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>MSRP:</strong> ${!! number_format($vehicle_3->total_msr) !!}
                </td>
            @endif
        </tr>

        <tr>
            <td class="text-center">
                <strong>Odometer:</strong> {!! $vehicle_1->odometer !!}
            </td>
            <td class="text-center">
                <strong>Odometer:</strong> {!! $vehicle_2->odometer !!}
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>Odometer:</strong> {!! $vehicle_3->odometer !!}
                </td>
            @endif
        </tr>

        <tr>
            <td class="text-center">
                <strong>Drivetrain:</strong> {!! $vehicle_1->drive_train !!}
            </td>
            <td class="text-center">
                <strong>Drivetrain:</strong> {!! $vehicle_2->drive_train !!}
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>Drivetrain:</strong> {!! $vehicle_3->drive_train !!}
                </td>
            @endif
        </tr>

        <tr>
            <td class="text-center">
                <strong>Exterior Colour:</strong> {!! $vehicle_1->exterior_color !!}
            </td>
            <td class="text-center">
                <strong>Exterior Colour:</strong> {!! $vehicle_2->exterior_color !!}
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>Exterior Colour:</strong> {!! $vehicle_3->exterior_color !!}
                </td>
            @endif
        </tr>

        <tr>
            <td class="text-center">
                <strong>Interior Colour:</strong> {!! $vehicle_1->interior_color !!}
            </td>
            <td class="text-center">
                <strong>Interior Colour:</strong> {!! $vehicle_2->interior_color !!}
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>Interior Colour:</strong> {!! $vehicle_3->interior_color !!}
                </td>
            @endif
        </tr>

        <tr>
            <td class="text-center">
                <strong>Engine:</strong> {!! $vehicle_1->engine !!}
            </td>
            <td class="text-center">
                <strong>Engine:</strong> {!! $vehicle_2->engine !!}
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>Engine:</strong> {!! $vehicle_3->engine !!}
                </td>
            @endif
        </tr>

        <tr>
            <td class="text-center">
                <strong>Transmission:</strong> {!! $vehicle_1->transmission !!}
            </td>
            <td class="text-center">
                <strong>Transmission:</strong> {!! $vehicle_2->transmission !!}
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>Transmission:</strong> {!! $vehicle_3->transmission !!}
                </td>
            @endif
        </tr>

        <tr>
            <td class="text-center">
                <strong>VIN:</strong> {!! $vehicle_1->vin !!}
            </td>
            <td class="text-center">
                <strong>VIN:</strong> {!! $vehicle_2->vin !!}
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>VIN:</strong> {!! $vehicle_3->vin !!}
                </td>
            @endif
        </tr>

        <tr>
            <td class="text-center">
                <strong>Options:</strong>
                <ul class="list-unstyled">
                    @foreach ($vehicle_1->options()->get() as $option)
                        <li><small>{!! $option->option->option_name !!}</small></li>
                    @endforeach
                </ul>
            </td>
            <td class="text-center">
                <strong>Options:</strong>
                <ul class="list-unstyled">
                    @foreach ($vehicle_2->options()->get() as $option)
                        <li><small>{!! $option->option->option_name !!}</small></li>
                    @endforeach
                </ul>
            </td>
            @if($vehicle_count == 3)
                <td class="text-center">
                    <strong>Options:</strong>
                    <ul class="list-unstyled">
                        @foreach ($vehicle_3->options()->get() as $option)
                            <li><small>{!! $option->option->option_name !!}</small></li>
                        @endforeach
                    </ul>
                </td>
            @endif
        </tr>

        </tbody>


</table>


