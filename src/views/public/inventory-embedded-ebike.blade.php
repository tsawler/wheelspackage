<div class="row">
    <div class="col-md-12">

        {!! Form::open(array(
                'url' => \Illuminate\Support\Facades\Request::path(),
                'role' => 'form',
                'name' => 'bookform',
                'id' => 'bookform',
                'method' => 'get',
                'class' => 'form-inline pull-left'
                ))
                !!}

        <input type="hidden" name="year" value="0">
{{--        <div class="form-group">--}}
{{--            <label for="make" class="sr-only">Year</label>--}}
{{--            {!! Form::select('year', $years, $year, ['class' => 'form-control']) !!}--}}
{{--        </div>--}}

        <div class="form-group">
            <label for="make" class="sr-only">Make</label>
            {!! Form::select('make', $makes, $make, ['class' => 'form-control', 'id' => 'make']) !!}
        </div>

        <div class="form-group">
            <label for="model" class="sr-only">Model</label>
            {!! Form::select('model', $models, $model, ['class' => 'form-control', 'id' => 'model']) !!}
        </div>

        <div class="form-group">
            <label for="price" class="sr-only">Prices</label>
            {!! Form::select('price', [
            '0' => 'All Prices',
            '1' => 'Low to High',

            '2' => 'High to Low',
            ], $price, ['class' => 'form-control']) !!}
        </div>

        <button type="submit" class="btn btn-default">Go</button>

        {!! Form::close() !!}

        @include('wheelspackage::public.partials.inventory-table-ebike')
    </div>
</div>


<script>
    $('#make').change(function () {
        $.ajax({
            url: '/inventory/modelsJsonMotorcycles',
            type: 'get',
            data: 'id=' + $('#make').val(),
            dataType: 'json',
            success: function (json) {
                $('#model').empty()
                $('#model').append($('<option>').text('All models').attr('value', 0));
                $.each(json, function (i, option) {
                    $('#model').append($('<option>').text(option.model).attr('value', option.id));
                });
            }
        });
    });
</script>
@include('wheelspackage::public.partials.modals')
@include('wheelspackage::public.partials.vehicle-js')




