@extends('public.base-inside')

@section('content')
    <div class="center-heading">
        <h2>Thanks for looking us over, we appreciate it and we will never disappoint you!</h2>
        <span class="center-line"></span>
    </div>

    <p>
        “The purpose of a business is to create and keep a customer” and in today’s competitive, disruptive economy,
        Canada’s Huggable Car Dealer serves as a beacon of this belief. Our most huggable goal is to always bring a
        smile to people’s faces and keep earning the right to be Canada’s most referred and preferred used vehicle
        dealer.
    </p>
    <p>
        We want to earn your business, share a Hug and as you know Wheels and Deals vehicles are the best prepared &
        served in the business. Our in house certification process is 200% above industry standards. Every vehicle is
        CarProofed and comes with Free Walkaway Job Loss Protection.
    </p>
    <p class="text-center">
        <img src="/images/Welcome-to-the-Hug-Club.gif" alt="Welcome to the Hug Club" class="img img-responsive center-block">
    </p>
    <p>
        Our Vehicles are Nicer & We Hug Our Customers Better. Free Protect Paint Sealant Shine, Full Tank of gas & much,
        much more. Save thousands buying slightly used, Pay Less, Owe Less, Shop HUGGABLE Today. Hugs Jim
    </p>

@stop

@section('bg-class') breadcrumb-1 !!} @stop