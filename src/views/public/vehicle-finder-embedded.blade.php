<div class="row">
    <div class="col-md-12">

    {!! Form::open(array(
        'url' => '/wheels/vehicle-finder',
        'role' => 'form',
        'name' => 'bookform',
        'id' => 'bookform',
        'method' => 'post',
        'class' => 'form-horizontal'
        ))
        !!}
    <!-- Form Group -->
        <div class="form-group">
            <!-- Label -->
            <label for="first_name" class="col-sm-3 control-label">First Name:</label>

            <div class="col-sm-9">
                @if(Auth::check())
                    {!! Form::text('first_name', Auth::user()->first_name, array('class' => 'required form-control',
                        'placeholder' => 'First Name',
                        'autofocus'=>'autofocus')) !!}
                @else
                    {!! Form::text('first_name', null, array('class' => 'required form-control',
                        'placeholder' => 'First Name',
                        'autofocus'=>'autofocus')) !!}
                @endif
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="last_name" class="col-sm-3 control-label">Last Name:</label>

            <div class="col-sm-9">
                @if(Auth::check())
                    {!! Form::text('last_name', Auth::user()->last_name, array('class' => 'required form-control',
                    'placeholder' => 'Last Name')) !!}
                @else
                    {!! Form::text('last_name', null, array('class' => 'required form-control',
                    'placeholder' => 'Last Name')) !!}
                @endif
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="email" class="col-sm-3 control-label">Email:</label>

            <div class="col-sm-9">
                @if(Auth::check())
                    {!! Form::text('email', Auth::user()->email, array('class' => 'required email form-control', 'placeholder' => 'you@example.ca')) !!}
                @else
                    {!! Form::text('email', null, array('class' => 'required email form-control', 'placeholder' => 'you@example.ca')) !!}
                @endif
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="phone" class="col-sm-3 control-label">Phone Number:</label>

            <div class="col-sm-9">
                <!-- Input -->
                {!! Form::text('phone', null, array('class' => 'form-control',
                'placeholder' => '506 XXX XXXX')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="contact_method" class="col-sm-3 control-label">Contact Method:</label>

            <div class="col-sm-9">
                <!-- Input -->
                {!! Form::select('contact_method', $methods, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="year" class="col-sm-3 control-label">Year:</label>

            <div class="col-sm-9">
                <!-- Input -->
                {!! Form::text('year', null, array('class' => 'form-control',
                'placeholder' => 'Vehicle year')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="phone" class="col-sm-3 control-label">Make:</label>

            <div class="col-sm-9">
                <!-- Input -->
                {!! Form::text('make', null, array('class' => 'form-control',
                'placeholder' => 'Vehicle model')) !!}
            </div>
        </div>

        <div class="form-group">
            <!-- Label -->
            <label for="model" class="col-sm-3 control-label">Model:</label>

            <div class="col-sm-9">
                <!-- Input -->
                {!! Form::text('model', null, array('class' => 'form-control',
                'placeholder' => 'Enter the model you are interested in')) !!}
            </div>
        </div>


        <div class="form-group">
            <label for="model" class="col-sm-3 control-label">Prove you're human:</label>
            <div class="col-sm-9">
                {!! NoCaptcha::display() !!}
            </div>
        </div>


        <hr>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <!-- Button -->
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#bookform").validate({
            errorClass: 'text-danger',
            validClass: 'text-success',
            errorElement: 'span',
            highlight: function (element, errorClass, validClass) {
                $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".text-danger").removeClass(errorClass).addClass(validClass);
            }
        });
    });
</script>
{!! NoCaptcha::renderJs() !!}
