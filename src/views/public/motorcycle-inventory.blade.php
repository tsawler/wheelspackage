@extends('public.base-inside')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.1/alt/video-js-cdn.min.css" integrity="sha256-27NDYItxSL+SZ0kOP3DbTusgf5k4mYxPC5LQrcERO+w=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.pannellum.org/2.4/pannellum.css"/>
    <style>
        #vehicle-carousel .carousel-indicators {
            bottom: 0;
            left: 10px;
            margin-left: 5px;
            width: 100%;
        }

        #vehicle-carousel .carousel-indicators li {
            border: medium none;
            border-radius: 0;
            float: left;
            height: 10px;
            margin-bottom: 5px;
            margin-left: 0;
            margin-right: 5px !important;
            margin-top: 100px;
            width: 100px;
        }

        #vehicle-carousel .carousel-indicators img {
            border: 2px solid #FFFFFF;
            float: left;
            height: auto;
            left: 0;
            width: 100px;
            margin-top: 30px;
        }

        #vehicle-carousel .carousel-indicators .active img {
            border: 2px solid #39b3d7;
        }

        #options ul {
            width: 100%;
            margin-bottom: 20px;
            overflow: hidden;
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            font-family: 'Raleway', sans-serif;
            color: #424242;
            font-size: 14px;
            line-height: 24px;
        }

        #options li {
            float: left;
            display: inline;
        }

        #double li {
            width: 50%;
        }

        #triple li {
            width: 33.333%;
        }

        #quad li {
            width: 25%;
        }

        .mutebtn {
            position: absolute;
            bottom: 50px;
            right: 50px;
            max-width: 76px;
            z-index: 10000;
            opacity: 0.5;
        }

        #vehicle-panorama {
            width: 426px;
            height: 200px;
        }
    </style>
@stop

@section('below-content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>
    </div>
    <div class="center-heading">
        <h2>{!! $item->year ?? '' !!} {!! $item->make->make ?? '' !!} {!! $item->vehicleModel->model ?? ''!!} <span
                    class="text-danger">${!! number_format($item->cost) !!}<sup>*</sup></span></h2>
        @if($item->total_msr != null)
        @if($item->cost < $item->total_msr)
            <em>MSRP ${!! number_format($item->total_msr)!!}</em><br>
        @endif
        @endif
        <span class="center-line"></span>
    </div>

    @if($video_id > 0)
        @php
            $video = \App\Video::find($video_id);
        @endphp
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <img id="mute-btn" src="/images/off.png" class="mutebtn">
                        <video id="vehicle-video" class="video-js vjs-default-skin vjs-big-play-centered" controls loop preload="auto" width="100%"
                               poster="/storage/videos/{!! $video->thumb !!}" data-setup='{ "fluid": true, "controls": true, "autoplay": true, "muted": true }' style="">
                            <source src="/storage/videos/{!! $video->id !!}-{!! $video->file_name !!}" type='video/mp4'>
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a web browser that
                                <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                            </p>
                        </video>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if((sizeof($item->images) > 0) || ($video_id > 0))
        <div class="container text-center" style="margin-bottom: 100px;">
            <div class="carousel slide article-slide" id="vehicle-carousel">
                <div class="carousel-inner cont-slider">

                    @foreach($item->images as $image)
                        @if($loop->iteration == 1)
                            <div class="item active">
                                <img src="/storage/inventory/{!! $item->id !!}/{!! $image->image !!}">
                            </div>
                        @else
                            <div class="item">
                                <img src="/storage/inventory/{!! $item->id !!}/{!! $image->image !!}">
                            </div>
                        @endif
                    @endforeach

                </div>

                <!-- Indicators -->
                <ol class="carousel-indicators visible-lg visible-md">
                    @foreach($item->images as $image)
                        @if($loop->iteration == 1)
                            <li class="active" data-slide-to="{!! $loop->iteration - 1 !!}"
                                data-target="#vehicle-carousel">
                                <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $image->image !!}">
                            </li>
                        @else
                            <li class="" data-slide-to="{!! $loop->iteration - 1 !!}" data-target="#vehicle-carousel">
                                <img src="/storage/inventory/{!! $item->id !!}/thumbs/{!! $image->image !!}">
                            </li>
                        @endif
                    @endforeach

                </ol>
            </div>
        </div>
    @endif

    @if($item->panorama != null)
        <div class="container">
            <div class="col-md-12 text-center">
                <div id="vehicle-panorama" class="center-block"></div>
                <p>&nbsp;</p>
            </div>

        </div>
    @endif


    <div class="container">
        <div class="row">
            <div class="col-md-6 well">
                <ul class="list-unstyled">
                    <li><strong>Stock #:</strong> {!! $item->stock_no !!}</li>
                    <li><strong>Trim:</strong> {!! $item->trim !!}</li>
                    <li><strong>Kilometers:</strong> {!! $item->odometer !!}</li>
                    <li><strong>Exterior Color:</strong> {!! $item->exterior_color !!}</li>
                    <li><strong>Engine:</strong> {!! $item->engine !!}</li>
                    <li><strong>VIN:</strong> {!! $item->vin !!}</li>
                </ul>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <p>
                    <a class="btn btn-block btn-danger" href="/get-pre-approved"><i class="fa fa-usd"></i> Fast
                        Financing</a>
                    <a class="btn btn-block btn-danger" href="#!" id="request-test-drive-button"><i
                                class="fa fa-car"></i> Request a Test Drive</a>
                    <a class="btn btn-block btn-danger" id="email-friend-button"><i class="fa fa-envelope"></i> Email a
                        Friend</a>
                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openTempFinance('{{ $item->id }}', '{!! $item->stock_no !!}, {!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick
                        Click Inquiry</a>

                    <a class="btn btn-block btn-danger" style="color: white;"
                       href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                                   . " "
                                   . $item->make->make
                                   . " "
                                   . $item->vehicleModel->model !!}')">Quick Quote</a>
                    <a href="/wheels/vehicle-window-sticker?id={!! $item->id !!}" class="btn btn-block btn-danger"><i class="fa fa-print"></i> Print Vehicle</a>
                </p>
                <p class="text-center">
                    <a href="http://www.autofinance.cibc.com/#/pre-approval/redirect?dealerid=C05722" target="_blank">
                        <img src="/vendor/wheelspackage/cibc.png" alt="">
                    </a>
                </p>
                <p class="text-center">
                    <strong>DEALER INFORMATION:</strong><br>
                    Jim Gilbert's Wheels and Deals<br>
                    402 St Mary's Street<br>
                    Fredericton, New Brunswick<br>
                    Phone: (506) 459-6832
                </p>
            </div>

        </div>

        @if($sales)
            <div class="row">
                <div class="col-md-12">
                    <hr>
                </div>
            </div>
            <div class="row">
                @foreach($sales as $salesperson)
                    <div class="col-md-2 text-center">
                        <a href="/sales/{!! $salesperson->slug !!}">
                            <img src="/salesstaff/{!! $salesperson->image !!}" alt="image" class="img img-responsive">
                        </a>
                    </div>
                @endforeach
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <hr>
                <p>
                    * All advertised prices exclude government fees and taxes, any finance charges and any dealer
                    document preparation charges. While every reasonable effort is made to ensure the accuracy of this
                    data, we are not responsible for any errors or omissions contained on these pages. Please verify any
                    information in question with a dealership sales representative.
                </p>
                <hr>
                <p>
                    ** We accept all trade ins and recommend your purchase of credit protection and extended vehicle
                    protection, however "as always the choice is yours". Jim Gilbert's Wheels and Deals is Canada's Best
                    six acre Car Shopping Experience and we would love to share our Huggable knowledge with you. You
                    will receive many extras, the warm Huggable feeling and the good attention that has made Canada's
                    Huggable Car Dealer Famous. Shop us First, Feel the Difference... then compare.

                </p>
            </div>
        </div>
    </div>


    <div class="modal fade" tabindex="-1" id="email-friend" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Send to a Friend</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(array(
                        'url' => '',
                        'role' => 'form',
                        'name' => 'friendform',
                        'id' => 'friendform',
                        'method' => 'post',
                        ))
                        !!}
                    <div class="form-group">
                        {!! Form::label('from', 'Your name:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                @if(Auth::check())
                                    {!! Form::text('name', Auth::user()->first_name . " " . Auth::user()->last_name, array('class' => 'required form-control', 'id' => 'name-friend', 'placeholder' => 'you@example.ca')) !!}
                                @else
                                    {!! Form::text('name', null, array('class' => 'required form-control', 'id' => 'name-friend', 'placeholder' => 'you@example.ca')) !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('from', 'Your email address:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                @if(Auth::check())
                                    {!! Form::text('from', Auth::user()->email, array('class' => 'required email form-control', 'id' => 'from-friend', 'placeholder' => 'you@example.ca')) !!}
                                @else
                                    {!! Form::text('from', null, array('class' => 'required email form-control', 'id' => 'from-friend', 'placeholder' => 'you@example.ca')) !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('to', 'Your friend\'s email address:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                {!! Form::text('to', null, array('class' => 'required form-control email', 'id' => 'to-friend',
                                                                    'placeholder' => '')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="send-to-a-friend">Send message</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" id="test-drive-modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Request a Test Drive</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(array(
                        'url' => '',
                        'role' => 'form',
                        'name' => 'testdrive',
                        'id' => 'testdrive',
                        'method' => 'post',
                        ))
                        !!}
                    <div class="form-group">
                        {!! Form::label('from', 'Your name:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                @if(Auth::check())
                                    {!! Form::text('users_name', Auth::user()->first_name . " " . Auth::user()->last_name, array('class' => 'required form-control', 'id' => 'name-test-drive', 'placeholder' => '')) !!}
                                @else
                                    {!! Form::text('users_name', null, array('class' => 'required form-control', 'id' => 'name-test-drive', 'placeholder' => '')) !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', 'Phone number:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {!! Form::text('phone', null, array('class' => 'required form-control', 'id' => 'phone-test-drive',
                                                                    'placeholder' => '')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'Your email address:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                @if(Auth::check())
                                    {!! Form::text('email', Auth::user()->email, array('class' => 'required email form-control', 'id' => 'email-test-drive', 'placeholder' => 'you@example.ca')) !!}
                                @else
                                    {!! Form::text('e,ao;', null, array('class' => 'required email form-control', 'id' => 'email-test-drive', 'placeholder' => 'you@example.ca')) !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('preferred_date', 'Preferred Date:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {!! Form::text('datetime', null, array('class' => 'required form-control datepicker', 'id' => 'preferred_date-test-drive',
                                                                    'placeholder' => '')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('preferred_time', 'Preferred Time:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                {!! Form::text('preferred_time', null, array('class' => 'required form-control timepicker', 'id' => 'preferred_time-test-drive',
                                                                    'placeholder' => 'e.g. 5:30 PM')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="request-test-drive">Request Test Drive</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="qq" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Quick Quote</h4>
                </div>

                <div class="modal-body" id="compareDiv">
                    <p>This virtual thing is different but I understand...</p>
                    {!! Form::open(array(
                        'url' => '/quick-quote',
                        'role' => 'form',
                        'name' => 'qqform',
                        'id' => 'qqform',
                        'method' => 'post',
                        ))
                        !!}

                    <div class="form-group">
                        {!! Form::label('from', 'My name is:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                @if(Auth::check())
                                    {!! Form::text('name', Auth::user()->first_name . " " . Auth::user()->last_name, array('class' => 'required form-control', 'id' => 'name', 'placeholder' => 'you@example.ca')) !!}
                                @else
                                    {!! Form::text('name', null, array('class' => 'required form-control', 'id' => 'name', 'placeholder' => 'you@example.ca')) !!}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'My email is:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                @if(Auth::check())
                                    {!! Form::text('email', Auth::user()->email, array('class' => 'required email form-control', 'id' => 'email', 'placeholder' => 'you@example.ca')) !!}
                                @else
                                    {!! Form::text('email', null, array('class' => 'required email form-control', 'id' => 'email', 'placeholder' => 'you@example.ca')) !!}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'My number is:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                {!! Form::text('phone', null, array('class' => 'form-control', 'id' => 'phone', 'placeholder' => '')) !!}
                            </div>
                        </div>
                    </div>

                    <p>Vehicle: <span id="vehicle"></span></p>
                    <input type="hidden" id="qq_id" name="id" value="0">

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="sendQQ()">Click here
                    </button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('bg-class') breadcrumb-1 !!} @stop

@section('bottom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.1/alt/video.novtt.min.js" integrity="sha256-oDs+Rw/te07oGYsyrIBNb9Vzerg/OYk9qVcrgGkNK9E=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.pannellum.org/2.4/pannellum.js"></script>
    <script>
        @if(sizeof($item->images) > 0)
        $('#vehicle-carousel').carousel({
            interval: 3000
        });
        @endif

        $("#request-test-drive-button").click(function () {
            $('#test-drive-modal').modal({backdrop: false});
        });

        $("#email-friend-button").click(function () {
            $('#email-friend').modal({backdrop: false});
        });

        $("#request-test-drive").click(function (e) {
            var okay = false;
            okay = $("#testdrive").validate().form();
            if (okay) {
                $.ajax({
                    url: '/ajax/request-test-drive',
                    type: 'post',
                    data: {
                        email: $("#email-test-drive").val(),
                        preferred_date: $("#preferred_date-test-drive").val(),
                        preferred_time: $("#preferred_time-test-drive").val(),
                        _token: '{!! csrf_token() !!}',
                        id: {!! $vehicle_id !!},
                        users_name: $("#name-test-drive").val(),
                        phone: $("#phone-test-drive").val()
                    },
                    success: function () {
                        bootbox.alert('Your request for a test drive has been sent! Someone will be in touch shortly.');
                    },
                    error: function () {
                        bootbox.alert('error!');
                    }
                });
                $('#test-drive-modal').modal('hide')
            }
        });

        $("#send-to-a-friend").click(function (e1) {
            var okay = false;
            okay = $("#friendform").validate().form();
            if (okay) {
                $.ajax({
                    url: '/ajax/send-to-a-friend',
                    type: 'post',
                    data: {
                        from: $("#from-friend").val(),
                        to: $("#to-friend").val(),
                        _token: '{!! csrf_token() !!}',
                        id: {!! $vehicle_id !!},
                        name: $("#name-friend").val()
                    },
                    success: function () {
                        bootbox.alert('Message sent to ' + $("#to-friend").val() + "!");
                    },
                    error: function () {
                        bootbox.alert('error!');
                    }
                });
                $('#email-friend').modal('hide')
            }
        });

        $(document).ready(function () {
            $("#friendform").validate({
                errorClass: 'text-danger',
                validClass: 'text-success',
                errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".text-danger").removeClass(errorClass).addClass(validClass);
                }
            });
            $('.timepicker').timepicker({'forceRoundTime': true});
            $(".datepicker").datepicker({format: 'yyyy-mm-dd', autoclose: true});

            carouselNormalization();
        });

        function openQQ(id, v) {
            $("#vehicle").html(v);
            $("#qq_id").val(id);
            $("#qq").modal({backdrop: false});
        }

        function sendQQ() {
            var okay = false;
            okay = $("#qqform").validate().form();
            if (okay) {
                $.ajax({
                    url: '/vehicles/quick-quote',
                    type: 'get',
                    data: 'id=' + $('#qq_id').val()
                    + '&email=' + $("#email").val()
                    + '&phone=' + $("#phone").val()
                    + '&name=' + $('#name').val(),
                    success: function (json) {
                        bootbox.alert('Thanks. Someone will be in touch shortly');
                    }
                });
            }
        }


        function carouselNormalization() {
            var items = $('#vehicle-carousel .item'), //grab all slides
                heights = [], //create empty array to store height values
                tallest; //create variable to make note of the tallest slide

            if (items.length) {
                function normalizeHeights() {
                    items.each(function() { //add heights to array
                        heights.push($(this).height());
                    });
                    tallest = Math.max.apply(null, heights); //cache largest value
                    items.each(function() {
                        $(this).css('min-height', tallest + 'px');
                    });
                };
                normalizeHeights();

                $(window).on('resize orientationchange', function() {
                    tallest = 0, heights.length = 0; //reset vars
                    items.each(function() {
                        $(this).css('min-height', '0'); //reset min-height
                    });
                    normalizeHeights(); //run it again
                });
            }
        }

                @if($video_id > 0)
        var myPlayer = videojs('#vehicle-video');
        $(".mutebtn").click(function(){
            var isVolumeMuted = myPlayer.muted();
            if (isVolumeMuted) {
                myPlayer.muted(false);
                $("#mute-btn").attr('src', '/images/on.png');
            } else {
                myPlayer.muted(true);
                $("#mute-btn").attr('src', '/images/off.png');
            }
        });
        @endif

        @if($item->panorama != null)

        pannellum.viewer('vehicle-panorama', {
            "type": "equirectangular",
            "panorama": "/storage/panoramas/{!! $item->panorama->panorama !!}",
            "autoLoad": true
        });

        @endif

    </script>
@stop
