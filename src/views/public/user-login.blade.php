@extends('public.base-inside')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.min.css" integrity="sha256-rFMLRbqAytD9ic/37Rnzr2Ycy/RlpxE5QH52h7VoIZo=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.css" integrity="sha256-M1GR1abSip/Of//bvHItt7pqBl2FlFuMPpZFS8/r7Eo=" crossorigin="anonymous" />
    <style>
        .checkbox label, .bootstrap-switch {
            padding-left: 0 !important;
        }
    </style>
@stop
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6 text-center">
            <div class="center-heading">
                <h2>Social Login</h2>
                <span class="center-line"></span>
            </div>

            <a class="btn btn-block btn-social btn-facebook" href="/wheels/facebook/api-login">
                <span class="fa fa-facebook"></span> Sign in with Facebook
            </a>

            <br>

            <a class="btn btn-block btn-social btn-google" href="/wheels/google/api-login">
                <span class="fa fa-google"></span> Sign in with Google+
            </a>

            <br>

            <a class="btn btn-block btn-social btn-github" href="/wheels/github/api-login">
                <span class="fa fa-github"></span> Sign in with Github
            </a>
        </div>
        <div class="col-md-6">
            <div class="center-heading">
                <h2>Standard Login</h2>
                <span class="center-line"></span>
            </div>
            <!-- Form -->
        {!! Form::open(array(
        'url' => '/user/login',
        'role' => 'form',
        'name' => 'bookform',
        'id' => 'bookform',
        'method' => 'post',
        'class' => 'form-horizontal'
        ))
        !!}

        <!-- Form Group -->
            <div class="form-group">
                <!-- Label -->
                <label for="user" class="col-sm-3 control-label">Email</label>

                <div class="col-sm-6">
                    <!-- Input -->
                    {!! Form::email('email', null, array('class' => 'required email form-control',
                    'placeholder' => 'you@example.com',
                    'autofocus'=>'autofocus')) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Password</label>

                <div class="col-sm-6">
                    {!! Form::password('password', array('class' => 'form-control required',
                    'placeholder' => 'Password')) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="test" id="test"> Remember me
                        </label>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <!-- Button -->
                    <button type="submit" class="btn btn-primary">Login</button>
                    &nbsp;
                    <button type="submit" class="btn btn-default">Reset</button>
                </div>
            </div>
            <div class="col-sm-offset-3 col-sm-9">
                <a href="/password/reset" class="black">Forgot Password?</a><br>
                Don't have an account? <a href="/wheels/user/create" class="black">Create one.</a>
            </div>
            {!! Form::close() !!}
            <br/>
        </div>
    </div>

</div>
@stop


@section('bottom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js" integrity="sha256-AKUJYz2DyEoZYHh2/+zPHm1tTdYb4cmG8HC2ydmTzM4=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            $("#test").bootstrapSwitch({ size: 'small'});
            $("#bookform").validate({
                errorClass:'text-danger',
                validClass:'text-success',
                errorElement:'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".text-danger").removeClass(errorClass).addClass(validClass);
                }
            });
        });
    </script>
@stop