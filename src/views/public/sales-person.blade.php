@extends('public.base-inside')

@section('css')

@stop

@section('content')
    <div class="row" style="background: black; border-bottom: 10px solid red;">
        <div class="col-md-12 text-center">
                <h1 style="color: white; text-transform: uppercase;">{!! $staff->salesperson_name !!}</h1>
        </div>

    </div>

    <div class="container">


        <div class="row">
            <div class="col-md-12 text-center">
                <img src="/vendor/wheelspackage/images/here-to-help.png" alt="here to help">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 hidden-xs hidden-sm">
                <img src="/vendor/wheelspackage/images/left-image.png" alt="vehicles">
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">

                <img src="/salesstaff/{!! $staff->image !!}" alt="image" usemap="#image-map">
                <map name="image-map" id="image-map">
                    <area target="" alt="" title="Text Me" href="sms://+{!! $staff->phone !!}" coords="49,176,447,234" shape="rect">
                    <area target="" alt="" title="Call Me" href="tel://+{!! $staff->phone !!}" coords="50,311,442,367" shape="rect">
                </map>
                <br>

            </div>
            <div class="col-md-4 hidden-xs hidden-sm">
                <img src="/vendor/wheelspackage/images/right-image.png" alt="vehicles">
            </div>
        </div>


    </div>
    </div>
    </div>
@stop

@section('bottom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/image-map-resizer/1.0.9/js/imageMapResizer.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#image-map').imageMapResize();
        });
    </script>
@stop