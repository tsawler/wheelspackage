@extends('public.base-inside')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.1/alt/video-js-cdn.min.css"
          integrity="sha256-27NDYItxSL+SZ0kOP3DbTusgf5k4mYxPC5LQrcERO+w=" crossorigin="anonymous"/>

    <style>
        .center-image {
            margin-left: auto !important;
            margin-right: auto !important;
        }

        .mutebtn {
            position: absolute;
            bottom: 185px;
            right: 25px;
            max-width: 55px;
            z-index: 10000;
            opacity: 0.5;
        }
    </style>
@stop

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="center-heading">
                    <h2>{!! $item->product_name !!}</h2>
                    <span class="center-line"></span>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(sizeof($images) > 0)
                    <div class="container text-center" style="margin-bottom: 100px;">
                        <div class="carousel slide article-slide" id="vehicle-carousel">
                            <div class="carousel-inner cont-slider">

                                @foreach($images as $image)
                                    @if($loop->iteration == 1)
                                        <div class="item active">
                                            <img class="center-image" alt=""
                                                 src="/storage/motorcycles/{!! $item->id !!}/{!! $image !!}.jpg">

                                        </div>
                                    @else
                                        <div class="item">
                                            <img class="center-image" alt=""
                                                 src="/storage/motorcycles/{!! $item->id !!}/{!! $image !!}.jpg">
                                        </div>
                                    @endif
                                @endforeach

                            </div>

                            <!-- Indicators -->
                            <ol class="carousel-indicators visible-lg visible-md">
                                @foreach($images as $image)
                                    @if($loop->iteration == 1)
                                        <li class="active" data-slide-to="{!! $loop->iteration - 1 !!}"
                                            data-target="#vehicle-carousel">
                                            <img alt="" src="/storage/motorcycles/{!! $item->id !!}/{!! $image !!}.jpg">
                                        </li>
                                    @else
                                        <li class="" data-slide-to="{!! $loop->iteration - 1 !!}"
                                            data-target="#vehicle-carousel">
                                            <img alt="" src="/storage/motorcycles/{!! $item->id !!}/{!! $image !!}.jpg">
                                        </li>
                                    @endif
                                @endforeach

                            </ol>
                        </div>
                    </div>
                @endif

                <hr>
            </div>
            <div class="row">
                <div class="col-md-8">

                    <h1>{!! $item->product_name !!} - {!! $item->year !!}</h1>
                    <p><strong>MSRP: {!! $item->msrp !!}</strong></p>

                    <p>{!! $item->description !!}</p>
                </div>


                <div class="col-md-4">
                    @if($item->video_id > 0)
                        @php
                            $video = \App\Video::find($item->video_id);
                        @endphp
                        <div>
                            <img id="mute-btn" src="/images/off.png" class="mutebtn">
                            <video id="vehicle-video" loop class="video-js vjs-default-skin vjs-big-play-centered" controls
                                   preload="auto" width="100%"
                                   poster="/storage/videos/{!! $video->thumb !!}"
                                   data-setup='{ "fluid": true, "controls": true, "autoplay": true, "muted": true }'
                                   style="">
                                <source src="/storage/videos/{!! $video->id !!}-{!! $video->file_name !!}"
                                        type='video/mp4'>
                                <p class="vjs-no-js">
                                    To view this video please enable JavaScript, and consider upgrading to a web browser
                                    that
                                    <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5
                                        video</a>
                                </p>
                            </video>
                        </div>
                        <p>&nbsp;</p>
                    @endif

                    <a class="btn btn-block btn-danger" href="/get-pre-approved"><i class="fa fa-usd"></i> Fast
                        Financing</a>
                    <a class="btn btn-block btn-danger"
                       href="#!" onclick="openQQ('{{ $item->id }}', '{!! $item->year
                       . " "
                       . $item->product_name !!}')">Quick Click Inquiry</a>
                    <a href="{!! $item->url !!}" target="_blank" class="btn btn-block btn-primary">Visit Manufacturer's
                        Page</a>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="qq" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Quick Quote</h4>
                </div>

                <div class="modal-body" id="compareDiv">
                    <p>This virtual thing is different but I understand...</p>
                    {!! Form::open(array(
                        'url' => '/quick-quote',
                        'role' => 'form',
                        'name' => 'qqform',
                        'id' => 'qqform',
                        'method' => 'post',
                        ))
                        !!}

                    <div class="form-group">
                        {!! Form::label('from', 'Your name:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                @if(Auth::check())
                                    {!! Form::text('name', Auth::user()->first_name . " " . Auth::user()->last_name, array('class' => 'required form-control', 'id' => 'name', 'placeholder' => 'you@example.ca')) !!}
                                @else
                                    {!! Form::text('name', null, array('class' => 'required form-control', 'id' => 'name', 'placeholder' => 'you@example.ca')) !!}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Your email address:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                @if(Auth::check())
                                    {!! Form::text('email', Auth::user()->email, array('class' => 'required email form-control', 'id' => 'email', 'placeholder' => 'you@example.ca')) !!}
                                @else
                                    {!! Form::text('email', null, array('class' => 'required email form-control', 'id' => 'email', 'placeholder' => 'you@example.ca')) !!}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'Your phone number:', array('class' => 'control-label')) !!}
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                {!! Form::text('phone', null, array('class' => 'form-control', 'id' => 'phone', 'placeholder' => '')) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'I  would like to talk about:', array('class' => 'control-label')) !!}
                        <div class="controls">

                            <textarea id="talkabout" class="form-control"></textarea>

                        </div>
                    </div>

                    <p>Vehicle: <span id="vehicle"></span></p>
                    <input type="hidden" id="qq_id" name="id" value="0">

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="sendQQ()">Request Quick
                        Quote
                    </button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop

@section('bottom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.1/alt/video.novtt.min.js"
            integrity="sha256-oDs+Rw/te07oGYsyrIBNb9Vzerg/OYk9qVcrgGkNK9E=" crossorigin="anonymous"></script>

    <script>
        function openQQ(id, v) {
            $("#vehicle").html(v);
            $("#qq_id").val(id);
            $("#qq").modal({backdrop: false});
        }

        function sendQQ() {
            var okay = false;
            okay = $("#qqform").validate().form();
            if (okay) {
                $.ajax({
                    url: '/vehicles/quick-quote',
                    type: 'get',
                    data: 'id=' + $('#qq_id').val()
                        + '&email=' + $("#email").val()
                        + '&phone=' + $("#phone").val()
                        + '&name=' + $('#name').val(),
                    success: function (json) {
                        bootbox.alert('Thanks. Someone will be in touch shortly');
                    }
                });
            }
        }

                @if($item->video_id > 0)
        var myPlayer = videojs('#vehicle-video');
        $(".mutebtn").click(function () {
            var isVolumeMuted = myPlayer.muted();
            if (isVolumeMuted) {
                myPlayer.muted(false);
                $("#mute-btn").attr('src', '/images/on.png');
            } else {
                myPlayer.muted(true);
                $("#mute-btn").attr('src', '/images/off.png');
            }
        });
        @endif
    </script>
@stop