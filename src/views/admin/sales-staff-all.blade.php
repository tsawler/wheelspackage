@extends('base')

@section('top-white')
    <h1>Sales Staff</h1>
@stop

@section('content-title')

@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Sales Staff</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <table id="itable" class="table table-compact table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($staff as $item)
                        <tr>
                            <td><a href="/admin/sales/salesperson?id={!! $item->id !!}">{!! $item->salesperson_name !!}</a></td>
                            @if($item->active == 1)
                                <td><span class="text-success">Active</span></td>
                            @else
                                <td><span class="text-danger">Inactive</span></td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        $(function() {
            $('#itable').DataTable({
                processing: true,
                stateSave: true,
                responsive: true,

            });
        });
    </script>
@stop