@extends('base')

@section('top-white')
    <h1>Test Drive Requests</h1>
@stop

@section('content-title')

@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Test Drive Requests</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <table id="itable" class="table table-compact table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        $(function() {
            $('#itable').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                responsive: true,
                ajax: '{!! route('datatables-admin-testdrives.allTestDrivesJson') !!}',
                columns: [
                    { "data": "users_name",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/admin/test-drives/test-drive?id=" + oData.id + "'>" + oData.users_name + "</a>");
                        }
                    },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    {
                        data: 'processed',
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if (oData.processed == 1) {
                                $(nTd).html("<span class='text-success'>Processed</span>");
                            } else {
                                $(nTd).html("<span class='text-danger'>Unprocessed</span>");
                            }
                        }
                    }
                ]
            });
        });
    </script>
@stop