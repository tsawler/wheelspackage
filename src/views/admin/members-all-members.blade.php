@extends('base')

<?php
$active = ['<span class="text-danger">Inactive</span>', '<span class="text-success">Active</span>'];
?>

@section('top-white')
    <h1>All Members</h1>
@stop

@section('content-title')
    Members
@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Members</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <table id="users-table" class="table table-compact table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Account Type</th>
                        <th>Created</th>
                        <th>Updated</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                responsive: true,
                ajax: '{!! route('datatables-admin-all-members.allMembersJson') !!}',
                columns: [
                    { "data": "first_name",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/admin/members/member?id="+oData.id+"'>"+oData.last_name+", " +oData.first_name+"</a>");
                        }
                    },
                    { "data": "login_types_id",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if (oData.login_types_id == 1) {
                                $(nTd).html("Standard");
                            } else if (oData.login_types_id == 2) {
                                $(nTd).html("Facebook");
                            } else if (oData.login_types_id == 3) {
                                $(nTd).html("Twitter");
                            } else if (oData.login_types_id == 4) {
                                $(nTd).html("Google");
                            } else if (oData.login_types_id == 2) {
                                $(nTd).html("Github");
                            }
                        }
                    },
                    { data: 'created_at', name: 'users.created_at' },
                    { data: 'updated_at', name: 'users.updated_at' }
                ]
            });
        });
    </script>
@stop