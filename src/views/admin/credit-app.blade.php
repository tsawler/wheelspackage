@extends('base')

@section('top-white')
    <h1>Credit Application</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Credit Application
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                <table class="table table-compact table-striped">
                    <tbody>
                    <tr>
                        <td>Name:</td>
                        <td>{!! $app->first_name !!} {!! $app->last_name !!}</td>
                    </tr>
                    <tr>
                        <td>
                            Email:
                        </td>
                        <td>
                            {!! $app->email !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Phone:
                        </td>
                        <td>
                            {!! $app->phone !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address:
                        </td>
                        <td>
                            {!! $app->address !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            City/Town:
                        </td>
                        <td>
                            {!! $app->city !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Province/State:
                        </td>
                        <td>
                            {!! $app->province !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Postal Code/Zip:
                        </td>
                        <td>
                            {!! $app->zip !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Interested In:
                        </td>
                        <td>
                            {!! $app->vehicle !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date/Time:
                        </td>
                        <td>
                            {!! $app->created_at !!}
                        </td>
                    </tr>
                    </tbody>
                </table>

                <hr>

                <a class="btn btn-primary" href="/admin/credit/process?id={!! $app->id !!}">Mark as Processed</a>
                <a class="btn btn-danger" href="#!" onclick="confirmDelete({!! $app->id !!})">Delete</a>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>

        function confirmDelete(x) {
            bootbox.confirm("Are you sure you want to delete this item?", function (result) {
                if (result == true) {
                    window.location.href = '/admin/credit/delete?id=' + x;
                }
            });
        }

    </script>
@stop