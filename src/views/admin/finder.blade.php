@extends('base')

@section('top-white')
    <h1>Vehicle Finder Request</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Vehicle Finder Request
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                <table class="table table-compact table-striped">
                    <tbody>
                    <tr>
                        <td>Name:</td>
                        <td>{!! $finder->first_name !!} {!! $finder->last_name !!}</td>
                    </tr>
                    <tr>
                        <td>
                            Email:
                        </td>
                        <td>
                            {!! $finder->email !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Phone:
                        </td>
                        <td>
                            {!! $finder->phone !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Preferred Contact:
                        </td>
                        <td>
                            {!! $finder->contact_method !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Year:
                        </td>
                        <td>
                            {!! $finder->year !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Make:
                        </td>
                        <td>
                            {!! $finder->make !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Model:
                        </td>
                        <td>
                            {!! $finder->model !!}
                        </td>
                    </tr>
                    </tbody>
                </table>

                <hr>

                <a class="btn btn-primary" href="/admin/finder/process?id={!! $finder->id !!}">Mark as Processed</a>
                <a class="btn btn-danger" href="#!" onclick="confirmDelete({!! $finder->id !!})">Delete</a>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>

        function confirmDelete(x) {
            bootbox.confirm("Are you sure you want to delete this item?", function (result) {
                if (result == true) {
                    window.location.href = '/admin/finder/delete?id=' + x;
                }
            });
        }

    </script>
@stop