@extends('base')

@section('top-white')
    <h1>Test Drive Request</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Test Drive Request
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                <table class="table table-compact table-striped">
                    <tbody>
                    <tr>
                        <td>Name:</td>
                        <td>{!! $app->users_name !!}</td>
                    </tr>
                    <tr>
                        <td>
                            Email:
                        </td>
                        <td>
                            {!! $app->email !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Phone:
                        </td>
                        <td>
                            {!! $app->phone !!}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Preferred Date/Time:
                        </td>
                        <td>
                            {!! $app->preferred_date !!} - {!! $app->preferred_time !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Vehicle:
                        </td>
                        <td>
                            <a href="/admin/inventory/vehicle?id={!! $car->id !!}">
                                Stock # {!! $car->stock_no !!}
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <hr>

                <a class="btn btn-primary" href="/admin/test-drives/process?id={!! $app->id !!}">Mark as Processed</a>
                <a class="btn btn-danger" href="#!" onclick="confirmDelete({!! $app->id !!})">Delete</a>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>

        function confirmDelete(x) {
            bootbox.confirm("Are you sure you want to delete this item?", function (result) {
                if (result == true) {
                    window.location.href = '/admin/test-drives/delete?id=' + x;
                }
            });
        }

    </script>
@stop