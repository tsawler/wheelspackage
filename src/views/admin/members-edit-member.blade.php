@extends('base')

@section('top-white')
    <div class="col-sm-6">
        <h1>Edit Member</h1>
    </div>
@stop

@section('content-title')

@stop

@section('content')


    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Edit Member
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                {!! Form::model($member, array(
                'url' => '/admin/members/member',
                'role' => 'form',
                'name' => 'bookform',
                'id' => 'bookform',
                'method' => 'post',
                'class' => 'form-horizontal'
                ))
                !!}
                <fieldset>
                    <div class="form-group">
                        <!-- Label -->
                        <label for="first_name" class="col-sm-3 control-label">First Name</label>

                        <div class="col-sm-6">
                            <!-- Input -->
                            {!! Form::text('first_name', null, array('class' => 'required form-control',
                            'placeholder' => 'Your first name')) !!}
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <!-- Label -->
                        <label for="last_name" class="col-sm-3 control-label">Last Name</label>

                        <div class="col-sm-6">
                            <!-- Input -->
                            {!! Form::text('last_name', null, array('class' => 'required form-control',
                            'placeholder' => 'Your last name')) !!}
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <!-- Label -->
                        <label for="email" class="col-sm-3 control-label">Email Address</label>

                        <div class="col-sm-6">
                            <!-- Input -->
                            {!! Form::text('email', null, array('class' => 'required form-control',
                            'placeholder' => 'you@example.com')) !!}
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        {!! Form::label('user_active', 'User active?', array('class' => 'col-sm-3 control-label')) !!}
                        <div class="controls">
                            <div class="col-sm-6">
                                {!! Form::select('user_active', array(
                                        '1' => 'Yes',
                                        '0' => 'No'),
                                        null,
                                        array('class' => 'form-control',
                                            'style' => 'max-width: 400px;')) !!}
                            </div>
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        {!! Form::label('access_level', 'Access Level', array('class' => 'col-sm-3 control-label')) !!}
                        <div class="controls">
                            <div class="col-sm-6">
                                {!! Form::select('access_level', array(
                                        '1' => 'Site Visitor',
                                        '3' => 'Admin'),
                                        null,
                                        array('class' => 'form-control',
                                            'style' => 'max-width: 400px;')) !!}
                            </div>
                        </div>
                    </div>



                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <!-- Button -->
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                        <a onclick="confirmDelete({!! $member->id !!}); return false"  href="#!" class="btn btn-danger">Delete this member</a>
                    </div>
                </div>
                {!! Form::hidden('id', null) !!}
                {!! Form::close() !!}


            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>


        $(document).ready(function () {
            $("#bookform").validate({
                errorClass: 'text-danger',
                validClass: 'text-success',
                errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".text-danger").removeClass(errorClass).addClass(validClass);
                }
            });
        });

        function confirmDelete(x){
            bootbox.confirm("Are you sure you want to delete this member and all of his/her information?", function(result) {
                if (result==true)
                {
                    window.location.href = '/admin/members/delete-member?id='+x;
                }
            });
        }
    </script>
@stop