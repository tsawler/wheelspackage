@extends('base')

@section('top-white')
    <h1>Huggable Word of Mouth</h1>
@stop

@section('content-title')

@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Huggable Word of Mouth</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <table id="itable" class="table table-compact table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        $(function() {
            $('#itable').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                responsive: true,
                ajax: '{!! route('datatables-admin-word.allWordJson') !!}',
                columns: [
                    { "data": "title",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/admin/word/edit?id=" + oData.id + "'>" + oData.title + "</a>");
                        }
                    },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    {
                        data: 'active',
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if (oData.active == 1) {
                                $(nTd).html("<span class='text-success'>Active</span>");
                            } else {
                                $(nTd).html("<span class='text-danger'>Inactive</span>");
                            }
                        }
                    }
                ]
            });
        });
    </script>
@stop