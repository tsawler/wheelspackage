@extends('base')

@section('top-white')
    <h1>Customer Testimonial</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Customer Testimonial
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                {!! Form::model($testimonial,array(
                    'url' => '/admin/testimonials/testimonial',
                    'role' => 'form',
                    'name' => 'bookform',
                    'id' => 'bookform',
                    'method' => 'post',
                    ))
                !!}

                <div class="form-group">
                    {!! Form::label('url', 'Youtube Link', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-link"></i></span>
                            {!! Form::text('url', null, array('class' => 'required url form-control ',
                                                                'style' => 'max-width: 400px;',
                                                                'placeholder' => '')) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('label', 'Label', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                            {!! Form::text('label', null, array('class' => 'required form-control ',
                                                                'style' => 'max-width: 400px;',
                                                                'placeholder' => '')) !!}
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::label('active', 'Testimonial active?', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-question"></i></span>
                            {!! Form::select('active', array(
                                    '1' => 'Yes',
                                    '0' => 'No'),
                                    null,
                                    array('class' => 'form-control',
                                        'style' => 'max-width: 400px;')) !!}
                        </div>
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <div class="controls">
                        {!! Form::submit('Save', array('class' => 'btn btn-primary submit')) !!}
                        @if ($testimonial_id > 0)
                            <a class="btn btn-danger" href="#!" onclick='confirmDelete({!! $testimonial_id !!})'>Delete this
                                testimonial</a>
                        @endif
                        <a class="btn btn-info" href="/admin/testimonials/all-testimonials">Cancel</a>
                    </div>
                </div>

                <div>&nbsp;</div>

                {!! Form::hidden('id', $testimonial_id) !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        @if ($testimonial_id > 0)
        function confirmDelete(x) {
            bootbox.confirm("Are you sure you want to delete this testimonial?", function (result) {
                if (result == true) {
                    window.location.href = '/admin/testimonials/delete?id=' + x;
                }
            });
        }
        @endif
        $(document).ready(function () {
            $("#bookform").validate({
                errorClass: 'has-error',
                validClass: 'has-success',
                errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
                }
            });
        });
    </script>
@stop