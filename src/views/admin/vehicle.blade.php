@extends('base')

@section('css')
    <link rel="stylesheet" href="https://cdn.pannellum.org/2.4/pannellum.css"/>
    <style>
        #vehicle-panorama {
            width: 426px;
            height: 200px;
        }
    </style>
@stop

@section('top-white')
    <h1>Vehicle</h1>
@stop


@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Vehicle
                    - {!! $item->year ?? '' !!} {!! $item->make->make ?? '' !!} {!! $item->vehicleModel->model ?? 'New Vehicle'!!}
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                {!! Form::model($item,array(
                    'url' => '/admin/inventory/vehicle',
                    'role' => 'form',
                    'name' => 'bookform',
                    'id' => 'bookform',
                    'method' => 'post',
                    'files' => 'true',
                    ))
                !!}

                <div role="tabpanel" id="myTabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#basic" aria-controls="basic" role="tab" data-toggle="tab" data-id="basic"
                               class="tab">Basic</a>
                        </li>
                        <li role="presentation">
                            <a href="#detailed" aria-controls="detailed" role="tab" data-toggle="tab" data-id="detailed"
                               class="tab">Detailed</a>
                        </li>
                        <li role="presentation">
                            <a href="#images" aria-controls="images" role="tab" data-toggle="tab" data-id="images"
                               class="tab">Images</a>
                        </li>
                        <li role="presentation">
                            <a href="#sticker" aria-controls="sticker" role="tab" data-toggle="tab" data-id="images"
                               class="tab">Window Sticker</a>
                        </li>
                        <li role="presentation">
                            <a href="#video" aria-controls="video" role="tab" data-toggle="tab" data-id="video"
                               class="tab">Video/Panorama</a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="basic">
                            <br>
                            <div class="form-group">
                                {!! Form::label('year', 'Year', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-question"></i></span>
                                        {!! Form::selectRange('year', date("Y") + 1, 1900,
                                                null,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px;')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('vehicle_type', 'Vehicle Type', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                        {!! Form::select('vehicle_type', [
                                            8 => 'ATV - Brute Force',
                                            11 => 'ATV - Mules',
                                            12 => 'ATV - Teryx',
                                            1 => 'Car',
                                            16 => 'Electric Bikes',
                                            13 => 'Jetski',
                                            10 => 'Mercury',
                                            7 => 'Motorcycle',
                                            3 => 'Other',
                                            9 => 'Pontoon Boat',
                                            15 => 'Power Boat',
                                            17 => 'Scooter',
                                            5 => 'SUV',
                                            14 => 'Trailer',
                                            2 => 'Truck',
                                            4 => 'Unknown',
                                            6 => 'Van',
                                            ],
                                                null,
                                                array('class' => 'form-control', 'id' => 'vehicle_type',
                                                    'style' => 'max-width: 400px;')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('vehicle_makes_id', 'Make', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-question"></i></span>
                                        {!! Form::select('vehicle_makes_id', $makes,
                                                null,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px;',
                                                    'id' => 'make')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('vehicle_models_id', 'Model', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-question"></i></span>
                                        {!! Form::select('vehicle_models_id', $models,
                                                null,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px;',
                                                    'id' => 'model')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('used', 'New or Used?', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-question"></i></span>
                                        {!! Form::select('used', ['0' => 'New', '1' => 'Used'],
                                                null,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px;',
                                                    'id' => 'hand_picked')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('hand_picked', 'Hand-picked Local Vehicle?', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-question"></i></span>
                                        {!! Form::select('hand_picked', ['0' => 'No', '1' => 'Yes'],
                                                null,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px;',
                                                    'id' => 'hand_picked')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('stock_no', 'Stock #', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                        {!! Form::text('stock_no', null, array('class' => 'required form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('stock_no', 'VIN', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                        {!! Form::text('vin', null, array('class' => 'required form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('odometer', 'Odometer', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-tachometer"></i></span>
                                        {!! Form::text('odometer', null, array('class' => 'required form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('exterior_color', 'Exterior Color', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                        {!! Form::text('exterior_color', null, array('class' => 'form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('interior_color', 'Interior Color', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                        {!! Form::text('interior_color', null, array('class' => 'form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('trim', 'Trim', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                        {!! Form::text('trim', null, array('class' => 'form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('body', 'Body', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                        {!! Form::text('body', null, array('class' => ' form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('engine', 'Engine', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-gear"></i></span>
                                        {!! Form::text('engine', null, array('class' => ' form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('transmission', 'Transmission', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-gear"></i></span>
                                        {!! Form::text('transmission', null, array('class' => ' form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('drive_train', 'Drivetrain', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-gear"></i></span>
                                        {!! Form::text('drive_train', null, array('class' => ' form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('cost', 'Price', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                        {!! Form::text('cost', null, array('class' => 'required form-control digits',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('total_msr', 'MSRP', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                        {!! Form::text('total_msr', null, array('class' => 'digits form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('total_msr', 'Price For Display', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                        {!! Form::text('price_for_display', null, array('class' => 'form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'maxlength' => '25',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('status', 'Status', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                        {!! Form::select('status', ['1' => 'For Sale', '0' => 'Sold', '2' => 'Pending', '3' => 'Trade In'],
                                                null,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px;',
                                                    'id' => 'model')) !!}
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div role="tabpanel" class="tab-pane fade in" id="detailed">
                            <br>
                            <div class="form-group">
                                {!! Form::label('description', 'Description', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        {!! Form::textarea('description', null, array('class' => 'form-control',
                                                'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <h2>Options</h2>
                            @foreach(Tsawler\WheelsPackage\Option::where('active','1')->orderBy('option_name')->get() as $option)
                                <div class="checkbox">
                                    <label>
                                        <input name="option_{!! $option->id !!}" type="checkbox"
                                               value="{{ $option->id }}"
                                               @if(in_array($option->id, $options))
                                               checked="checked"
                                                @endif
                                        >
                                        {{ $option->option_name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>

                        <div role="tabpanel" class="tab-pane fade in" id="images">
                            <br>
                            <label class="btn btn-info btn-file">
                                {!! Form::file('image_name[]',['id' => 'image_name', 'multiple' => true]) !!} <i
                                        class="fa fa-image"></i> Choose a new image...
                            </label>
                            <span id="file_name" class="file_name"></span><br>
                            <input type="submit" class="btn btn-primary" value="Save Changes">
                            <hr>
                            <ul class="list-inline sortable" id="sortablelist">
                                @foreach ($item->images as $image)
                                    <li data-id="{!! $image->id !!}">
                                        <img src="/storage/inventory/{{ $vehicle_id }}/thumbs/{!! $image->image !!}"
                                             class="img img-responsive">
                                        <a href="#!"><i class="fa fa-trash"
                                                        onclick="confirmDeleteImage({!! $image->id !!})"></i></a></li>
                                @endforeach
                            </ul>
                            <hr>

                        </div>

                        <div role="tabpanel" class="tab-pane fade in" id="sticker">
                            <br>
                            <ul class="list-inline">
                                <a class="btn btn-info" target="_blank"
                                   href="/wheels/vehicle-window-sticker?id={{ $vehicle_id }}">Print Window Sticker (PDF)</a>
                            </ul>

                        </div>

                        <div role="tabpanel" class="tab-pane fade in" id="video">
                            <br>

                            @if($video_id > 0)
                                @php
                                $video = \App\Video::find($video_id);
                                @endphp
                                <video width="320" controls style="max-width: 320px">
                                    <source src="/storage/videos/{{ $video->id }}-{!! $video->file_name !!}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                            @endif

                            <div class="form-group">
                                <label for="video_id" class="control-label">Choose a video</label>
                                <div class="controls">
                                    <div class="input-group">
                                        {!! Form::select('video_id', $videos,
                                                $video_id,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px; min-width: 400px;',
                                                    'id' => 'video_id')) !!}
                                    </div>
                                </div>
                            </div>

                            <hr>

                            @if($item->panorama != null)
                                <div id="vehicle-panorama"></div>
                            @endif

                            <div class="form-group">
                                <label for="panorama" class="control-label">Choose a Panaorama</label>
                                <div class="controls">
                                    <div class="input-group">
                                        {!! Form::file('panorama',['id' => 'panorama',]) !!}
                                    </div>
                                </div>

                            </div>




                        </div>

                    </div>

                    <hr>


                </div>
                <input type="submit" class="btn btn-primary" value="Save Changes">
                <input type="hidden" name="id" value="{!! $vehicle_id !!}">
                <input type="hidden" name="tab" id="tab" value="{!! $tab !!}">
                <input type="hidden" name="sort_list" id="sort_list">
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script src="/vendor/vcms5/public-assets/admin/js/jquery.sortable.min.js"></script>
    <script type="text/javascript" src="https://cdn.pannellum.org/2.4/pannellum.js"></script>
    <script>
        $(document).ready(function () {
            $("#bookform").validate({
                errorClass: 'has-error',
                validClass: 'has-success',
                errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
                }
            });

            $(':file').on('fileselect', function (event, numFiles, label) {
                if (numFiles == 1) {
                    $(".file_name").html(label);
                } else {
                    $(".file_name").html(numFiles + " images selected");
                }
            });

            @if($item->panorama != null)

            pannellum.viewer('vehicle-panorama', {
                "type": "equirectangular",
                "panorama": "/storage/panoramas/{!! $item->panorama->panorama !!}",
                "autoLoad": true
            });

            @endif

            $(".datepicker").datepicker({format: 'yyyy-mm-dd', autoclose: true});
            $('#myTabs a[href="#{!! $tab !!}"]').tab('show')

            CKEDITOR.replace('description',
                    {
                        toolbar: 'MyToolbar',
                        forcePasteAsPlainText: true,
                        enterMode: '1',
                        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
                    });


            $('#sortablelist').sortable();

            var a = {};
            $("#sortablelist li").each(function (i, el) {
                a[$(el).data('id')] = $(el).index() + 1;
            });
            sorteda = JSON.stringify(a);
            $("#sort_list").val(sorteda);

            $('#sortablelist').sortable().bind('sortupdate', function () {
                var a = {};
                $("#sortablelist li").each(function (i, el) {
                    a[$(el).data('id')] = $(el).index() + 1;
                });
                sorteda = JSON.stringify(a);
                $("#sort_list").val(sorteda);
            });
        });


        $(document).on('change', ':file', function () {
            var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });


        $("#make").change(function () {
            $.ajax({
                url: '/admin/inventory/modelsJson',
                type: 'get',
                data: 'id=' + $('#make').val(),
                dataType: 'json',
                success: function (json) {
                    $('#model').empty()
                    $.each(json, function (i, option) {
                        $('#model').append($('<option>').text(option.model).attr('value', option.id));
                    });
                }
            });
        });

        @if ($vehicle_id > 0)
        function confirmDeleteImage(x) {
            bootbox.confirm("Are you sure you want to delete this image?", function (result) {
                if (result == true) {
                    window.location.href = '/admin/inventory/delete-image?id=' + x + '&vid=' + {!! $vehicle_id !!};
                }
            });
        }
        @endif

        $(".tab").click(function () {
            $("#tab").val($(this).data('id'));
        });
    </script>
@stop