@extends('base')

@section('top-white')
    <h1>Powersports Item</h1>
@stop


@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    {!! $item->year or '' !!} {!! $item->product_name or 'New item' !!}
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                {!! Form::model($item,array(
                    'url' => '/admin/powersports/item',
                    'role' => 'form',
                    'name' => 'bookform',
                    'id' => 'bookform',
                    'method' => 'post',
                    'files' => 'true',
                    ))
                !!}

                <div role="tabpanel" id="myTabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#basic" aria-controls="basic" role="tab" data-toggle="tab" data-id="basic"
                               class="tab">Basic</a>
                        </li>

                        <li role="presentation">
                            <a href="#images" aria-controls="images" role="tab" data-toggle="tab" data-id="images"
                               class="tab">Images</a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="basic">
                            <br>
                            <div class="form-group">
                                {!! Form::label('year', 'Year', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-question"></i></span>
                                        {!! Form::selectRange('year', date("Y") + 1, 1900,
                                                null,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px;')) !!}
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                {!! Form::label('category', 'Category', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-question"></i></span>
                                        {!! Form::select('category', $categories,
                                                null,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px;',
                                                    'id' => 'model')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('product_name', 'Product Name', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                        {!! Form::text('product_name', null, array('class' => 'required form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('msrp', 'MSRP', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                        {!! Form::text('msrp', null, array('class' => 'form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('url', 'URL to product', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                        {!! Form::text('url', null, array('class' => 'form-control',
                                                                            'style' => 'max-width: 400px;',
                                                                            'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            @if($item->video_id > 0)
                                @php
                                    $video = \App\Video::find($item->video_id);
                                @endphp
                                <video width="320" controls style="max-width: 320px">
                                    <source src="/storage/videos/{{ $video->id }}-{!! $video->file_name !!}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                            @endif

                            @if($item->id > 0)
                                <img src="/storage/motorcycles/{{ $item->id }}.jpg">
                            @endif

                            <div class="form-group">
                                <label for="thumbnail">Thumbnail image</label>
                                <div class="input-group">
                                    <input type="file" name="thumbnail" id="thumbnail">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="video_id" class="control-label">Choose a video</label>
                                <div class="controls">
                                    <div class="input-group">
                                        {!! Form::select('video_id', $videos,
                                                $item->video_id,
                                                array('class' => 'form-control',
                                                    'style' => 'max-width: 400px; min-width: 400px;',
                                                    'id' => 'video_id')) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                {!! Form::label('description', 'Description', array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <div class="input-group">
                                        {!! Form::textarea('description', null, array('class' => 'form-control',
                                                'placeholder' => '')) !!}
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div role="tabpanel" class="tab-pane fade in" id="images">
                            <br>
                            <label class="btn btn-info btn-file">
                                {!! Form::file('image_name[]',['id' => 'image_name', 'multiple' => true]) !!} <i
                                        class="fa fa-image"></i> Choose a new image...
                            </label>
                            <span id="file_name" class="file_name"></span><br>
                            <input type="submit" class="btn btn-primary" value="Save Changes">
                            <hr>
                            <ul class="list-inline" id="sortablelist">
                                @if(sizeof($images) > 0)
                                    @foreach ($images as $key )
                                        <li data-id="{!! $key !!}">
                                            <img src="/storage/motorcycles/{{ $item->id }}/{!! $key !!}.jpg"
                                                 class="img img-responsive" style="max-width: 150px">
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                            <hr>

                        </div>

                    </div>

                    <hr>


                </div>
                <input type="submit" class="btn btn-primary" value="Save Changes">
                <input type="hidden" name="id" value="{!! $item->id !!}">
                <input type="hidden" name="tab" id="tab" value="{!! $tab !!}">
                <input type="hidden" name="sort_list" id="sort_list">
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script src="/vendor/vcms5/public-assets/admin/js/jquery.sortable.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#bookform").validate({
                errorClass: 'has-error',
                validClass: 'has-success',
                errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
                }
            });

            $('#image_name :file').on('fileselect', function (event, numFiles, label) {
                if (numFiles == 1) {
                    $(".file_name").html(label);
                } else {
                    $(".file_name").html(numFiles + " images selected");
                }
            });

            $('#myTabs a[href="#{!! $tab !!}"]').tab('show')

            CKEDITOR.replace('description',
                {
                    toolbar: 'MyToolbar',
                    forcePasteAsPlainText: true,
                    enterMode: '1',
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
                });
        });


        $(document).on('change', '#image_name :file', function () {
            var input = $(this),
                numFiles = input.get(1).files ? input.get(1).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        $(".tab").click(function () {
            $("#tab").val($(this).data('id'));
        });
    </script>
@stop