@extends('base')

@section('top-white')
    <h1>Welcome Email</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Welcome Email
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                {!! Form::model($email,array(
                    'url' => '/admin/email/welcome-email',
                    'role' => 'form',
                    'name' => 'bookform',
                    'id' => 'bookform',
                    'method' => 'post',
                    ))
                !!}

                <div class="form-group">
                    {!! Form::label('subject', 'Subject', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                            {!! Form::text('subject', null, array('class' => 'required form-control',
                                                                'style' => 'max-width: 400px;',
                                                                'placeholder' => 'Script name')) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('message', 'Message', array('class' => 'control-label')) !!}
                    <div class="controls">
                        {!! Form::textarea('message', null ) !!}
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <div class="controls">
                        {!! Form::submit('Save', array('class' => 'btn btn-primary submit')) !!}
                    </div>
                </div>

                <div>&nbsp;</div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        $(document).ready(function () {
            $("#bookform").validate({
                errorClass: 'has-error',
                validClass: 'has-success',
                errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
                }
            });
        });

        CKEDITOR.replace('message',
                {
                    toolbar: 'MyToolbar',
                    forcePasteAsPlainText: true,
                    enterMode: '1',
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files'
                });
    </script>
@stop