@extends('base')

@section('top-white')
    <h1>All Staff</h1>
@stop

@section('content-title')

@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Staff</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <table id="itable" class="table table-compact table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Position</th>
                        <th>Active</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        $(function() {
            $('#itable').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                responsive: true,
                ajax: '{!! route('datatables-admin-staff.allStaffJson') !!}',
                columns: [
                    { "data": "last_name",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/admin/staff/staff?id=" + oData.id + "'>" + oData.last_name + "</a>");
                        }
                    },
                    { data: 'first_name', name: 'first_name' },
                    { data: 'position', name: 'position' },
                    {
                        data: 'active',
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if (oData.active == 1) {
                                $(nTd).html("<span class='text-success'>Active</span>");
                            } else {
                                $(nTd).html("<span class='text-danger'>Inactive</span>");
                            }
                        }
                    }
                ]
            });
        });
    </script>
@stop