@extends('base')

@section('top-white')
    <h1>Staff Members: Sort Order</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Staff Members: Sort Order
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">


                <ul class="sortable" id="sortablelist">
                    @foreach($staff as $item)
                        <li data-id="{!! $item->id !!}">{!! $item->first_name !!} {!! $item->last_name !!}</li>
                    @endforeach
                </ul>

                <hr>

                <a class="btn btn-primary" href="#!" onclick="saveSortOrder()">Save Sort Order</a>

                {!! Form::model($staff,array(
                    'url' => '/admin/staff/sort',
                    'role' => 'form',
                    'name' => 'sortform',
                    'id' => 'sortform',
                    'method' => 'post',
                    ))
                !!}
                <input type="hidden" name="sort_list" id="sort_list">
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script src="/vendor/vcms5/public-assets/admin/js/jquery.sortable.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#sortablelist').sortable();

            var a = {};
            $("#sortablelist li").each(function (i, el) {
                a[$(el).data('id')] = $(el).index() + 1;
            });
            sorteda = JSON.stringify(a);
            $("#sort_list").val(sorteda);

            $('#sortablelist').sortable().bind('sortupdate', function () {
                var a = {};
                $("#sortablelist li").each(function (i, el) {
                    a[$(el).data('id')] = $(el).index() + 1;
                });
                sorteda = JSON.stringify(a);
                $("#sort_list").val(sorteda);
            });
        });

        function saveSortOrder()
        {
            $("#sortform").submit();
        }
    </script>
@stop