@extends('base')

@section('top-white')
    <h1>Vehicle Inventory</h1>
@stop

@section('content-title')

@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Vehicle Inventory</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <table id="itable" class="table table-compact table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Year</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Trim</th>
                        <th>Stock No</th>
                        <th>VIN</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th>Updated</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        $(function() {
            $('#itable').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                responsive: true,
                ajax: '/admin/json/all-inventory-json-powersports',
                columns: [
                    { "data": "year",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/admin/inventory/vehicle?id=" + oData.id + "'>" + oData.year + "</a>");
                        }
                    },
                    { data: 'make', name: 'make.make' },
                    { data: 'model', name: 'vehicleModel.model' },
                    { data: 'trim', name: 'trim' },
                    { data: 'stock_no', name: 'stock_no' },
                    { data: 'vin', name: 'vin' },
                    {
                        data: 'status',
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if (oData.status == 1) {
                                $(nTd).html("<span class='text-success'>For Sale</span>");
                            } else if (oData.status == 2) {
                                $(nTd).html("<span class='text-warning'>Pending</span>");
                            } else if (oData.status == 0) {
                                $(nTd).html("<span class='text-danger'>Sold</span>");
                            } else if (oData.status == 3) {
                                $(nTd).html("<span class='text-info'>Trade In</span>");
                            }
                        }
                    },
                    { data: 'created_at', name: 'vehicles.created_at' },
                    { data: 'updated_at', name: 'vehicles.updated_at' }
                ]
            });
        });
    </script>
@stop