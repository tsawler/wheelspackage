@extends('base')

@section('top-white')
    <h1>Staff Member</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Staff Member
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                {!! Form::model($staff,array(
                    'url' => '/admin/staff/staff',
                    'role' => 'form',
                    'name' => 'bookform',
                    'id' => 'bookform',
                    'method' => 'post',
                    'files' => true,
                    ))
                !!}

                <div class="form-group">
                    {!! Form::label('first_name', 'First Name', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                            {!! Form::text('first_name', null, array('class' => 'required form-control',
                                                                'style' => 'max-width: 400px;',
                                                                'placeholder' => 'First name')) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('last_name', 'Last Name', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                            {!! Form::text('last_name', null, array('class' => 'required form-control ',
                                                                'style' => 'max-width: 400px;',
                                                                'placeholder' => 'Last name')) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            {!! Form::email('email', null, array('class' => 'required email form-control ',
                                                                'style' => 'max-width: 400px;',
                                                                'placeholder' => 'you@company.com')) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('position', 'Position', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                            {!! Form::text('position', null, array('class' => 'required form-control ',
                                                                'style' => 'max-width: 400px;',
                                                                'placeholder' => 'Position')) !!}
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::label('active', 'Staff active?', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-question"></i></span>
                            {!! Form::select('active', array(
                                    '1' => 'Yes',
                                    '0' => 'No'),
                                    null,
                                    array('class' => 'form-control',
                                        'style' => 'max-width: 400px;')) !!}
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::label('image', 'Image', array('class' => 'control-label')) !!}
                    @if(($staff_id > 0) && ($staff->image != null))
                        <br>
                        <img class="img img-responsive" src="/storage/staff/{{ Tsawler\WheelsPackage\Employee::find($staff_id)->image }}">
                    @endif
                    <div class="controls">
                        <div class="input-group">
                            {!! Form::file('image',['id' => 'image']) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description', array('class' => 'control-label')) !!}
                    <div class="controls">
                        {!! Form::textarea('description', null ) !!}
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <div class="controls">
                        {!! Form::submit('Save', array('class' => 'btn btn-primary submit')) !!}
                        @if ($staff_id > 0)
                            <a class="btn btn-danger" href="#!" onclick='confirmDelete({!! $staff_id !!})'>Delete this
                                staff member</a>
                        @endif
                        <a class="btn btn-info" href="/admin/staff/all">Cancel</a>
                    </div>
                </div>

                <div>&nbsp;</div>

                {!! Form::hidden('employee_id', $staff_id )!!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        @if ($staff_id > 0)
        function confirmDelete(x) {
            bootbox.confirm("Are you sure you want to delete this staff member?", function (result) {
                if (result == true) {
                    window.location.href = '/admin/staff/delete?id=' + x;
                }
            });
        }
        @endif
        $(document).ready(function () {
            $("#bookform").validate({
                errorClass: 'has-error',
                validClass: 'has-success',
                errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
                }
            });

            CKEDITOR.replace('description',
                    {
                        toolbar: 'MyToolbar',
                        forcePasteAsPlainText: true,
                        enterMode: '1'
                    });
        });
    </script>
@stop