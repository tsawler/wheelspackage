@extends('base')

@section('top-white')
    <h1>Power Sports</h1>
@stop

@section('content-title')

@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Power Sports Inventory</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <table id="itable" class="table table-compact table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Year</th>
                        <th>Type</th>
                        <th>Created</th>
                        <th>Updated</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        $(function() {
            $('#itable').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                responsive: true,
                ajax: '{!! route('datatables-admin-all-powersports.allitemsJson') !!}',
                columns: [
                    { "data": "product_name",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/admin/powersports/item?id=" + oData.id + "'>" + oData.product_name + "</a>");
                        }
                    },
                    { data: 'year', name: 'year' },
                    { data: 'category', name: 'category' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' }
                ]
            });
        });
    </script>
@stop