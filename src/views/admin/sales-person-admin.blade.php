@extends('base')

@section('top-white')
    <h1>Sales Person Page</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Sales Person Page
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">

                @if($id > 0)
                    <a class="btn btn-primary" href="/sales/{!! $item->slug !!}" target="_blank">View Sales Person's Page</a>
                    <hr>
                @endif

                {!! Form::model($item,array(
                    'url' => '/admin/sales/salesperson',
                    'role' => 'form',
                    'name' => 'bookform',
                    'id' => 'bookform',
                    'method' => 'post',
                    'files' => 'true',
                    ))
                !!}

                <div class="form-group">
                    {!! Form::label('salesperson_name', 'Name', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                            {!! Form::text('salesperson_name', null, array('class' => 'required form-control ',
                                                                'style' => 'max-width: 400px;',
                                                                'placeholder' => '')) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Phone (10 digits, e.g. 15065551212)', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                            {!! Form::text('phone', null, array('class' => 'required form-control ',
                                                                'style' => 'max-width: 400px;',
                                                                'placeholder' => '')) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    @if($item->image != null)
                        <img src="/salesstaff/{!! $item->image !!}" class="img img-responsive" style="max-height: 200px;">
                    @endif

                        {!! Form::label('image', 'Image - must be 500px wide by 1074px tall, 72dpi')!!}
                        {!! Form::file('image',['id' => 'image']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('active', 'Active?', array('class' => 'control-label')) !!}
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-question"></i></span>
                            {!! Form::select('active', array(
                                    '1' => 'Yes',
                                    '0' => 'No'),
                                    null,
                                    array('class' => 'form-control',
                                        'style' => 'max-width: 400px;')) !!}
                        </div>
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <div class="controls">
                        {!! Form::submit('Save', array('class' => 'btn btn-primary submit')) !!}
                        @if ($id > 0)
                            <a class="btn btn-danger" href="#!" onclick='confirmDelete({!! $id !!})'>Delete this
                                sales person</a>
                        @endif
                        <a class="btn btn-info" href="/admin/sales/all-sales-people">Cancel</a>
                    </div>
                </div>

                <div>&nbsp;</div>

                {!! Form::hidden('id', $id) !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        @if ($id > 0)
        function confirmDelete(x) {
            bootbox.confirm("Are you sure you want to delete this sales person?", function (result) {
                if (result == true) {
                    window.location.href = '/admin/sales/delete?id=' + x;
                }
            });
        }
        @endif
        $(document).ready(function () {
            $("#bookform").validate({
                rules: {
                    phone: {
                        required: true,
                        digits: true,
                        minlength: 10,
                    }
                },
                errorClass: 'has-error',
                validClass: 'has-success',
                errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div[class='form-group']").addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
                }
            });
        });
    </script>
@stop