@php
    $credit_apps = Tsawler\WheelsPackage\CreditApplication::where('processed', '=', 0)->count();
    $finders = Tsawler\WheelsPackage\Finder::where('processed', '=', '0')->count();
    $test_drives = Tsawler\WheelsPackage\TestDrive::where('processed', '=', '0')->count();
    $pending_vehicles = Tsawler\WheelsPackage\Vehicle::where('status', '=', '2')->count();
    $quick_quotes = Tsawler\WheelsPackage\QuickQuote::where('processed', '=', '0')->count();

    if ($finders == 0)
        $finders = '';
    if ($credit_apps == 0)
        $credit_apps = '';
    if ($test_drives == 0)
        $test_drives = '';
    if ($pending_vehicles == 0)
        $pending_vehicles = '';
    if ($quick_quotes == 0)
        $quick_quotes = '';
@endphp

<ul class="list-inline style-icons">

    @if (Auth::user()->hasRole('finder'))
        <li class="text-center">
            <a href="/admin/finder/all">
                <i class="fa fa-fw fa-search fa-4x fa-fw text-success"></i>
                <br>
                <a href="/admin/finder/all" class="btn btn-primary" type="button">
                    Vehicle Finder Requests <span class="badge">{!! $finders !!}</span>
                </a>
            </a>
        </li>
        <li>&nbsp;</li>
    @endif

    @if (Auth::user()->hasRole('credit'))
        <li class="text-center">
            <a href="/admin/credit/all">
                <i class="fa fa-fw fa-usd fa-4x fa-fw text-success"></i>
                <br>
                <a href="/admin/credit/all" class="btn btn-primary" type="button">
                    Credit Applications <span class="badge">{!! $credit_apps !!}</span>
                </a>
            </a>
        </li>
        <li>&nbsp;</li>

        <li class="text-center">
            <a href="/admin/quick-quotes/all">
                <i class="fa fa-fw fa-usd fa-4x fa-fw text-success"></i>
                <br>
                <a href="/admin/quick-quotes/all" class="btn btn-primary" type="button">
                    Quick Quotes <span class="badge">{!! $quick_quotes !!}</span>
                </a>
            </a>
        </li>
        <li>&nbsp;</li>
    @endif

    @if (Auth::user()->hasRole('test_drive'))
        <li class="text-center">
            <a href="/admin/test-drives/all">
                <i class="fa fa-fw fa-calendar fa-4x fa-fw text-success"></i>
                <br>
                <a href="/admin/test-drives/all" class="btn btn-primary" type="button">
                    Test Drive Requests <span class="badge">{!! $test_drives !!}</span>
                </a>
            </a>
        </li>
        <li>&nbsp;</li>
    @endif

    @if (Auth::user()->hasRole('inventory'))
        <li class="text-center">
            <a href="/admin/inventory/all-vehicles-pending">
                <i class="fa fa-fw fa-car fa-4x fa-fw text-success"></i>
                <br>
                <a href="/admin/inventory/all-vehicles-pending" class="btn btn-primary" type="button">
                    Pending Vehicles <span class="badge">{!! $pending_vehicles !!}</span>
                </a>
            </a>
        </li>
        <li>&nbsp;</li>
    @endif

</ul>

<div class="row">
    <div class="col-md-12"><hr></div>
</div>