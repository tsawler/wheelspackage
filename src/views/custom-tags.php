<?php

$words = $crawler->filter('word-of-mouth')->count();
$testimonials = $crawler->filter('testimonials')->count();
$staff = $crawler->filter('staff')->count();
$credit_app = $crawler->filter('credit-app')->count();
$vehicle_finder = $crawler->filter('vehicle-finder')->count();
$all_inventory = $crawler->filter('all-inventory')->count();
$car_inventory = $crawler->filter('car-inventory')->count();
$truck_inventory = $crawler->filter('truck-inventory')->count();
$suv_inventory = $crawler->filter('suv-inventory')->count();
$van_inventory = $crawler->filter('van-inventory')->count();
$atv = $crawler->filter('atv-inventory')->count();
$mule = $crawler->filter('mule-inventory')->count();
$teryx = $crawler->filter('teryx-inventory')->count();
$jetski = $crawler->filter('jetski-inventory')->count();
$mercury = $crawler->filter('mercury-inventory')->count();
$motorcycle = $crawler->filter('motorcycle-inventory')->count();
$glastron = $crawler->filter('glastron-inventory')->count();
$glastron_used = $crawler->filter('glastron-used-inventory')->count();
$bennington = $crawler->filter('bennington-inventory')->count();
$hand_picked_inventory = $crawler->filter('hand-picked-inventory')->count();
$trailers = $crawler->filter('trailer-inventory')->count();
$powersports_used = $crawler->filter('powersports-used-inventory')->count();
$ebike_inventory = $crawler->filter('ebike-inventory')->count();
$vespa = $crawler->filter('vespa-inventory')->count();

if ($all_inventory > 0) {
    $inv_content = Tsawler\WheelsPackage\VehicleController::getEmbeddedInventory();
    $content = str_replace("<all-inventory></all-inventory>", $inv_content, $content);
}

if ($car_inventory > 0) {
    $inv_content = Tsawler\WheelsPackage\CarController::getEmbeddedInventory();
    $content = str_replace("<car-inventory></car-inventory>", $inv_content, $content);
}

if ($truck_inventory > 0) {
    $inv_content = Tsawler\WheelsPackage\TruckController::getEmbeddedInventory();
    $content = str_replace("<truck-inventory></truck-inventory>", $inv_content, $content);
}

if ($suv_inventory > 0) {
    $inv_content = Tsawler\WheelsPackage\SUVController::getEmbeddedInventory();
    $content = str_replace("<suv-inventory></suv-inventory>", $inv_content, $content);
}

if ($van_inventory > 0) {
    $inv_content = Tsawler\WheelsPackage\VanController::getEmbeddedInventory();
    $content = str_replace("<van-inventory></van-inventory>", $inv_content, $content);
}

if ($hand_picked_inventory > 0) {
    $inv_content = Tsawler\WheelsPackage\VehicleController::getEmbeddedHandPickedInventory();
    $content = str_replace("<hand-picked-inventory></hand-picked-inventory>", $inv_content, $content);
}

if ($vehicle_finder > 0) {
    $finder = Tsawler\WheelsPackage\VehicleFinderController::getVehicleFinder();
    $content = str_replace("<vehicle-finder></vehicle-finder>", $finder, $content);
}

if ($credit_app > 0) {
    $app = Tsawler\WheelsPackage\CreditController::getCreditApp();
    $content = str_replace("<credit-app></credit-app>", $app, $content);
}

if ($staff > 0) {
    $thestaff = Tsawler\WheelsPackage\StaffController::getStaff();
    $content = str_replace("<staff></staff>", $thestaff, $content);
}

if ($testimonials > 0) {
    $test = Tsawler\WheelsPackage\TestimonialController::getTestimonials();
    $content = str_replace("<testimonials></testimonials>", $test, $content);
}

if ($words > 0) {
    $test = Tsawler\WheelsPackage\WordController::getAll();
    $content = str_replace("<word-of-mouth></word-of-mouth>", $test, $content);
}

if ($motorcycle > 0) {
    $test = Tsawler\WheelsPackage\MotorcycleController::getEmbeddedInventory();
    $content = str_replace("<motorcycle-inventory></motorcycle-inventory>", $test, $content);
}

if ($atv > 0) {
    $test = Tsawler\WheelsPackage\ATVController::getEmbeddedInventory();
    $content = str_replace("<atv-inventory></atv-inventory>", $test, $content);
}

if ($mule > 0) {
    $test = Tsawler\WheelsPackage\ATVController::getEmbeddedInventoryMules();
    $content = str_replace("<mule-inventory></mule-inventory>", $test, $content);
}

if ($teryx > 0) {
    $test = Tsawler\WheelsPackage\ATVController::getEmbeddedInventoryTeryx();
    $content = str_replace("<teryx-inventory></teryx-inventory>", $test, $content);
}

if ($jetski > 0) {
    $test = Tsawler\WheelsPackage\ATVController::getEmbeddedInventoryJetski();
    $content = str_replace("<jetski-inventory></jetski-inventory>", $test, $content);
}

if ($mercury > 0) {
    $test = Tsawler\WheelsPackage\MercuryController::getEmbeddedInventory();
    $content = str_replace("<mercury-inventory></mercury-inventory>", $test, $content);
}

if ($glastron > 0) {
    $test = Tsawler\WheelsPackage\GlastronController::getEmbeddedInventory();
    $content = str_replace("<glastron-inventory></glastron-inventory>", $test, $content);
}

if ($glastron_used > 0) {
    $test = Tsawler\WheelsPackage\GlastronController::getEmbeddedUsedInventory();
    $content = str_replace("<glastron-used-inventory></glastron-used-inventory>", $test, $content);
}

if ($bennington > 0) {
    $test = Tsawler\WheelsPackage\BenningtonController::getEmbeddedInventory();
    $content = str_replace("<bennington-inventory></bennington-inventory>", $test, $content);
}

if ($trailers > 0) {
    $test = Tsawler\WheelsPackage\TrailerController::getEmbeddedInventory();
    $content = str_replace("<trailer-inventory></trailer-inventory>", $test, $content);
}

if ($powersports_used > 0) {
    $test = Tsawler\WheelsPackage\GlastronController::getEmbeddedUsedPowerSportsInventory();
    $content = str_replace("<powersports-used-inventory></powersports-used-inventory>", $test, $content);
}

if ($ebike_inventory > 0) {
    $test = Tsawler\WheelsPackage\ElectricBikeController::getEmbeddedInventory();
    $content = str_replace("<ebike-inventory></ebike-inventory>", $test, $content);
}

if ($vespa > 0) {
    $test = Tsawler\WheelsPackage\ElectricBikeController::getEmbeddedVespaInventory();
    $content = str_replace("<vespa-inventory></vespa-inventory>", $test, $content);
}