@extends('wheelspackage::mail.base-email')

@section('email-content')

    <p>Hello,</p>

    <p>
        A Vehicle Finder Request from {!! $first_name !!} {!! $last_name !!} was just made online.
    </p>

    <ul>
        <li>First Name: {!! $first_name !!}</li>
        <li>Last Name: {!! $last_name !!}</li>
        <li>Phone: {!! $phone !!}</li>
        <li>Email: {!! $email !!}</li>
        <li>Contact Method: {!! $contact_method !!}</li>
        <li>Year: {!! $year !!}</li>
        <li>Make: {!! $make !!}</li>
        <li>Model: {!! $model !!}</li>
    </ul>

    <p>
        <a class="btn btn-primary" href="{!! env('SITE_URL') !!}/admin/finder/finder?id={!! $id !!}">
            Click here to see the application
        </a>
    </p>
@stop