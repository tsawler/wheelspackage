@extends('wheelspackage::mail.base-email')

@section('email-content')

<p>Hello,</p>

<p>
A credit application from {!! $first_name !!} {!! $last_name !!} was just made online.
</p>

<ul>
<li>First Name: {!! $first_name !!}</li>
<li>Last Name: {!! $last_name !!}</li>
<li>Date of Birth: {!! $dob !!}</li>
<li>Phone: {!! $phone !!}</li>
<li>Email: {!! $email !!}</li>
<li>Address: {!! $address !!}</li>
<li>City/Town: {!! $city !!}</li>
<li>Province: {!! $province !!}</li>
<li>Postal Code: {!! $zip !!}</li>
<li>Interested In: {!! $vehicle !!}</li>
<li>Mortgage/Rent: {!! $rent !!}</li>
<li>Employer: {!! $employer !!}</li>
<li>Income: {!! $income !!}</li>
</ul>

<p>
<a class="btn btn-primary" href="{!! env('SITE_URL') !!}/admin/credit/app?id={!! $id !!}">
Click here to see the application
</a>
</p>
@stop