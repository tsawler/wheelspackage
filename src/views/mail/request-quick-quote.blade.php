@extends('wheelspackage::mail.base-email')

@section('email-content')

    <p>Hello!</p>

    <p>{!! $users_name !!} has requested a quick quote for the following vehicle:

    @if ($vehicle_id > 0)
    <ul class="list-unstyled">
        <li>{!! Tsawler\WheelsPackage\Vehicle::find($vehicle_id)->year !!} {!! Tsawler\WheelsPackage\Vehicle::find($vehicle_id)->make->make !!} {!! Tsawler\WheelsPackage\Vehicle::find($vehicle_id)->vehicleModel->model !!}</li>
        <li>Stock#: {!! Tsawler\WheelsPackage\Vehicle::find($vehicle_id)->stock_no !!}</li>
    </ul>
    <p>
        <a class="btn btn-danger" href="{{ env('SITE_URL') }}/vehicles/vehicle/{{ $vehicle_id }}">Click here to see the vehicle</a>
    </p>
    @endif

    <ul>
        <li>Name: {!! $users_name !!}</li>
        <li>Email: {!! $users_email !!}</li>
        <li>Phone: {!! $users_phone !!}</li>
        @if($talk_about != "")
        <li>Wants to talk about {{ $talk_about }}</li>
        @endif
    </ul>
@stop
