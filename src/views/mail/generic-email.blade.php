@extends('wheelspackage::mail.base-email')

@section('email-content')
    <p>Hello {!! $name !!},</p>

    {!! $content !!}
@stop