<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width"/>

    <style type="text/css">

        @include('wheelspackage::mail.bootstrap')

    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="center-block" align="center">
            @include('wheelspackage::mail.logo')
            <br><br>
        </div>
    </div>

    <div class="col-md-12">
        @yield('email-content')
    </div>

    <hr>

    <div class="row">
        <div class="col-md-12 text-center">
            <p style="border-top: 1px solid silver;">
                &nbsp;
            </p>
            <p>
                <strong>Jim Gilbert's Wheels and Deals</strong><br>
                402 St. Mary's Street<br>
                Fredericton, NB<br>
                <a href="tel:15064596832">(506) 459-6832</a><br>
                <a href="mailto:reception@wheelsanddeals.ca">reception@wheelsanddeals.ca</a><br>
                <a href="http://www.wheelsanddeals.ca">www.wheelsanddeals.ca</a>
            </p>
        </div>
    </div>
</div>


</body>
</html>
