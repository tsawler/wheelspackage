@extends('wheelspackage::mail.base-email')

@section('email-content')

    <p>Hello!</p>

    <p>{!! $users_name !!} thought you might be interested in this vehicle at
        <a href="{{ env('SITE_URL') }}">Jim Gilbert's Wheels &amp; Deals</a>:</p>

    <p>
        <a class="btn btn-danger" href="{{ env('SITE_URL') }}/vehicles/vehicle/{{ $vehicle_id }}">Click here to see the vehicle!</a>
    </p>
@stop
