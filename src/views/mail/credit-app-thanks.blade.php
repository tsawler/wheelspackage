@extends('wheelspackage::mail.base-email')

@section('email-content')

    <p>Hello {!! $first_name !!},</p>

    <p>
        Your information has been received. Thank you.
    </p>
    <p>
        A Huggable Wheels and Deals Team Finance Manager will review it and contact you if any additional information is
        requested. Remember at Canada's Huggable Car Dealer you have choice's. We will work with you and help you get
        the vehicle that fits your budget and needs.
    </p>
    <p>
        We extend "Free Walkaway Job Loss and Credit Protection" on every vehicle. All our vehicles come with Factory
        Warranty and our Motor Vehicle Inspection is Guaranteed. We will be contacting you soon with your approval.
    </p>
    <p>
        Tip: Keep your credit score positive, do not send out multiple applications. Let us get you approved at the
        lowest rate! possible! "Less credit hits are better."
    </p>

@stop